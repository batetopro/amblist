# amblist

Дипломна работа
Семантично хранилище за амбулаторни листи и инструменти за работа с тях

![overview](https://media.springernature.com/full/springer-static/image/art%3A10.1186%2Fs13326-018-0176-y/MediaObjects/13326_2018_176_Fig1_HTML.gif "Overview")

## Инсталация

### База данни

Системата използва mySQL като релационна база данни, в която се съхранява информацията в строго структуриран ваирант,
в който добре са описани взаимните връзки между различните компоненти, които съставят един амбулаторен лист.

* Инсталиране на mySQL server. За целта може да се използват следните връзки:
    * [самостоятелен mySQL server](https://dev.mysql.com/downloads/installer/)
    * [mySQL server като част от цялостен пакет](https://www.apachefriends.org/download.html)

* Създаване на базата от данни. Изпълнява се следната команда в mySQL client:
```sql
CREATE DATABASE amblist CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

* Създаване на таблиците. Изпълнаява се следната команда от треминала:
```bash
mysql amblist -u root < ./db/schema.sql
``` 

* Прочитане на номенкултурните таблици. Изпълнаяват се следните команди от треминала:
```bash
mysql amblist -u root < ./db/seed/CodeSpec.sql
mysql amblist -u root < ./db/seed/MKBCode.sql
``` 

### Python част

Системата използва python като скриптов език, като се използва например за 
извличането на различна информация и добавянето и към базата от данни.

* Инсталиране на python3.5. За целта може да се използва следната връзка:
[python 3.5.7](https://www.python.org/downloads/release/python-357/)

* Инсталиране на необходимите python пакети. Изпълнява се следната команда:
```bash
pip install -r python/requirements.txt
``` 
Възможно е да се окаже, че има проблем с mysqlclient пакета, тогава може да се 
използва следната връза, за да се изтегли този пакет като whl файл:
[mysqlclient](https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient)

Избира се whl файла, който отговаря на системата, която се ползва и се инсталацра като:
```bash
pip install {downloaded whl file}
```

* Настройка на конфирацията на python частта. За целта се копира файла settings.inc.py:
```bash
cp python\settings\settings.inc.py python\settings\settings.py
```

След това се отваря файла settings.py и се попълват следните полета:
```python
Settings.SECRET_KEY # ключ, който се ползва от django с целите на сигурността
Settings.DATABASES.default.NAME # име на базата данни, която се изплолзва (amblist)
Settings.DATABASES.default.USER # потребител на базата данни, като трябва да има достъп за четен и писане
Settings.DATABASES.default.PASSWORD # парола на потребителя
```

### PHP web interface

* Инсталиране на php7.1+. За целта може да се използва следната връзка:
     * [php като самостятелен пакет](https://www.php.net/downloads.php)
     * [mySQL server като част от цялостен пакет](https://www.apachefriends.org/download.html)

В случай, че се избере като самостоятелен пакет, е необходимо да се настрои и 
http server, каквито са apache или nignx.

* Инсталиране на зависомостите
За целта на php web интерфейса е използван framework-a laravel.
По-подробна информация за него може да се види [тук](https://laravel.com/docs/5.8/installation)

За да се инсталират зависимостите, трябва да се използва [composer](https://getcomposer.org/download/).
След като се нагласи composer на системата, могат да се изтеглят необходимите завсимости:

```bash
cd webui
composer install
```

* Конфигуриране на HTTP web server-a
Това е специфично за всеки HTTP server, който може да се използва и работи с php.
В случая на apache в неговият ``httpd-vhosts.conf`` файл се добавя следните редове:

```
<VirtualHost *:80>
    DocumentRoot "път към webui/public"
    ServerName amblist.com
    
    <Directory ""път към webui/public"">
		Options Indexes FollowSymLinks Includes ExecCGI
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
```

* Конфигуриране на laravel
За целта се копира `.env.example` файла като `.env`:
```bash
cp webui/.env.example webui/.env
```

Този `.env` файл се редактира, като:
```bash
DB_DATABASE='базата данни'
DB_USERNAME='потребителя на базата данни'
DB_PASSWORD='паролата на потребителя'
```

След това се генерира стойност на ключа на приложението със следната команда:
```bash
php webui\artisan key:generate
```

## Прочитане на инфромацията в базата данни
 






TODO: this is mot OK

* data - хранилище на данни. Съдържа:
  * amblist - примерни амбулаторни листи
* db - имплементация на базата данни
* docs - документи
* py-requirements - изисквания откъм страна на python
* python - част от системата, имплементирана на python. Съдържа:
  * прехвърляне на xml документите към базата данни
* xsd - дефиниции на XML докуемнтите







## Web interface

