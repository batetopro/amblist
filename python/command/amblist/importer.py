import os
import xml.etree.ElementTree as ET
from amblist.parser import PracticeParser
from mkb.registry import MKBCodeRegistry


class AmblistImporter:
    def execute(self):
        dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
        dir_path = os.path.join(dir_path, 'data')

        code_registry = MKBCodeRegistry()
        code_registry.load()

        print('Importing XML Documents ...')
        practices = PracticeParser(code_registry)
        for root, folders, files in os.walk(os.path.join(dir_path, 'amblist')):
            for file in files:
                path = os.path.join(root, file)
                print('Importing {0}'.format(path))
                print('-' * 30)

                tree = ET.parse(path)
                practices.parse(tree.getroot(), path.replace(dir_path, ''))
