import logging
import scrapy.crawler
from mkb.spider import MkbSpider


class MkbCodeLoader:
    def execute(self):
        logger = logging.getLogger("command")
        logger.info('[mkb_spider] Importing MKB codes')

        print('Importing MKB codes ...')

        process = scrapy.crawler.CrawlerProcess(MkbSpider.get_settings())

        process.crawl(MkbSpider)
        process.start()
