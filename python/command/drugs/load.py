import os
import csv


class DrugsLoader:
    def execute(self):
        from amblist.model import DrugInfo

        dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
        dir_path = os.path.join(dir_path, 'data')

        print('Importing drugs ...')

        with open(os.path.join(dir_path, 'drugs.csv')) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                DrugInfo.create(
                    row['code'],
                    row['name'],
                    row['market_name'],
                    row['form'],
                    row['quantity'],
                    row['unit']
                )
