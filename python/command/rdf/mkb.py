from rdflib import BNode, Literal, Graph
from rdflib.namespace import Namespace


class MKBRdfReader:
    def execute(self, namespace_path):
        from mkb.model import MKBChapter, MKBSet, MKBCode

        mkb_owl = Namespace(namespace_path)

        graph = Graph()
        for chapter in MKBChapter.objects.all():
            rdf_chapter = BNode()
            graph.add((rdf_chapter, mkb_owl.code, Literal(chapter.code)))
            graph.add((rdf_chapter, mkb_owl.name, Literal(chapter.name)))

            for set in MKBSet.objects.filter(chapter_id=chapter.id):
                rdf_set = BNode()
                graph.add((rdf_set, mkb_owl.SetChapter, rdf_chapter))
                graph.add((rdf_set, mkb_owl.code, Literal(set.code)))
                graph.add((rdf_set, mkb_owl.name, Literal(set.name)))

                for code in MKBCode.objects.filter(set_id=set.id):
                    rdf_code = BNode()
                    graph.add((rdf_code, mkb_owl.CodeSet, rdf_set))
                    graph.add((rdf_code, mkb_owl.code, Literal(code.code)))
                    graph.add((rdf_code, mkb_owl.name, Literal(code.name)))
                    graph.add((rdf_code, mkb_owl.name_latin, Literal(code.name_latin)))

        with open("output.xml", mode="w", encoding="utf8") as file:
            file.write(graph.serialize(format='xml').decode())
