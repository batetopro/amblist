import logging
from .mkb import MKBRdfReader


class RdfLoader:
    def execute(self):
        logger = logging.getLogger("command")
        logger.info('[mkb_spider] Importing MKB codes')

        print('Importing MKB codes ...')

        mkb = MKBRdfReader()
        mkb.execute('./../owl/MKB.owl')
