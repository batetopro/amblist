from command import *


class Settings:
    SECRET_KEY = ""
    TIME_ZONE = "UTC"
    INSTALLED_APPS = ("mkb", "amblist", )

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.mysql",
            "NAME": "amblist",
            "USER": "root",
            "PASSWORD": "",
            "HOST": "127.0.0.1",
            "PORT": "3306",
            'OPTIONS': {'charset': 'utf8mb4'},
        }
    }

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level':'DEBUG',
                'class':'logging.handlers.RotatingFileHandler',
                'filename': 'logs/mylog.log',
                'maxBytes': 1024*1024*5,        # 5 MB
                'backupCount': 5,
                'formatter':'standard',
            },
            "command_handler": {
                'level': 'INFO',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': 'logs/command.log',
                'maxBytes': 1024 * 1024 * 5,    # 5 MB
                'backupCount': 5,
                'formatter': 'standard',
            },
            "error_handler": {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': 'logs/error.log',
                'maxBytes': 1024 * 1024 * 5,    # 5 MB
                'backupCount': 5,
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'DEBUG',
                'propagate': True
            },
            'command': {
                'handlers': ['command_handler'],
                'level': 'DEBUG',
                'propagate': True
            },
            'error':  {
                'handlers': ['error_handler'],
                'level': 'DEBUG',
                'propagate': False
            },
        }
    }

    COMMAND = {
        "amblist": {
            "runner": AmblistImporter,
            "arguments": {}
        },
        "mkb_codes": {
            "runner": MkbCodeLoader,
            "arguments": {}
        }
    }
