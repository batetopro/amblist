class RpParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse_drug(self, xml):
        from ..model import Drug

        prescNum = 0
        DrugCode = ''
        MKB_id = None
        Quantity = 1
        Day = 1

        for elem in xml:
            if elem.tag.endswith('prescNum'):
                prescNum = elem.text
            elif elem.tag.endswith('DrugCode'):
                DrugCode = elem.text
            elif elem.tag.endswith('DrugMKB'):
                MKB_id = self.mkb_code_registry.lookup(elem.text)
            elif elem.tag.endswith('Quantity'):
                Quantity = elem.text
            elif elem.tag.endswith('Day'):
                Day = elem.text

        return [Drug.lookup(MKB_id, DrugCode).id, prescNum, Quantity, Day]

    def parse(self, xml, patient_id, amblist_id):
        from ..model import Rp, Rp_Drug

        RpBook = None
        drugs = []

        for elem in xml:
            if elem.tag.endswith('RpBook'):
                RpBook = elem.text
            elif elem.tag.endswith('RpDrug'):
                drugs.append(self.parse_drug(elem))

        rp = Rp.lookup(patient_id, RpBook)
        Rp_Drug.link(rp.id, amblist_id, drugs)

        return rp
