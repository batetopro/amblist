class HospParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml, patient_id, amblist_id):
        from ..model import HospNapr

        mkb_codes = []
        PathNum = ''
        NaprNo = ''
        DateNapr = None

        for elem in xml:
            if elem.tag.endswith('HospMKB'):
                mkb_codes.append(self.mkb_code_registry.lookup_both(elem.text))
            elif elem.tag.endswith('PathNum'):
                PathNum = elem.text
            elif elem.tag.endswith('NaprNo'):
                NaprNo = elem.text
            elif elem.tag.endswith('DateNapr'):
                DateNapr = elem.text

        hosp_napr = HospNapr.lookup(amblist_id, PathNum, NaprNo, DateNapr)

        self.mkb_code_registry.link(hosp_napr.id, mkb_codes, 'HospNapr_MKBCode', 'HospNapr_id')

        return hosp_napr
