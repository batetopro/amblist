class MDDParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml, patient_id, amblist_id):
        from ..model import MDD, kodMDD, MDDkodMDD

        mkb_id = None
        mkb_link_id = None

        NoMDD = ''
        MDDFor = 0
        ACHIcode = None
        codes = []

        for elem in xml:
            if elem.tag.endswith('NoMDD'):
                NoMDD = elem.text
            elif elem.tag.endswith('MDDFor'):
                MDDFor = elem.text
            elif elem.tag.endswith('kodMDD'):
                codes.append(kodMDD.lookup(elem.text))
            elif elem.tag.endswith('ACHIcode'):
                ACHIcode = elem.text
            elif elem.tag.endswith('MDDMKB'):
                mkb_id, mkb_link_id = self.mkb_code_registry.lookup_both(elem.text)

        mdd = MDD.lookup(amblist_id, mkb_id, mkb_link_id, NoMDD, MDDFor, ACHIcode)
        MDDkodMDD.link(mdd.id, codes)

        return mdd
