class PatientParser:
    def parse(self, patient):
        from ..model import Patient

        EGN = ''
        LNCH_dateBirth = None
        LNCH_Sex = None
        SS_No = None
        RZOK = ''
        ZdrRajon = ''
        NameGiven = ''
        NameSur = None
        NameFamily = ''
        Address = ''
        IsHealthInsurance = True
        COUNTRY_COUNTRYCODE = None
        COUNTRY_COUNTRYIDNO = None
        COUNTRY_Type_CERTIFICATE = None
        COUNTRY_DateIssue = None
        COUNTRY_DateFrom = None
        COUNTRY_DateTo = None
        COUNTRY_EHIC_No = None
        COUNTRY_PersID_No = None

        for elem in patient:
            if elem.tag.endswith('EGN'):
                EGN = elem.text
            elif elem.tag.endswith('LNCH_dateBirth'):
                LNCH_dateBirth = elem.text
            elif elem.tag.endswith('LNCH_Sex'):
                LNCH_Sex = elem.text
            elif elem.tag.endswith('SS_No'):
                SS_No = elem.text
            elif elem.tag.endswith('RZOK'):
                RZOK = elem.text
            elif elem.tag.endswith('ZdrRajon'):
                ZdrRajon = elem.text
            elif elem.tag.endswith('Address'):
                Address = elem.text
            elif elem.tag.endswith('IsHealthInsurance'):
                IsHealthInsurance = elem.text
            elif elem.tag.endswith('COUNTRY_COUNTRYCODE'):
                COUNTRY_COUNTRYCODE = elem.text
            elif elem.tag.endswith('COUNTRY_COUNTRYIDNO'):
                COUNTRY_COUNTRYIDNO = elem.text
            elif elem.tag.endswith('COUNTRY_Type_CERTIFICATE'):
                COUNTRY_Type_CERTIFICATE = elem.text
            elif elem.tag.endswith('COUNTRY_DateIssue'):
                COUNTRY_DateIssue = elem.text
            elif elem.tag.endswith('COUNTRY_DateFrom'):
                COUNTRY_DateFrom = elem.text
            elif elem.tag.endswith('COUNTRY_DateTo'):
                COUNTRY_DateTo = elem.text
            elif elem.tag.endswith('COUNTRY_EHIC_No'):
                COUNTRY_EHIC_No = elem.text
            elif elem.tag.endswith('COUNTRY_PersID_No'):
                COUNTRY_PersID_No = elem.text
            elif elem.tag.endswith('Name'):
                for subelem in elem:
                    if subelem.tag.endswith('Given'):
                        NameGiven = subelem.text
                    elif subelem.tag.endswith('Sur'):
                        NameSur = subelem.text
                    elif subelem.tag.endswith('Family'):
                        NameFamily = subelem.text

        return Patient.lookup(EGN, LNCH_dateBirth, LNCH_Sex, SS_No, RZOK, ZdrRajon, NameGiven, NameSur, NameFamily,
            Address, IsHealthInsurance, COUNTRY_COUNTRYCODE, COUNTRY_COUNTRYIDNO, COUNTRY_Type_CERTIFICATE,
            COUNTRY_DateIssue, COUNTRY_DateFrom, COUNTRY_DateTo, COUNTRY_EHIC_No, COUNTRY_PersID_No)
