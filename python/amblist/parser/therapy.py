class TherapyParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse_reimbursible(self, xml):
        from ..model import Drug

        drug_code = None
        drug_mkb_id = None
        quantity = 1
        day = 1

        for elem in xml:
            if elem.tag.endswith('DrugCode'):
                drug_code = elem.text.strip()
            elif elem.tag.endswith('DrugMKB'):
                drug_mkb_id = self.mkb_code_registry.lookup(elem.text).id
            elif elem.tag.endswith('Quantity'):
                quantity = elem.text
            elif elem.tag.endswith('Day'):
                day = elem.text

        return [Drug.lookup(drug_mkb_id, drug_code).id, quantity, day]

    def parse(self, xml):
        nonreimburce = None
        reimbursible = []

        for elem in xml:
            if elem.tag.endswith('Nonreimburce'):
                nonreimburce = elem.text.strip()
            elif elem.tag.endswith('Reimbursible'):
                reimbursible.append(self.parse_reimbursible(elem))

        return nonreimburce, reimbursible

    def link(self, amblist_id, reimbursible):
        from ..model import AmblistDrug
        AmblistDrug.link(amblist_id, reimbursible)
