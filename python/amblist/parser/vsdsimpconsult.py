class VSDSIMPConsultParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml, patient_id, amblist_id):
        from ..model import VSDSIMPConsult

        kodSpVSD = ''
        VSDFor = 0
        NoNVSD = ''
        kodVSD = ''
        ACHIcode = None
        HospVSD = None

        mkb_codes = []

        for elem in xml:
            if elem.tag.endswith('kodSpVSD'):
                kodSpVSD = elem.text
            elif elem.tag.endswith('VSDFor'):
                VSDFor = elem.text
            elif elem.tag.endswith('NoNVSD'):
                NoNVSD = elem.text
            elif elem.tag.endswith('kodVSD'):
                kodVSD = elem.text
            elif elem.tag.endswith('ACHIcode'):
                ACHIcode = elem.text
            elif elem.tag.endswith('HospVSD'):
                HospVSD = elem.text
            elif elem.tag.endswith('VSDMKB'):
                mkb_codes.append(self.mkb_code_registry.lookup_both(elem.text))

        consult = VSDSIMPConsult.lookup(amblist_id, kodSpVSD, VSDFor, NoNVSD, kodVSD, ACHIcode, HospVSD)

        self.mkb_code_registry.link(consult.id, mkb_codes, 'VSDSIMPconsult_MKBCode', 'VSDSIMPConsult_id')
        
        return consult
