class DiagnoseParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml):
        MKB = None
        MKBLinkD = None
        imeD = None
        imeLinkD = None

        for elem in xml:
            if elem.tag.endswith('MKB'):
                MKB = elem.text
            elif elem.tag.endswith('MKBLinkD'):
                MKBLinkD = elem.text
            elif elem.tag.endswith('imeD') or elem.tag.endswith('imeMD'):
                imeD = elem.text
            elif elem.tag.endswith('imeLinkD'):
                imeLinkD = elem.text

        MKB_id = self.mkb_code_registry.lookup(MKB, imeD).id
        if MKBLinkD:
            MKBLinkD_id = self.mkb_code_registry.lookup(MKBLinkD, imeLinkD).id
        else:
            MKBLinkD_id = None

        return MKB_id, MKBLinkD_id

    def link(self, amblist_id, mkb_codes):
        self.mkb_code_registry.link(amblist_id, mkb_codes, 'Amblist_MKBCode', 'Amblist_id')
