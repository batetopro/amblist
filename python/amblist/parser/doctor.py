from .amblist import AmblistParser


class DoctorParser:
    def __init__(self, mkb_code_registry):
        self.amblist = AmblistParser(mkb_code_registry)

    def parse(self, xml, practice):
        from ..model import Doctor

        UIN = ''
        EGN = ''
        FullName = ''
        SIMPCode = ''
        amblists = []

        for elem in xml:
            if elem.tag.endswith('UIN'):
                UIN = elem.text
            elif elem.tag.endswith('EGN'):
                EGN = elem.text
            elif elem.tag.endswith('FullName'):
                FullName = elem.text
            elif elem.tag.endswith('SIMPCode'):
                SIMPCode = elem.text
            elif elem.tag.endswith('AmbList'):
                amblists.append(elem)

        doctor = Doctor.lookup(practice, UIN, EGN, FullName, SIMPCode)

        for elem in amblists:
            self.amblist.parse(elem, doctor)
