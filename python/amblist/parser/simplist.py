from .sended_from import SendedFromParser


class SimpListParser:
    def __init__(self):
        self.senders = SendedFromParser()

    def parse_primary_visit(self, xml):
        result = {
            'VidNapr': None,
            'NoNapr': None,
            'NaprFor': None,
            'dateNapr': None,
            'SendedFrom': None,
        }

        for elem in xml:
            if elem.tag.endswith('VidNapr'):
                result['VidNapr'] = elem.text
            elif elem.tag.endswith('NoNapr'):
                result['NoNapr'] = elem.text
            elif elem.tag.endswith('NaprFor'):
                result['NaprFor'] = elem.text
            elif elem.tag.endswith('dateNapr'):
                result['dateNapr'] = elem.text
            elif elem.tag.endswith('SendedFrom'):
                result['SendedFrom'] = self.senders.parse(elem).id

        return result

    def parse_secondary_visit(self, xml):
        result = {'NoAmbL': None, 'dateAmbL': None }

        for elem in xml:
            if elem.tag.endswith('NoAmbL'):
                result['NoAmbL'] = elem.text
            elif elem.tag.endswith('dateAmbL'):
                result['dateAmbL'] = elem.text

        return result

    def parse(self, xml):
        result = dict(has_primary_visit=False, has_secondary_visit=False, primary_visit=None, secondary_visit=None)

        for elem in xml:
            if elem.tag.endswith('PrimaryVisit'):
                result['has_primary_visit'] = True
                result['primary_visit'] = self.parse_primary_visit(elem)
            elif elem.tag.endswith('SecondaryVisit'):
                result['has_secondary_visit'] = True
                result['secondary_visit'] = self.parse_secondary_visit(elem)

        return result

    def link_parsed_info(self, amblist_id, parsed_info):
        from ..model import PrimaryVisit, SecondaryVisit

        if parsed_info['has_primary_visit']:
            PrimaryVisit.lookup(
                amblist_id,
                parsed_info['primary_visit']['SendedFrom'],
                parsed_info['primary_visit']['VidNapr'],
                parsed_info['primary_visit']['NoNapr'],
                parsed_info['primary_visit']['NaprFor'],
                parsed_info['primary_visit']['dateNapr']
            )

        if parsed_info['has_secondary_visit']:
            SecondaryVisit.lookup(
                amblist_id,
                parsed_info['secondary_visit']['NoAmbL'],
                parsed_info['secondary_visit']['dateAmbL']
            )

