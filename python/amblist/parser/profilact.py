class ProfilactParser:
    def parse(self, xml):
        from ..model import Profilact

        child = False
        mother = False
        gest_week = None
        over18 = False
        risk = False

        for elem in xml:
            if elem.tag.endswith('Child'):
                child = elem.text
            elif elem.tag.endswith('Mother'):
                mother = elem.text
            elif elem.tag.endswith('GestWeek'):
                gest_week = elem.text
            elif elem.tag.endswith('Over18'):
                over18 = elem.text
            elif elem.tag.endswith('Risk'):
                risk = elem.text

        return Profilact.lookup(child, mother, over18, risk), gest_week
