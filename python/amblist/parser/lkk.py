class LKKParser:
    def __init__(self):
        pass

    def parse(self, xml, patient_id, amblist_id):
        from ..model import CodeSpec, LKK, LKKCodeSpec

        NoLKK = ''
        TypeLKK = 0
        codes = []

        for elem in xml:
            if elem.tag.endswith('NoLKK'):
                NoLKK = elem.text
            elif elem.tag.endswith('TypeLKK'):
                TypeLKK = elem.text
            elif elem.tag.endswith('CodeSpec'):
                codes.append(CodeSpec.lookup(elem.text))

        lkk = LKK.lookup(amblist_id, NoLKK, TypeLKK)
        LKKCodeSpec.link(lkk.id, codes)

        return lkk
