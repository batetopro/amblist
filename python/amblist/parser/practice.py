from .doctor import DoctorParser


class PracticeParser:
    def __init__(self, mkb_code_registry):
        self.doctors = DoctorParser(mkb_code_registry)

    def parse(self, xml, file_name):
        from ..model import Practice, PracticeLoad

        doctors = []

        practice_code = ''
        practice_name = ''
        NZOK_code = None
        contract_number = None
        contract_date = None
        date_from = None
        date_to = None
        ContrHA = False

        for elem in xml:
            if elem.tag.endswith('PracticeCode'):
                practice_code = elem.text
            elif elem.tag.endswith('PracticeName'):
                practice_name = elem.text
            elif elem.tag.endswith('NZOKCode'):
                NZOK_code = elem.text
            elif elem.tag.endswith('ContractNo'):
                contract_number = elem.text
            elif elem.tag.endswith('ContractDate'):
                contract_date = elem.text
            elif elem.tag.endswith('DateFrom'):
                date_from = elem.text
            elif elem.tag.endswith('DateTo'):
                date_to = elem.text
            elif elem.tag.endswith('ContrHA'):
                ContrHA = elem.text
            elif elem.tag.endswith('Doctor'):
                doctors.append(elem)

        practice = Practice.lookup(practice_code, practice_name, NZOK_code, contract_number, contract_date, ContrHA)
        parctice_load = PracticeLoad.lookup(practice.id, date_from, date_to, file_name)

        for elem in doctors:
            self.doctors.parse(elem, practice)
