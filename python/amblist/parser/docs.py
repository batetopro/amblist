from .simpconsult import SimpconsultParser
from .vsdsimpconsult import VSDSIMPConsultParser
from .mdd import MDDParser
from .lkk import LKKParser
from .hlist import HListParser
from .rp import RpParser
from .hosp_napr import HospParser


class DocsParser:
    def __init__(self, mkb_code_registry):
        self.parsers = dict(
            simpconsult=SimpconsultParser(mkb_code_registry),
            vsdsimpconsult=VSDSIMPConsultParser(mkb_code_registry),
            mdd=MDDParser(mkb_code_registry),
            lkk=LKKParser(),
            hlist=HListParser(mkb_code_registry),
            rp=RpParser(mkb_code_registry),
            hosp_napr=HospParser(mkb_code_registry)
        )

    def parse_elems(self, elems, patient_id, amblist_id):
        for k in self.parsers:
            if len(elems[k]) == 0:
                continue

            for elem in elems[k]:
                self.parsers[k].parse(elem, patient_id, amblist_id)

    def parse(self, xml):
        talon_telk = False
        izvestie = False
        et_epikriza = False
        medical_note = False

        elems = dict(simpconsult=[], vsdsimpconsult=[], mdd=[], lkk=[], hlist=[], rp=[], hosp_napr=[])

        for elem in xml:
            if elem.tag.endswith('TalonTELK'):
                talon_telk = elem.text
            elif elem.tag.endswith('Izvestie'):
                izvestie = elem.text
            elif elem.tag.endswith('EtEpikriza'):
                et_epikriza = elem.text
            elif elem.tag.endswith('MedicalNote'):
                medical_note = elem.text
            elif elem.tag.endswith('VSDSIMPConsult'):
                elems['vsdsimpconsult'].append(elem)
            elif elem.tag.endswith('SIMPConsult'):
                elems['simpconsult'].append(elem)
            elif elem.tag.endswith('MDD'):
                elems['mdd'].append(elem)
            elif elem.tag.endswith('LKK'):
                elems['lkk'].append(elem)
            elif elem.tag.endswith('HospNapr'):
                elems['hosp_napr'].append(elem)
            elif elem.tag.endswith('HList'):
                elems['hlist'].append(elem)
            elif elem.tag.endswith('Rp'):
                elems['rp'].append(elem)

        return talon_telk, izvestie, et_epikriza, medical_note, elems
