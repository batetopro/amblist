from .patient import PatientParser
from .diagnose import DiagnoseParser
from .procedure import ProcedureParser
from .simplist import SimpListParser
from .visit_for import VisitForParser
from .imun import ImunParser
from .therapy import TherapyParser
from .hired_subst import HiredSubstParser
from .docs import DocsParser


class AmblistParser:
    def __init__(self, mkb_code_registry):
        self.patients = PatientParser()
        self.diagnoses = DiagnoseParser(mkb_code_registry)
        self.therapy = TherapyParser(mkb_code_registry)
        self.docs = DocsParser(mkb_code_registry)

        self.procedures = ProcedureParser()
        self.simpLists = SimpListParser()
        self.visits = VisitForParser()
        self.imuns = ImunParser()
        self.hired_subst = HiredSubstParser()

    def parse(self, amblist, doctor):
        from ..model import Amblist, AmblistLKKMember

        no_al = ''
        data_al = None
        time = None
        pay = ''
        disadv = 0
        incidentally = 0
        exam_type = None
        anamnesa = None
        HState = None
        examine = None
        sign = False
        patient = None
        main_mkb_id = None
        main_mkb_link_id = None
        mantu = None
        vaccine_number = None
        vaccine_type = None
        visit_for = None
        nonreimburce = None
        hired_subst = None
        reimbursible = []

        talon_telk = None
        izvestie = None
        et_epikriza = None
        medical_note = None

        lkk_members = []
        gest_week = None

        hired_subst_id = None

        simp_lists_info = dict(has_primary_visit=False, has_secondary_visit=False)
        docs_elems = dict(simpconsult=[], vsdsimpconsult=[], mdd=[], lkk=[], hlist=[], rp=[], hosp_napr=[])

        diagnoses = []
        procedures = []
        imuns = []

        for elem in amblist:
            if elem.tag.endswith('NoAl'):
                no_al = elem.text
            elif elem.tag.endswith('dataAl'):
                data_al = elem.text
            elif elem.tag.endswith('time'):
                time = elem.text
            elif elem.tag.endswith('Pay'):
                pay = elem.text
            elif elem.tag.endswith('Hired_Subst'):
                hired_subst = self.hired_subst.parse(elem)
            elif elem.tag.endswith('Patient'):
                patient = self.patients.parse(elem)
            elif elem.tag.endswith('Disadv'):
                disadv = elem.text
            elif elem.tag.endswith('Incidentally'):
                incidentally = elem.text
            elif elem.tag.endswith('MainDiag'):
                main_mkb_id, main_mkb_link_id = self.diagnoses.parse(elem)
            elif elem.tag.endswith('Diag'):
                diagnoses.append(self.diagnoses.parse(elem))
            elif elem.tag.endswith('Procedure'):
                procedures.append(self.procedures.parse(elem))
            elif elem.tag.endswith('Anamnesa'):
                anamnesa = elem.text
            elif elem.tag.endswith('HState'):
                HState = elem.text
            elif elem.tag.endswith('Examine'):
                examine = elem.text
            elif elem.tag.endswith('Therapy'):
                nonreimburce, reimbursible = self.therapy.parse(elem)
            elif elem.tag.endswith('SIMPList'):
                simp_lists_info = self.simpLists.parse(elem)
            elif elem.tag.endswith('VisitFor'):
                visit_for, lkk_members, gest_week = self.visits.parse(elem)
            elif elem.tag.endswith('ExamType'):
                exam_type = elem.text
            elif elem.tag.endswith('Imun'):
                mantu, vaccine_number, vaccine_type, imuns = self.imuns.parse(elem)
            elif elem.tag.endswith('Docs'):
                talon_telk, izvestie, et_epikriza, medical_note, docs_elems = self.docs.parse(elem)
            elif elem.tag.endswith('Sign'):
                sign = elem.text

        if hired_subst is not None:
            hired_subst_id = hired_subst.id

        amblist = Amblist.lookup(
            doctor.id, patient.id, main_mkb_id, main_mkb_link_id, visit_for.id,
            hired_subst_id, simp_lists_info['has_primary_visit'], simp_lists_info['has_secondary_visit'],
            no_al, data_al, time, pay, disadv, incidentally, sign,
            exam_type, anamnesa, HState, examine, mantu, vaccine_number, vaccine_type, nonreimburce,
            talon_telk, izvestie, et_epikriza, medical_note, gest_week, len(lkk_members),
            len(docs_elems['simpconsult']), len(docs_elems['vsdsimpconsult']), len(docs_elems['mdd']),
            len(docs_elems['lkk']), len(docs_elems['hlist']), len(docs_elems['rp']), len(docs_elems['hosp_napr'])
        )

        AmblistLKKMember.link(amblist.id, lkk_members)

        self.procedures.link(amblist.id, procedures)
        self.diagnoses.link(amblist.id, diagnoses)
        self.imuns.link(amblist.id, imuns)
        self.therapy.link(amblist.id, reimbursible)

        self.simpLists.link_parsed_info(amblist.id, simp_lists_info)
        self.docs.parse_elems(docs_elems, patient.id, amblist.id)

        return amblist
