
class HiredSubstParser:
    def __init__(self):
        pass

    def parse(self, xml):
        from ..model import LkkMember

        UIN = ''
        SIMPCode = ''

        for elem in xml:
            if elem.tag.endswith('UIN'):
                UIN = elem.text

            if elem.tag.endswith('SIMPCode'):
                SIMPCode = elem.text

        return LkkMember.lookup(UIN, SIMPCode)

