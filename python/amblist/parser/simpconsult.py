class SimpconsultParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml, patient_id, amblist_id):
        from ..model import SIMPConsult

        kodSp = ''
        SpFor = 0
        NoN = ''
        mkb_codes = []

        for elem in xml:
            if elem.tag.endswith('kodSp'):
                kodSp = elem.text
            elif elem.tag.endswith('SpFor'):
                SpFor = elem.text
            elif elem.tag.endswith('NoN'):
                NoN = elem.text
            elif elem.tag.endswith('SIMPMKB'):
                mkb_codes.append(self.mkb_code_registry.lookup_both(elem.text))

        simpconsult = SIMPConsult.lookup(amblist_id, kodSp, SpFor, NoN)

        self.mkb_code_registry.link(simpconsult.id, mkb_codes, 'SIMPconsult_MKBCode', 'SIMPConsult_id')

        return simpconsult
