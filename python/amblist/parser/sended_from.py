class SendedFromParser:
    def parse(self, xml):
        from ..model import SendedFrom

        PracticeCode = ""
        UIN = ""
        SIMPCode = ""
        H_S = None
        UINH_S = None

        for elem in xml:
            if elem.tag.endswith('PracticeCode'):
                PracticeCode = elem.text
            elif elem.tag.endswith('UIN'):
                UIN = elem.text
            elif elem.tag.endswith('SIMPCode'):
                SIMPCode = elem.text
            elif elem.tag.endswith('UINH_S'):
                UINH_S = elem.text
            elif elem.tag.endswith('H_S'):
                H_S = elem.text

        return SendedFrom.lookup(PracticeCode, UIN, SIMPCode, H_S, UINH_S)
