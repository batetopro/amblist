class HListParser:
    def __init__(self, mkb_code_registry):
        self.mkb_code_registry = mkb_code_registry

    def parse(self, xml, patient_id, amblist_id):
        from ..model import HList

        NoBl = ''
        days = 0
        mkb_id = None
        HLFromDate = ''
        HLToDate = ''
        HLtype = None

        for elem in xml:
            if elem.tag.endswith('NoBl'):
                NoBl = elem.text
            elif elem.tag.endswith('days'):
                days = elem.text
            elif elem.tag.endswith('HLMKB'):
                mkb_id = self.mkb_code_registry.lookup(elem.text).id
            elif elem.tag.endswith('HLFromDate'):
                HLFromDate = elem.text
            elif elem.tag.endswith('HLToDate'):
                HLToDate = elem.text
            elif elem.tag.endswith('HLtype'):
                HLtype = elem.text

        return HList.lookup(amblist_id, mkb_id, NoBl, days, HLFromDate, HLToDate, HLtype)
