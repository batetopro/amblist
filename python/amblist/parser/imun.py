class ImunParser:
    def __init__(self):
        pass

    def parse(self, xml):
        from ..model import Imun

        Mantu = None
        VaccineNo = None
        VaccineType = None
        imuns = []

        for elem in xml:
            if elem.tag.endswith('Mantu'):
                Mantu = elem.text
            elif elem.tag.endswith('VaccineNo'):
                VaccineNo = elem.text
            elif elem.tag.endswith('VaccineType'):
                VaccineType = elem.text
            elif elem.tag.endswith('CodeImun'):
                imuns.append(Imun.lookup(elem.text))

        return Mantu, VaccineNo, VaccineType, imuns

    def link(self, amblist_id, imuns):
        from ..model import AmblistImun
        AmblistImun.link(amblist_id, imuns)
