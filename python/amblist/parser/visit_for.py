from .profilact import ProfilactParser
from .lkk_member import LKKMemberParser


class VisitForParser:
    def __init__(self):
        self.profilacts = ProfilactParser()
        self.lkk_members = LKKMemberParser()

    def parse(self, xml):
        from ..model import VisitFor

        consult = 0
        profilact = None
        gest_week = None
        disp = None
        hosp = None
        rp_hosp = False
        lkk_visit = False
        telk = False

        lkk_members = []

        for elem in xml:
            if elem.tag.endswith('Consult'):
                consult = elem.text
            elif elem.tag.endswith('Profilact'):
                profilact, gest_week = self.profilacts.parse(elem)
            elif elem.tag.endswith('Disp'):
                disp = elem.text
            elif elem.tag.endswith('VSDVisit'):
                for subelem in elem:
                    if subelem.tag.endswith('Hosp'):
                        hosp = subelem.text
            elif elem.tag.endswith('RpHosp'):
                rp_hosp = elem.text
            elif elem.tag.endswith('LKKVisit'):
                lkk_visit = elem.text
            elif elem.tag.endswith('LKKMember'):
                lkk_members.append(self.lkk_members.parse(elem).id)
            elif elem.tag.endswith('Telk'):
                telk = elem.text

        profilact_id = None
        if profilact is not None:
            profilact_id = profilact.id

        visit_for = VisitFor.lookup(profilact_id, consult, disp, hosp, rp_hosp, lkk_visit, telk)

        return visit_for, lkk_members, gest_week
