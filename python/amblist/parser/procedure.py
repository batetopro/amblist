class ProcedureParser:
    def __init__(self):
        pass

    def parse(self, xml):
        from ..model import Procedure

        imeP = ''
        kodP = ''
        ACHIcode = None
        countP = 1

        for elem in xml:
            if elem.tag.endswith('imeP'):
                imeP = elem.text
            elif elem.tag.endswith('kodP'):
                kodP = elem.text
            elif elem.tag.endswith('CountP'):
                countP = elem.text
            elif elem.tag.endswith('ACHIcode'):
                ACHIcode = elem.text

        if kodP in imeP:
            imeP = imeP.replace(kodP, '').strip()

        return Procedure.lookup(imeP, kodP, ACHIcode), countP

    def link(self, amblist_id, procedures):
        from ..model import AmblistProcedure
        AmblistProcedure.link(amblist_id, procedures)
