from django.db import models


class LkkMember(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "LkkMember"

    UIN = models.CharField(null=False, max_length=255)
    SIMPCode = models.CharField(null=False, max_length=2)

    @staticmethod
    def lookup(UIN, SIMPCode):
        try:
            lkk_member = LkkMember.objects.get(UIN=UIN)
        except LkkMember.DoesNotExist:
            lkk_member = LkkMember()
            lkk_member.UIN = UIN
            lkk_member.SIMPCode = SIMPCode
            lkk_member.save()

        return lkk_member
