from django.db import models


class SendedFrom(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "SendedFrom"

    PracticeCode = models.CharField(null=False, max_length=255)
    UIN = models.CharField(null=False, max_length=255)
    SIMPCode = models.CharField(null=False, max_length=2)
    H_S = models.BooleanField(null=True)
    UINH_S = models.CharField(max_length=10, null=True)

    @staticmethod
    def lookup(PracticeCode, UIN, SIMPCode, H_S, UINH_S):
        try:
            sender = SendedFrom.objects.get(PracticeCode=PracticeCode, UIN=UIN)
        except:
            sender = SendedFrom()
            sender.PracticeCode = PracticeCode
            sender.UIN = UIN
            sender.SIMPCode = SIMPCode
            sender.H_S = H_S
            sender.UINH_S = UINH_S
            sender.save()

        return sender
