from django.db import models


class DrugInfo(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "DrugInfo"

    DrugName = models.ForeignKey('DrugName', null=False, on_delete=models.PROTECT)
    Code = models.CharField(null=False, max_length=6)
    Name = models.CharField(null=False, max_length=255)
    MarketName = models.CharField(null=False, max_length=255)
    Form = models.CharField(null=False, max_length=255)
    Quantity = models.CharField(null=False, max_length=60)
    Unit = models.CharField(null=False, max_length=60)

    @staticmethod
    def lookup(code):
        try:
            return DrugInfo.objects.get(Code=code)
        except DrugInfo.DoesNotExist:
            return None

    @staticmethod
    def create(code, name, market_name, form, quantity, unit):


        try:
            drug = DrugInfo.objects.get(Code=code)
        except DrugInfo.DoesNotExist:
            drug = DrugInfo()
            drug.Code = code
            drug.Name = name
            drug.MarketName = market_name
            drug.Form = form
            drug.Quantity = quantity
            drug.Unit = unit
            drug.save()

        return drug
