from django.db import models, connection


class MDD(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "MDD"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKBCode', on_delete=models.PROTECT, null=False)
    MKBLink = models.ForeignKey('mkb.MKBCode', on_delete=models.PROTECT, null=True)
    NoMDD = models.CharField(null=False, max_length=6)
    MDDFor = models.IntegerField(null=False)
    ACHIcode = models.CharField(null=True, max_length=8)

    @staticmethod
    def lookup(amblist_id, mkb_id, mkb_link_id, NoMDD, MDDFor, ACHIcode):
        try:
            mdd = MDD.objects.get(Amblist_id=amblist_id, NoMDD=NoMDD)
        except MDD.DoesNotExist:
            mdd = MDD()
            mdd.Amblist_id = amblist_id
            mdd.MKB_id = mkb_id
            mdd.MKBLink_id = mkb_link_id
            mdd.NoMDD = NoMDD
            mdd.MDDFor = MDDFor
            mdd.ACHIcode = ACHIcode
            mdd.save()

        return mdd


class kodMDD(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "kodMDD"

    Code = models.CharField(max_length=6, null=False)

    @staticmethod
    def lookup(code):
        return kodMDD.objects.get_or_create(Code=code)[0]


class MDDkodMDD(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "MDD_kodMDD"

    MDD = models.ForeignKey('MDD', null=False, on_delete=models.PROTECT)
    kodMDD = models.ForeignKey('kodMDD', null=False, on_delete=models.PROTECT)

    @staticmethod
    def link(mdd_id, codes):
        if len(codes) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM MDD_kodMDD WHERE MDD_id=%s'
            cursor.execute(sql, [mdd_id])

            sql = 'INSERT INTO MDD_kodMDD (MDD_id, kodMDD_id) VALUES ' + \
                  ','.join(['({0}, {1})'.format(mdd_id, v.id) for v in codes])
            cursor.execute(sql, [])
