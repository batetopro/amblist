from django.db import models


class SIMPConsult(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "SIMPConsult"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    kodSp = models.CharField(null=False, max_length=2)
    SpFor = models.IntegerField(null=False, max_length=10)
    NoN = models.CharField(null=False, max_length=6)

    @staticmethod
    def lookup(amblist_id, kodSp, SpFor, NoN):
        try:
            simp_consult = SIMPConsult.objects.get(Amblist_id=amblist_id, NoN=NoN)
        except SIMPConsult.DoesNotExist:
            simp_consult = SIMPConsult()
            simp_consult.Amblist_id = amblist_id
            simp_consult.kodSp = kodSp
            simp_consult.SpFor = SpFor
            simp_consult.NoN = NoN
            simp_consult.save()

        return simp_consult


class SIMPconsultMKBCode(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "SIMPconsult_MKBCode"

    Simpconsult = models.ForeignKey('Simpconsult', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=False)
    MKBLink = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=False)
