from django.db import models


class Patient(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Patient"

    EGN = models.CharField(null=False, max_length=10)
    LNCH_dateBirth = models.DateField(null=True)
    LNCH_Sex = models.BooleanField(null=True)
    SS_No = models.CharField(max_length=20, null=True)
    RZOK = models.CharField(max_length=2, null=True)
    ZdrRajon = models.CharField(max_length=2, null=True)
    NameGiven = models.CharField(max_length=30, null=False)
    NameSur = models.CharField(max_length=30, null=True)
    NameFamily = models.CharField(max_length=30, null=False)
    Address = models.CharField(max_length=100, null=True)
    IsHealthInsurance = models.BooleanField(null=True, default=1)
    COUNTRY_COUNTRYCODE = models.CharField(2, null=True)
    COUNTRY_COUNTRYIDNO = models.CharField(20, null=True)
    COUNTRY_Type_CERTIFICATE = models.CharField(6, null=True)
    COUNTRY_DateIssue = models.DateField(null=True)
    COUNTRY_DateFrom = models.DateField(null=True)
    COUNTRY_DateTo = models.DateField(null=True)
    COUNTRY_EHIC_No = models.CharField(null=True, max_length=20)
    COUNTRY_PersID_No = models.CharField(null=True, max_length=20)

    @staticmethod
    def lookup(EGN, LNCH_dateBirth, LNCH_Sex, SS_No, RZOK, ZdrRajon, NameGiven, NameSur, NameFamily, Address, IsHealthInsurance,
           COUNTRY_COUNTRYCODE, COUNTRY_COUNTRYIDNO, COUNTRY_Type_CERTIFICATE, COUNTRY_DateIssue, COUNTRY_DateFrom, COUNTRY_DateTo,
           COUNTRY_EHIC_No, COUNTRY_PersID_No):

        try:
            return Patient.objects.get(EGN=EGN)
        except Patient.DoesNotExist:
            patient = Patient()
            patient.EGN = EGN
            patient.LNCH_dateBirth = LNCH_dateBirth
            patient.LNCH_Sex = LNCH_Sex
            patient.SS_No = SS_No
            patient.RZOK = RZOK
            patient.ZdrRajon = ZdrRajon
            patient.NameGiven = NameGiven
            patient.NameSur = NameSur
            patient.NameFamily = NameFamily
            patient.Address = Address
            patient.IsHealthInsurance = IsHealthInsurance
            patient.COUNTRY_COUNTRYCODE = COUNTRY_COUNTRYCODE
            patient.COUNTRY_COUNTRYIDNO = COUNTRY_COUNTRYIDNO
            patient.COUNTRY_Type_CERTIFICATE = COUNTRY_Type_CERTIFICATE
            patient.COUNTRY_DateIssue = COUNTRY_DateIssue
            patient.COUNTRY_DateFrom = COUNTRY_DateFrom
            patient.COUNTRY_DateTo = COUNTRY_DateTo
            patient.COUNTRY_EHIC_No = COUNTRY_EHIC_No
            patient.COUNTRY_PersID_No = COUNTRY_PersID_No
            patient.save()

        return patient
