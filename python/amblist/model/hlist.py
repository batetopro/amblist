from django.db import models


class HList(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "HList"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKBCode', on_delete=models.PROTECT, null=False)
    NoBl = models.CharField(null=False, max_length=12)
    days = models.IntegerField(null=False)
    HLFromDate = models.DateField(null=False)
    HLToDate = models.DateField(null=False)
    HLtype = models.BooleanField(null=True)

    @staticmethod
    def lookup(amblist_id, mkb_id, NoBl, days, HLFromDate, HLToDate, HLtype):
        try:
            return HList.objects.get(Amblist_id=amblist_id, NoBl=NoBl)
        except HList.DoesNotExist:
            hlist = HList()
            hlist.Amblist_id = amblist_id
            hlist.MKB_id = mkb_id
            hlist.NoBl = NoBl
            hlist.days = days
            hlist.HLFromDate = HLFromDate
            hlist.HLToDate = HLToDate
            hlist.HLtype = HLtype
            hlist.save()

        return hlist
