from django.db import models


class Profilact(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Profilact"

    Child = models.BooleanField(null=False, default=False)
    Mother = models.BooleanField(null=False, default=False)
    Over18 = models.BooleanField(null=False, default=False)
    Risk = models.BooleanField(null=False, default=False)

    @staticmethod
    def lookup(Child, Mother, Over18, Risk):
        return Profilact.objects.get_or_create(
            Child=Child,
            Mother=Mother,
            Over18=Over18,
            Risk=Risk
        )[0]
