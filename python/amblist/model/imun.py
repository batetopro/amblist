from django.db import models, connection


class Imun(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Imun"

    Code = models.CharField(null=False, max_length=2)

    @staticmethod
    def lookup(code):
        return Imun.objects.get_or_create(Code=code)[0]


class AmblistImun(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist_Imun"

    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.PROTECT)
    Imun = models.ForeignKey('Imun', null=False, on_delete=models.PROTECT)

    @staticmethod
    def link(amblist_id, codes):
        if len(codes) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM Amblist_Imun WHERE Amblist_Id=%s'
            cursor.execute(sql, [amblist_id])

            sql = 'INSERT INTO Amblist_Imun (Amblist_Id, Imun_id) VALUES ' + \
                  ','.join(['({0}, {1})'.format(amblist_id, v.id) for v in codes])
            cursor.execute(sql, [])
