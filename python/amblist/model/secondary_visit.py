from django.db import models


class SecondaryVisit(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "SecondaryVisit"

    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.CASCADE)
    NoAmbL = models.CharField(null=False, max_length=6)
    dateAmbL = models.DateField(null=False)

    @staticmethod
    def lookup(amblist_id, no_amb_l, date_amb_l):
        try:
            visit = SecondaryVisit.objects.get(Amblist_id=amblist_id)
        except SecondaryVisit.DoesNotExist:
            visit = SecondaryVisit()
            visit.Amblist_id = amblist_id
            visit.NoAmbL = no_amb_l
            visit.dateAmbL = date_amb_l
            visit.save()

        return visit
