from django.db import models, connection


class Rp(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Rp"

    Patient = models.ForeignKey('Patient', null=False, on_delete=models.PROTECT)
    RpBook = models.CharField(null=True, default=False, max_length=7)

    @staticmethod
    def lookup(patient_id, RpBook):
        try:
            rp = Rp.objects.get(Patient_id=patient_id)
        except Rp.DoesNotExist:
            rp = Rp()
            rp.Patient_id = patient_id
            rp.RpBook = RpBook
            rp.save()

        return rp


class Rp_Drug(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Rp_Drug"

    Rp = models.ForeignKey('Rp', on_delete=models.PROTECT, null=False)
    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.PROTECT)
    Diag = models.ForeignKey('Drugs', on_delete=models.PROTECT, null=False)

    @staticmethod
    def link(rp_id, amblist_id, diags):
        if len(diags) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM Rp_Drug WHERE Rp_id=%s'
            cursor.execute(sql, [rp_id])

            sql = 'INSERT IGNORE INTO Rp_Drug (Rp_id, Amblist_id, Drug_id, prescNum, Quantity, `Day`) VALUES ' + \
                  ','.join(['({0}, {1}, {2}, {3}, {4}, {5})'.format(rp_id, amblist_id, p[0], p[1], p[2], p[3]) for p in diags]) + \
                  ' ON DUPLICATE KEY UPDATE Quantity = Quantity + VALUES(Quantity), `Day` = `Day` + VALUES(`Day`)'
            cursor.execute(sql, [])
