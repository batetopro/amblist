from django.db import models, connection


class LKK(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "LKK"

    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.PROTECT)
    NoLKK = models.CharField(null=False, max_length=6)
    TypeLKK = models.IntegerField(null=False)

    @staticmethod
    def lookup(amblist_id, NoLKK, TypeLKK):
        try:
            result = LKK.objects.get(Amblist_id=amblist_id)
        except LKK.DoesNotExist:
            result = LKK()
            result.Amblist_id = amblist_id
            result.NoLKK = NoLKK
            result.TypeLKK = TypeLKK
            result.save()

        return result


class CodeSpec(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "CodeSpec"

    Code = models.CharField(null=False)

    @staticmethod
    def lookup(code):
        return CodeSpec.objects.get_or_create(Code=code)[0]


class LKKCodeSpec(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "LKK_CodeSpec"

    LKK = models.ForeignKey('LKK', on_delete=models.PROTECT, null=False)
    CodeSpec = models.ForeignKey('CodeSpec', on_delete=models.PROTECT, null=False)

    @staticmethod
    def link(docs_id, codes):
        if len(codes) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM LKK_CodeSpec WHERE LKK_id=%s'
            cursor.execute(sql, [docs_id])

            sql = 'INSERT IGNORE INTO LKK_CodeSpec (LKK_id, CodeSpec_id) VALUES ' + \
                  ','.join(['({0}, {1})'.format(docs_id, v.id) for v in codes])
            cursor.execute(sql, [])
