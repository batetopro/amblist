from django.db import models


class Doctor(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Doctor"

    Practice = models.ForeignKey('Practice', null=False, on_delete=models.PROTECT)
    UIN = models.CharField(null=False, max_length=255)
    EGN = models.CharField(null=False, max_length=10)
    FullName = models.CharField(null=False, max_length=100)
    SIMPCode = models.CharField(null=False, max_length=2)

    @staticmethod
    def lookup(practice, UIN, EGN, FullName, SIMPCode):
        try:
            doctor = Doctor.objects.get(UIN=UIN)
        except Doctor.DoesNotExist:
            doctor = Doctor()
            doctor.Practice = practice
            doctor.UIN = UIN
            doctor.EGN = EGN
            doctor.FullName = FullName
            doctor.SIMPCode = SIMPCode
            doctor.save()

        return doctor
