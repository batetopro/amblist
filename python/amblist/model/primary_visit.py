from django.db import models


class PrimaryVisit(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "PrimaryVisit"

    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.CASCADE)
    SendedFrom = models.ForeignKey('SendedFrom', null=False, on_delete=models.CASCADE)
    NoNapr = models.CharField(max_length=6, null=False)
    VidNapr = models.IntegerField(null=False)
    NaprFor = models.IntegerField(null=False)
    dateNapr = models.DateField(null=False)

    @staticmethod
    def lookup(amblist_id, sended_from_id, vid_napr, no_napr, napr_for, date_napr):
        from ..model import PrimaryVisit

        try:
            visit = PrimaryVisit.objects.get(Amblist_id=amblist_id)
        except PrimaryVisit.DoesNotExist:
            visit = PrimaryVisit()
            visit.Amblist_id = amblist_id
            visit.SendedFrom_id = sended_from_id
            visit.VidNapr = vid_napr
            visit.NoNapr = no_napr
            visit.NaprFor = napr_for
            visit.dateNapr = date_napr
            visit.save()

        return visit
