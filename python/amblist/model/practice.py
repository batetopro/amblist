from django.db import models


class Practice(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Practice"

    PracticeCode = models.CharField(null=False, max_length=120)
    PracticeName = models.CharField(null=True, max_length=150)
    NZOKCode = models.CharField(null=True, max_length=10)
    ContractNo = models.CharField(null=True, max_length=6)
    ContractDate = models.DateField(null=False)
    ContrHA = models.BooleanField(null=False)

    @staticmethod
    def lookup(practice_code, practice_name, NZOK_code, contract_number, contract_date, ContrHA):
        try:
            practice = Practice.objects.get(PracticeCode=practice_code)
        except Practice.DoesNotExist:
            practice = Practice()
            practice.PracticeCode = practice_code
            practice.PracticeName = practice_name
            practice.NZOKCode = NZOK_code
            practice.ContractNo = contract_number
            practice.ContractDate = contract_date
            practice.ContrHA = ContrHA
            practice.save()

        return practice


class PracticeLoad(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "PracticeLoad"

    Practice = models.ForeignKey('Practice', on_delete=models.PROTECT, null=False)
    FileName = models.CharField(null=False, max_length=255)
    DateFrom = models.DateField(null=False)
    DateTo = models.DateField(null=False)

    @staticmethod
    def lookup(practice_id, date_from, date_to, file_name):
        try:
            load = PracticeLoad.objects.get(Practice_id=practice_id)
        except PracticeLoad.DoesNotExist:
            load = PracticeLoad()
            load.Practice_id = practice_id
            load.DateFrom = date_from
            load.DateTo = date_to
            load.FileName = file_name
            load.save()

        return load
