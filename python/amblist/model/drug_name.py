from django.db import models


class DrugName(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "DrugName"

    Name = models.CharField(null=False, max_length=255)

    @staticmethod
    def lookup(name):
        try:
            drugname = DrugName.objects.get(Name=name)
        except DrugName.DoesNotExist:
            drugname = DrugName()
            drugname.Name = name
            drugname.save()

        return drugname
