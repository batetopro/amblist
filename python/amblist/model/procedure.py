from django.db import models, connection


class Procedure(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Procedure"

    imeP = models.CharField(null=False, max_length=250)
    kodP = models.CharField(null=False, max_length=6)
    ACHIcode = models.CharField(null=False, max_length=8)

    @staticmethod
    def lookup(imeP, kodP, ACHIcode):
        try:
            return Procedure.objects.get(kodP=kodP)
        except Procedure.DoesNotExist:
            procedure = Procedure()
            procedure.imeP = imeP
            procedure.kodP = kodP
            procedure.ACHIcode = ACHIcode
            procedure.save()

        return procedure


class AmblistProcedure(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist_Procedure"

    Procedure = models.ForeignKey('Procedure', on_delete=models.PROTECT, null=False)
    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    CountP = models.PositiveIntegerField(null=False)

    @staticmethod
    def link(amblist_id, procedures):
        if len(procedures) == 0:
            return

        values = []
        for procedure in procedures:
            values.append([procedure[0].id, procedure[1]])

        with connection.cursor() as cursor:
            sql = 'DELETE FROM Amblist_Procedure WHERE Amblist_id=%s'
            cursor.execute(sql, [amblist_id])

            sql = 'INSERT INTO Amblist_Procedure (Amblist_id, Procedure_id, CountP) VALUES ' + \
                  ','.join(['({0}, {1}, {2})'.format(amblist_id, p[0], p[1]) for p in values]) + \
                  ' ON DUPLICATE KEY UPDATE CountP = CountP + VALUES(CountP)'
            cursor.execute(sql, [])
