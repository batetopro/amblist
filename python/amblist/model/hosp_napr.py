from django.db import models


class HospNapr(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "HospNapr"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    PathNum = models.CharField(null=False, max_length=5)
    NaprNo = models.CharField(null=False, max_length=6)
    DateNapr = models.DateField(null=True)

    @staticmethod
    def lookup(amblist_id, PathNum, NaprNo, DateNapr):
        try:
            return HospNapr.objects.get(Amblist_id=amblist_id)
        except HospNapr.DoesNotExist:
            hosp_napr = HospNapr()
            hosp_napr.Amblist_id = amblist_id
            hosp_napr.PathNum = PathNum
            hosp_napr.NaprNo = NaprNo
            hosp_napr.DateNapr = DateNapr
            hosp_napr.save()

        return hosp_napr


class HospNaprMKBCode(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "HospNapr_MKBCode"

    HospNapr = models.ForeignKey('HospNapr', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=False)
    MKBLink = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=True)
