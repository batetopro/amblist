from django.db import models, connection


class Amblist(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist"

    Doctor = models.ForeignKey('Doctor', null=False, on_delete=models.PROTECT)
    Patient = models.ForeignKey('Patient', null=False, on_delete=models.PROTECT)
    VisitFor = models.ForeignKey('VisitFor', null=False, on_delete=models.PROTECT)
    MKB = models.ForeignKey('mkb.MKBCode', null=False, on_delete=models.PROTECT)
    MKBLink = models.ForeignKey('mkb.MKBCode', null=False, on_delete=models.PROTECT)
    HiredSubst = models.ForeignKey('HiredSubst', null=False, on_delete=models.PROTECT)
    HasPrimaryVisit = models.BooleanField(null=False, default=False)
    HasSecondaryVisit = models.BooleanField(null=False, default=False)
    NoAl = models.CharField(null=False, max_length=6)
    dataAl = models.DateField(null=False)
    time = models.TimeField(null=False)
    Pay = models.BooleanField(null=False, default=True)
    Disadv = models.BooleanField(null=False, default=True)
    Incidentally = models.BooleanField(null=False, default=True)
    Sign = models.BooleanField(null=False, default=True)
    ExamType = models.CharField(null=False, max_length=2)
    Anamnesa = models.TextField(null=True)
    HState = models.TextField(null=True)
    Examine = models.TextField(null=True)
    Therapy_Nonreimburce = models.TextField(null=True)
    Mantu = models.BooleanField(null=True)
    VaccineNo = models.CharField(null=True, max_length=30)
    VaccineType = models.BooleanField(null=True, max_length=5)
    TalonTELK = models.BooleanField(null=False, default=False)
    Izvestie = models.BooleanField(null=False, default=False)
    EtEpikriza = models.BooleanField(null=False, default=False)
    MedicalNote = models.BooleanField(null=False, default=False)
    ProfilactGestWeek = models.IntegerField(null=True)
    CountSIMPConsults = models.IntegerField(null=False, default=0)
    CountVSDSIMPConsults = models.IntegerField(null=False, default=0)
    CountMDD = models.IntegerField(null=False, default=0)
    CountLKK = models.IntegerField(null=False, default=0)
    CountHlist = models.IntegerField(null=False, default=0)
    CountRp = models.IntegerField(null=False, default=0)
    CountHospNapr = models.IntegerField(null=False, default=0)

    @staticmethod
    def lookup(doctor_id, patient_id, mkb_id, mkb_link_id, visit_for_id, hired_subst_id,
               has_primary_visit, has_secondary_visit,
               no_al, data_al, time, pay, disadv, incidentally, sign, exam_type, anamnesa,
               HState, examine, mantu, vaccine_number, vaccine_type, nonreimburce, talon_telk, izvestie,
               et_epikriza, medical_note, gest_week, count_lkk_members, count_simp_consults,
               count_vsd_simp_consults, count_mdd, count_lkk, count_hlist, count_rp, count_hosp_napr
    ):

        try:
            amblist = Amblist.objects.get(Doctor_id=doctor_id, NoAl=no_al)
        except Amblist.DoesNotExist:
            amblist = Amblist()
            amblist.Doctor_id = doctor_id
            amblist.Patient_id = patient_id
            amblist.MKB_id = mkb_id
            amblist.MKBLink_id = mkb_link_id
            amblist.VisitFor_id = visit_for_id
            amblist.HiredSubst_id = hired_subst_id
            amblist.HasPrimaryVisit = has_primary_visit
            amblist.HasSecondaryVisit = has_secondary_visit
            amblist.NoAl = no_al
            amblist.dataAl = data_al
            amblist.time = time
            amblist.Pay = pay
            amblist.Disadv = disadv
            amblist.Incidentally = incidentally
            amblist.Sign = sign
            amblist.ExamType = exam_type
            amblist.Anamnesa = anamnesa
            amblist.HState = HState
            amblist.Examine = examine
            amblist.Mantu = mantu
            amblist.VaccineNo = vaccine_number
            amblist.VaccineType = vaccine_type
            amblist.Therapy_Nonreimburce = nonreimburce

            amblist.TalonTELK = talon_telk
            amblist.Izvestie = izvestie
            amblist.EtEpikriza = et_epikriza
            amblist.MedicalNote = medical_note

            amblist.ProfilactGestWeek = gest_week
            amblist.CountLKKMembers = count_lkk_members

            amblist.CountSIMPConsults = count_simp_consults
            amblist.CountVSDSIMPConsults = count_vsd_simp_consults
            amblist.CountMDD = count_mdd
            amblist.CountLKK = count_lkk
            amblist.CountHlist = count_hlist
            amblist.CountRp = count_rp
            amblist.CountHospNapr = count_hosp_napr

            amblist.save()

        return amblist


class AmblistMKBCode(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist_MKBCode"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKBCode', on_delete=models.PROTECT, null=False)
    MKBLink = models.ForeignKey('mkb.MKBCode', on_delete=models.PROTECT, null=True)


class AmblistLKKMember(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist_LKKMember"

    Amblist = models.ForeignKey('Amblist', null=False, on_delete=models.PROTECT)
    LKKMember = models.ForeignKey('LKKMember', null=False, on_delete=models.PROTECT)

    @staticmethod
    def link(amblist_id, lkk_members):
        if len(lkk_members) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM Amblist_LKKMember WHERE Amblist_id=%s'
            cursor.execute(sql, [amblist_id])

            sql = 'INSERT INTO Amblist_LKKMember(Amblist_id, LKKMember_id) VALUES ' + \
                  ','.join(['({0}, {1})'.format(amblist_id, v) for v in lkk_members])
            cursor.execute(sql, [])
