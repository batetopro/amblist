from django.db import models


class VisitFor(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = 'VisitFor'

    Profilact = models.ForeignKey('Profilact', on_delete=models.PROTECT, null=True)
    Consult = models.BooleanField(null=False)
    Disp = models.BooleanField(null=False)
    Hosp = models.BooleanField(null=True)
    RpHosp = models.BooleanField(null=False)
    LKKVisit = models.BooleanField(null=False)
    Telk = models.BooleanField(null=False)

    @staticmethod
    def lookup(profilact_id, Consult, Disp, Hosp, RpHosp, LKKVisit, Telk):
        return VisitFor.objects.get_or_create(
            Profilact_id=profilact_id,
            Consult=Consult,
            Disp=Disp,
            Hosp=Hosp,
            RpHosp=RpHosp,
            LKKVisit=LKKVisit,
            Telk=Telk
        )[0]
