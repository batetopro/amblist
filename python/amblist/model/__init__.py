from .drug_name import DrugName
from .doctor import Doctor
from .practice import Practice, PracticeLoad
from .amblist import Amblist, AmblistMKBCode, AmblistLKKMember
from .patient import Patient

from .procedure import Procedure, AmblistProcedure
from .sended_from import SendedFrom

from .secondary_visit import SecondaryVisit
from .primary_visit import PrimaryVisit
from .simp_consult import SIMPConsult, SIMPconsultMKBCode
from .drug import Drug, AmblistDrug
from .drug_info import DrugInfo
from .imun import Imun, AmblistImun
from .profilact import Profilact
from .visit_for import VisitFor
from .lkk_member import LkkMember
from .hired_subst import HiredSubst

from .mdd import MDD, kodMDD, MDDkodMDD
from .lkk import CodeSpec, LKK, LKKCodeSpec
from .hlist import HList
from .rp import Rp, Rp_Drug
from .hosp_napr import HospNapr

from .vsdsimp_consult import VSDSIMPConsult, VSDSIMPConsultMKBCode
