from django.db import models, connection


class Drug(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Drugs"

    MKB = models.ForeignKey('mkb.MKBCode', null=False, on_delete=models.PROTECT)
    DrugInfo = models.ForeignKey('DrugInfo', null=False, on_delete=models.PROTECT)
    DrugCode = models.CharField(null=False, max_length=6)

    @staticmethod
    def lookup(mkb_id, drug_code):
        from .drug_info import DrugInfo

        try:
            drug = Drug.objects.get(DrugCode=drug_code, MKB_id=mkb_id)
        except Drug.DoesNotExist:
            drug = Drug()
            drug.MKB_id = mkb_id
            drug.DrugCode = drug_code

            info = DrugInfo.lookup(drug_code)
            if info:
                drug.DrugInfo_id = info.id

            drug.save()

        return drug


class AmblistDrug(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Amblist_Drug"

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    Drug = models.ForeignKey('Drugs', on_delete=models.PROTECT, null=False)

    @staticmethod
    def link(amblist_id, drugs):
        if len(drugs) == 0:
            return

        with connection.cursor() as cursor:
            sql = 'DELETE FROM Amblist_Drug WHERE Amblist_Id=%s'
            cursor.execute(sql, [amblist_id])

            sql = 'INSERT INTO Amblist_Drug (Amblist_Id, Drug_id, Quantity, `Day`) VALUES ' + \
                  ','.join(['({0}, {1}, {2}, {3})'.format(amblist_id, d[0], d[1], d[2]) for d in drugs]) + \
                  ' ON DUPLICATE KEY UPDATE Quantity = Quantity + VALUES(Quantity), `Day` = `Day` + VALUES(`Day`)'
            cursor.execute(sql, [])