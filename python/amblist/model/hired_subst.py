from django.db import models


class HiredSubst(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "Hired_Subst"

    H_S = models.BooleanField(null=False)
    UINH_S = models.CharField(null=False, max_length=255)
    FullNameH_S = models.CharField(null=False, max_length=100)

    @staticmethod
    def lookup(H_S, UINH_S, FullNameH_S):
        try:
            hired = HiredSubst.objects.get(UINH_S=UINH_S)
        except HiredSubst.DoesNotExist:
            hired = HiredSubst()
            hired.H_S = H_S
            hired.UINH_S = UINH_S
            hired.FullNameH_S = FullNameH_S
            hired.save()

        return hired
