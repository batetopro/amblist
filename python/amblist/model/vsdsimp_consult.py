from django.db import models


class VSDSIMPConsult(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = 'VSDSIMPConsult'

    Amblist = models.ForeignKey('Amblist', on_delete=models.PROTECT, null=False)
    kodSpVSD = models.CharField(null=False, max_length=2)
    VSDFor = models.IntegerField(null=False)
    NoNVSD = models.CharField(null=False, max_length=6)
    kodVSD = models.CharField(null=False, max_length=6)
    ACHIcode = models.CharField(null=True, max_length=8)
    HospVSD = models.BooleanField(null=False, default=False)

    @staticmethod
    def lookup(amblist_id, kodSpVSD, VSDFor, NoNVSD, kodVSD, ACHIcode, HospVSD):
        try:
            result = VSDSIMPConsult.objects.get(Amblist_id=amblist_id, NoNVSD=NoNVSD)
        except VSDSIMPConsult.DoesNotExist:
            result = VSDSIMPConsult()
            result.Amblist_id = amblist_id
            result.kodSpVSD = kodSpVSD
            result.VSDFor = VSDFor
            result.NoNVSD = NoNVSD
            result.kodVSD = kodVSD
            result.ACHIcode = ACHIcode
            result.HospVSD = HospVSD
            result.save()

        return result


class VSDSIMPConsultMKBCode(models.Model):
    class Meta:
        app_label = "amblist"
        db_table = "VSDSIMPConsult_MKBCode"

    VSDSIMPConsult = models.ForeignKey('VSDSIMPConsult', on_delete=models.PROTECT, null=False)
    MKB = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=False)
    MKB_Link = models.ForeignKey('mkb.MKB', on_delete=models.PROTECT, null=True)
