import os
import django
from settings import Settings

# from config import Config
# from client.registry import ClientRegistry

SECRET_KEY = Settings.SECRET_KEY
INSTALLED_APPS = Settings.INSTALLED_APPS
DATABASES = Settings.DATABASES
USE_TZ = True
TIME_ZONE = Settings.TIME_ZONE
COMMAND = Settings.COMMAND
LOGGING = Settings.LOGGING

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "boot")

django.setup()
