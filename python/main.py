import sys
import argparse

from boot import COMMAND


def error(commands):
    print("Invalid command")
    print("Available commands:\n\t{0}".format("\n\t".join([k for k in sorted(commands.keys())])))
    exit(2)


def main(argv):
    if len(COMMAND) == 0:
        print("No commands")
        return

    if len(COMMAND) == 1:
        name = list(COMMAND.keys())[0]
        request = argv[1:]
    else:
        if len(argv) < 2:
            error(COMMAND)

        name = argv[1]
        if name not in COMMAND:
            error(COMMAND)

        request = argv[2:]

    command = COMMAND[name]

    parser = argparse.ArgumentParser()
    for k in command["arguments"]:
        parser.add_argument(k, **command["arguments"][k])

    args = vars(parser.parse_args(request))
    runner = command["runner"]()
    runner.execute(**args)


if __name__ == "__main__":
    main(sys.argv)
