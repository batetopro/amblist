import os
import json
import logging
import scrapy
from scrapy.http import Request


class MkbSpider(scrapy.Spider):
    name = "mkb_spider"

    @staticmethod
    def get_settings():
        log_path = os.path.join('.', 'logs', 'spiders')
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_path = os.path.join(log_path, 'mkb_codes.log')

        return {
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
            'LOG_ENABLED': True,
            'LOG_ENCODING': 'utf8',
            'LOG_FILE': log_path,
            'LOG_LEVEL': logging.INFO,
            'LOG_FORMAT': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            'LOG_STDOUT': True,
        }

    allowed_domains = ['mediately.co']

    def start_requests(self):
        from .model import MKBChapter

        dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
        file_path = os.path.join(dir_path, 'data', 'mediately_icd.json')

        url_base = 'https://mediately.co/api/v4/icd/bg/sets/?chapter={0}'

        with open(file_path, 'r') as file:
            data = json.load(file)
            for item in data:
                chapter = MKBChapter.lookup(item['codes'], item['name'])
                yield Request(url_base.format(item['codes']), self.parse_chapter, meta={'chapter_id': chapter.id})

    def parse_chapter(self, response):
        from .model import MKBSet

        logger = logging.getLogger('command')
        logger.info("[mkb_codes] {0}".format(response.url))

        url_base = 'https://mediately.co/api/v4/icd/bg/classifications/?set={0}'

        for item in json.loads(response.body_as_unicode()):
            mkb_set = MKBSet.lookup(response.meta['chapter_id'], item['codes'], item['name'])
            yield Request(url_base.format(item['codes']), self.parse_set, meta={
                'chapter_id': response.meta['chapter_id'],
                'set_id': mkb_set.id
            })

    def parse_set(self, response):
        logger = logging.getLogger('command')
        logger.info("[mkb_codes] {0}".format(response.url))

        url_base = 'https://mediately.co/api/v4/icd/bg/classifications/{0}/'

        for item in json.loads(response.body_as_unicode()):
            yield Request(url_base.format(item['code']), self.parse_item, meta=response.meta)

    def parse_item(self, response):
        from .model import MKBCode, MKBMorbidity

        logger = logging.getLogger('command')
        logger.info("[mkb_codes] {0}".format(response.url))

        data = json.loads(response.body_as_unicode())

        morbidity_id = None
        if data['morbidity']:
            morbidity_id = MKBMorbidity.lookup(data['morbidity']['code'], data['morbidity']['name']).id

        MKBCode.load(
            response.meta['chapter_id'],
            response.meta['set_id'],
            morbidity_id,
            data['code'],
            data['name'],
            data['name_latin']
        )
