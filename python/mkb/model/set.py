from django.db import models


class MKBSet(models.Model):
    class Meta:
        app_label = "mkb"
        db_table = "MKBSet"

    chapter = models.ForeignKey('MKBChapter', null=False, on_delete=models.PROTECT)
    code = models.CharField(max_length=20, null=False)
    name = models.CharField(max_length=255, null=False)

    @staticmethod
    def lookup(chapter_id, code, name):
        try:
            mkb_set = MKBSet.objects.get(code=code)
        except MKBSet.DoesNotExist:
            mkb_set = MKBSet()
            mkb_set.chapter_id = chapter_id
            mkb_set.code = code
            mkb_set.name = name
            mkb_set.save()

        return mkb_set
