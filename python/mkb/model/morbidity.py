from django.db import models


class MKBMorbidity(models.Model):
    class Meta:
        app_label = "mkb"
        db_table = "MKBMorbidity"

    code = models.IntegerField(null=False)
    name = models.CharField(max_length=255, null=False)

    @staticmethod
    def lookup(code, name):
        try:
            morbidity = MKBMorbidity.objects.get(code=code)
        except MKBMorbidity.DoesNotExist:
            morbidity = MKBMorbidity()
            morbidity.code = code
            morbidity.name = name
            morbidity.save()

        return morbidity
