from django.db import models


class MKBChapter(models.Model):
    class Meta:
        app_label = "mkb"
        db_table = "MKBChapter"

    code = models.CharField(max_length=20, null=False)
    name = models.CharField(max_length=255, null=False)

    @staticmethod
    def lookup(code, name):
        try:
            chapter = MKBChapter.objects.get(code=code)
        except MKBChapter.DoesNotExist:
            chapter = MKBChapter()
            chapter.code = code
            chapter.name = name
            chapter.save()

        return chapter
