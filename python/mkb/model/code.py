from django.db import models


class MKBCode(models.Model):
    class Meta:
        app_label = "mkb"
        db_table = "MKBCode"

    chapter = models.ForeignKey('MKBChapter', on_delete=models.PROTECT, null=True)
    set = models.ForeignKey('MKBSet', on_delete=models.PROTECT, null=True)
    morbidity = models.ForeignKey('MKBMorbidity', on_delete=models.PROTECT, null=True)
    code = models.CharField(max_length=20, null=False)
    name = models.CharField(max_length=255, null=True)
    name_latin = models.CharField(max_length=255, null=True)
    is_found = models.BooleanField(null=False, default=False)

    @staticmethod
    def load(chapter_id, set_id, morbidity_id, code, name, name_latin):
        try:
            result = MKBCode.objects.get(code=code)
        except MKBCode.DoesNotExist:
            result = MKBCode()
            result.chapter_id = chapter_id
            result.set_id = set_id
            result.morbidity_id = morbidity_id
            result.code = code
            result.name = name
            result.name_latin = name_latin
            result.is_found = False
            result.save()
        return result

    @staticmethod
    def lookup(code, name):
        try:
            result = MKBCode.objects.get(code=code)
            if result.name is None:
                result.name = name
                result.save()
        except MKBCode.DoesNotExist:
            result = MKBCode()
            result.code = code
            result.name = name
            result.is_found = True
            result.save()

        return result
