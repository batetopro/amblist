from django.db import connection


class MKBCodeRegistry:
    def __init__(self):
        self.data = dict()

    def load(self):
        from ..model import MKBCode

        for mkb in MKBCode.objects.all():
            self.data[mkb.code] = mkb

    def lookup(self, code, name=None):
        if code in self.data:
            return self.data[code]

        from ..model import MKBCode

        result = MKBCode.lookup(code, name)
        self.data[code] = result

        return result

    def lookup_both(self, codes):
        codes = codes.strip()

        if ' ' not in codes:
            return self.lookup(codes).id, None

        parts = codes.split(' ')

        return self.lookup(parts[0]).id, self.lookup(parts[1]).id

    @staticmethod
    def link(target_id, mkb_codes, db_table, db_column):
        if len(mkb_codes) == 0:
            return

        values = []
        for mc in mkb_codes:
            if mc[1] is None:
                values.append([mc[0], 'NULL'])
            else:
                values.append([mc[0], mc[1]])

        with connection.cursor() as cursor:
            sql = 'DELETE FROM {0} WHERE {1}=%s'.format(db_table, db_column)
            cursor.execute(sql, [target_id])

            sql = 'INSERT IGNORE INTO {0}({1}, MKB_id, MKBLink_id) VALUES '.format(db_table, db_column) + \
                  ','.join(['({0}, {1}, {2})'.format(target_id, p[0], p[1]) for p in values])
            cursor.execute(sql, [])
