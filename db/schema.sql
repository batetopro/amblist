CREATE TABLE IF NOT EXISTS MKBChapter(
    id INT unsigned auto_increment PRIMARY KEY,
    code VARCHAR(20) NOT NULL,
    name VARCHAR(255) NOT NULL,
    usages INT unsigned NOT NULL DEFAULT 0
);


CREATE TABLE IF NOT EXISTS MKBSet(
    id INT unsigned auto_increment PRIMARY KEY,
    chapter_id INT unsigned NOT NULL,
    code VARCHAR(20) NOT NULL,
    name VARCHAR(255) NOT NULL,
    usages INT unsigned NOT NULL DEFAULT 0,
    FOREIGN KEY(chapter_id) REFERENCES MKBChapter(id)
);


CREATE TABLE IF NOT EXISTS MKBMorbidity(
    id int unsigned auto_increment PRIMARY KEY,
    code int unsigned NOT NULL,
    name varchar(255) NOT NULL
);


CREATE TABLE IF NOT EXISTS MKBCode(
    id INT unsigned auto_increment PRIMARY KEY,
    chapter_id INT unsigned NULL,
    set_id INT unsigned NULL,
    morbidity_id INT unsigned NULL,
    code VARCHAR(20) NOT NULL,
    name VARCHAR(255) NULL,
    name_latin VARCHAR(255) NULL,
    is_found TINYINT unsigned NOT NULL DEFAULT 0,
    usages INT unsigned NOT NULL DEFAULT 0,
    FOREIGN KEY(chapter_id) REFERENCES MKBChapter(id),
    FOREIGN KEY(set_id) REFERENCES MKBSet(id),
    FOREIGN KEY(morbidity_id) REFERENCES MKBMorbidity(id)
);


CREATE TABLE IF NOT EXISTS DrugName (
    id int unsigned auto_increment PRIMARY KEY,
    Name varchar(255) NOT NULL
);


CREATE TABLE IF NOT EXISTS DrugInfo(
    id INT unsigned auto_increment PRIMARY KEY,
    DrugName_id int unsigned NOT NULL,
    Code VARCHAR(6) NOT NULL,
    Name VARCHAR(255) NOT NULL,
    MarketName VARCHAR(255) NOT NULL,
    Form VARCHAR(255) NOT NULL,
    Quantity VARCHAR(60) NOT NULL,
    Unit VARCHAR(60) NOT NULL,
    FOREIGN KEY(DrugName_id) REFERENCES DrugName(id)
);


CREATE TABLE IF NOT EXISTS Practice (
    id INT unsigned auto_increment PRIMARY KEY,
    PracticeCode VARCHAR(120) NOT NULL,
    PracticeName VARCHAR(150) NULL,
    NZOKCode VARCHAR(10) NULL,
    ContractNo VARCHAR(6) NULL,
    ContractDate DATE NULL,
    ContrHA TINYINT unsigned NULL
);


CREATE TABLE IF NOT EXISTS PracticeLoad (
    id INT unsigned auto_increment PRIMARY KEY,
    Practice_id INT unsigned NOT NULL,
    DateFrom DATE NOT NULL,
    DateTo DATE NOT NULL,
    FileName VARCHAR(255) NOT NULL,
    FOREIGN KEY(Practice_id) REFERENCES Practice(id)
);


CREATE TABLE IF NOT EXISTS Doctor (
    id INT unsigned auto_increment PRIMARY KEY,
    Practice_id INT unsigned NOT NULL,
    UIN VARCHAR(255) NOT NULL,
    EGN VARCHAR(10) NOT NULL,
    FullName VARCHAR(100) NOT NULL,
    SIMPCode VARCHAR(2) NOT NULL,
    FOREIGN KEY (Practice_id) REFERENCES Practice(id)
);


CREATE TABLE IF NOT EXISTS Patient (
    id INT unsigned auto_increment PRIMARY KEY,
    EGN VARCHAR(64) NOT NULL,
    LNCH_dateBirth DATE NULL,
    LNCH_Sex TINYINT unsigned NULL,
    SS_No VARCHAR(20) NULL,
    RZOK VARCHAR(2) NOT NULL,
    ZdrRajon VARCHAR(2) NOT NULL,
    NameGiven VARCHAR(30) NOT NULL,
    NameSur VARCHAR(30) NULL,
    NameFamily VARCHAR(30) NOT NULL,
    Address VARCHAR(100) NULL,
    IsHealthInsurance INT unsigned NOT NULL,
    COUNTRY_COUNTRYCODE VARCHAR(2) NULL,
    COUNTRY_COUNTRYIDNO VARCHAR(20) NULL,
    COUNTRY_Type_CERTIFICATE VARCHAR(6) NULL,
    COUNTRY_DateIssue DATE NULL,
    COUNTRY_DateFrom DATE NULL,
    COUNTRY_DateTo DATE NULL,
    COUNTRY_EHIC_No VARCHAR(20) NULL,
    COUNTRY_PersID_No VARCHAR(20) NULL
);


CREATE TABLE IF NOT EXISTS SendedFrom(
    id INT unsigned auto_increment PRIMARY KEY,
    PracticeCode VARCHAR(255) NOT NULL,
    UIN VARCHAR(255) NOT NULL,
    SIMPCode VARCHAR(2) NOT NULL,
    H_S TINYINT unsigned NULL,
    UINH_S VARCHAR(255) NULL
);


CREATE TABLE IF NOT EXISTS Hired_Subst(
    id INT unsigned auto_increment PRIMARY KEY,
    H_S TINYINT unsigned NOT NULL,
    UINH_S VARCHAR(255) NOT NULL,
    FullNameH_S VARCHAR(100) NOT NULL
);


CREATE TABLE IF NOT EXISTS LKKMember (
    id INT unsigned auto_increment PRIMARY KEY,
    UIN VARCHAR(255) NOT NULL,
    SIMPCode VARCHAR(2) NOT NULL
);


CREATE TABLE IF NOT EXISTS Profilact(
    id INT unsigned auto_increment PRIMARY KEY,
    Child TINYINT unsigned NOT NULL DEFAULT 0,
    Mother TINYINT unsigned NOT NULL DEFAULT 0,
    Over18 TINYINT unsigned NOT NULL DEFAULT 0,
    Risk TINYINT unsigned NOT NULL DEFAULT 0
);


CREATE TABLE IF NOT EXISTS Imun(
    id INT unsigned auto_increment PRIMARY KEY,
    Code VARCHAR(2) NOT NULL
);


CREATE TABLE IF NOT EXISTS Drugs(
    id INT unsigned auto_increment PRIMARY KEY,
    MKB_id INT unsigned NOT NULL,
    DrugInfo_id int unsigned NULL,
    DrugCode VARCHAR(6) NOT NULL,
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(DrugInfo_id) REFERENCES DrugInfo(id)
);


CREATE TABLE IF NOT EXISTS `Procedure`(
    id INT unsigned auto_increment PRIMARY KEY,
    imeP VARCHAR(250) NOT NULL,
    kodP VARCHAR(6) NOT NULL,
    ACHIcode VARCHAR(8) NULL
);


CREATE TABLE IF NOT EXISTS VisitFor (
    id INT unsigned auto_increment PRIMARY KEY,
    Profilact_id INT unsigned NULL,
    Consult TINYINT unsigned NOT NULL DEFAULT 0,
    Disp TINYINT unsigned NOT NULL DEFAULT 0,
    Hosp TINYINT unsigned NULL,
    RpHosp TINYINT unsigned NOT NULL DEFAULT 0,
    LKKVisit TINYINT unsigned NOT NULL DEFAULT 0,
    Telk TINYINT unsigned NOT NULL DEFAULT 0,
    FOREIGN KEY(Profilact_id) REFERENCES Profilact(id)
);


CREATE TABLE IF NOT EXISTS Amblist (
    id INT unsigned auto_increment PRIMARY KEY,
    Doctor_id INT unsigned NOT NULL,
    Patient_id INT unsigned NOT NULL,
    VisitFor_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    HiredSubst_id INT unsigned NULL,
    HasPrimaryVisit TINYINT unsigned NOT NULL DEFAULT 0,
    HasSecondaryVisit TINYINT unsigned NOT NULL DEFAULT 0,
    NoAl VARCHAR(6) NOT NULL,
    dataAl DATE NOT NULL,
    time TIME NOT NULL,
    Pay TINYINT unsigned NOT NULL DEFAULT 1,
    Disadv TINYINT unsigned NOT NULL DEFAULT 0,
    Incidentally TINYINT unsigned NOT NULL DEFAULT 0,
    Anamnesa MEDIUMTEXT NULL,
    HState MEDIUMTEXT NULL,
    Examine MEDIUMTEXT NULL,
    ExamType VARCHAR(2) NULL,
    Sign TINYINT unsigned NOT NULL,
    Therapy_Nonreimburce text NULL,
    Mantu TINYINT unsigned NULL,
    VaccineNo VARCHAR(30) NULL,
    VaccineType VARCHAR(5) NULL,
    TalonTELK TINYINT unsigned NULL,
    Izvestie TINYINT unsigned NULL,
    EtEpikriza TINYINT unsigned NULL,
    MedicalNote TINYINT unsigned NULL,
    ProfilactGestWeek INT unsigned NULL,
    CountLKKMembers TINYINT unsigned NOT NULL DEFAULT 0,
    CountSIMPConsults TINYINT unsigned NOT NULL DEFAULT 0,
    CountVSDSIMPConsults TINYINT unsigned NOT NULL DEFAULT 0,
    CountMDD TINYINT unsigned NOT NULL DEFAULT 0,
    CountLKK TINYINT unsigned NOT NULL DEFAULT 0,
    CountHlist TINYINT unsigned NOT NULL DEFAULT 0,
    CountRp TINYINT unsigned NOT NULL DEFAULT 0,
    CountHospNapr TINYINT unsigned NOT NULL DEFAULT 0,
    FOREIGN KEY(Doctor_id) REFERENCES Doctor(id),
    FOREIGN KEY(Patient_id) REFERENCES Patient(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id),
    FOREIGN KEY(HiredSubst_id) REFERENCES Hired_Subst(id),
    FOREIGN KEY(VisitFor_id) REFERENCES VisitFor(id)
);


CREATE TABLE IF NOT EXISTS PrimaryVisit(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    SendedFrom_id INT unsigned NOT NULL,
    VidNapr INT unsigned NOT NULL,
    NoNapr VARCHAR(6) NOT NULL,
    NaprFor INT NOT NULL,
    dateNapr DATE NOT NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(SendedFrom_id) REFERENCES SendedFrom(id)
);


CREATE TABLE IF NOT EXISTS SecondaryVisit(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    NoAmbL VARCHAR(6) NULL,
    dateAmbL DATE NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS Amblist_LKKMember (
    Amblist_id INT unsigned NOT NULL,
    LKKMember_id INT unsigned NOT NULL,
    PRIMARY KEY(Amblist_id, LKKMember_id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(LKKMember_id) REFERENCES LKKMember(id)
);


CREATE TABLE IF NOT EXISTS Amblist_Imun (
    Amblist_id INT unsigned NOT NULL,
    Imun_id INT unsigned NOT NULL,
    PRIMARY KEY(Amblist_id, Imun_id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(Imun_id) REFERENCES Imun(id)
);


CREATE TABLE IF NOT EXISTS Amblist_Drug (
    Amblist_id INT unsigned NOT NULL,
    Drug_id INT unsigned NOT NULL,
    Quantity INT unsigned NOT NULL,
    `Day` INT unsigned NOT NULL,
    PRIMARY KEY(Amblist_id, Drug_id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(Drug_id) REFERENCES Drugs(id)
);


CREATE TABLE IF NOT EXISTS Amblist_MKBCode (
    Amblist_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    PRIMARY KEY(Amblist_id, MKB_id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS Amblist_Procedure (
    Amblist_id INT unsigned NOT NULL,
    Procedure_id INT unsigned NOT NULL,
    CountP INT unsigned NOT NULL,
    PRIMARY KEY(Amblist_id, Procedure_id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(Procedure_id) REFERENCES `Procedure` (id)
);


CREATE TABLE IF NOT EXISTS SIMPConsult(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    kodSp VARCHAR(2) NOT NULL,
    SpFor INT unsigned NOT NULL,
    NoN VARCHAR(6) NOT NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS VSDSIMPConsult(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    kodSpVSD VARCHAR(2) NOT NULL,
    VSDFor TINYINT unsigned NOT NULL,
    NoNVSD VARCHAR(6) NOT NULL,
    kodVSD VARCHAR(6) NOT NULL,
    ACHIcode VARCHAR(6) NULL,
    HospVSD TINYINT unsigned NOT NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS kodMDD(
    id INT unsigned auto_increment PRIMARY KEY,
    Code VARCHAR(6) NOT NULL
);


CREATE TABLE IF NOT EXISTS MDD(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    NoMDD VARCHAR(6) NOT NULL,
    MDDFor INT unsigned NOT NULL,
    ACHIcode VARCHAR(13) NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id)
);


CREATE TABLE IF NOT EXISTS CodeSpec(
    id INT unsigned auto_increment PRIMARY KEY,
    Code VARCHAR(2) NOT NULL,
    Name VARCHAR(64) NOT NULL
);


CREATE TABLE IF NOT EXISTS LKK(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    NoLKK VARCHAR(6) NOT NULL,
    TypeLKK INT unsigned NOT NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS Rp(
    id INT unsigned auto_increment PRIMARY KEY,
    Patient_id INT unsigned NOT NULL,
    RpBook VARCHAR(7) NULL,
    FOREIGN KEY(Patient_id) REFERENCES Patient(id)
);


CREATE TABLE IF NOT EXISTS HList(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    NoBl VARCHAR(12) NOT NULL,
    days INT unsigned NOT NULL,
    HLFromDate DATE NOT NULL,
    HLToDate DATE NOT NULL,
    HLtype TINYINT unsigned NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id)
);


CREATE TABLE IF NOT EXISTS HospNapr(
    id INT unsigned auto_increment PRIMARY KEY,
    Amblist_id INT unsigned NOT NULL,
    PathNum VARCHAR(5) NOT NULL,
    NaprNo VARCHAR(6) NOT NULL,
    DateNapr DATE NULL,
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS SIMPConsult_MKBCode(
    SIMPConsult_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    PRIMARY KEY(SIMPConsult_id, MKB_id),
    FOREIGN KEY(SIMPConsult_id) REFERENCES SIMPConsult(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id)
);


CREATE TABLE IF NOT EXISTS VSDSIMPConsult_MKBCode(
    VSDSIMPConsult_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    PRIMARY KEY(VSDSIMPConsult_id, MKB_id),
    FOREIGN KEY(VSDSIMPConsult_id) REFERENCES VSDSIMPConsult(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id)
);


CREATE TABLE IF NOT EXISTS HospNapr_MKBCode(
    HospNapr_id INT unsigned NOT NULL,
    MKB_id INT unsigned NOT NULL,
    MKBLink_id INT unsigned NULL,
    PRIMARY KEY(HospNapr_id, MKB_id),
    FOREIGN KEY(HospNapr_id) REFERENCES HospNapr(id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(MKBLink_id) REFERENCES MKBCode(id)
);


CREATE TABLE IF NOT EXISTS MDD_kodMDD(
    MDD_id INT unsigned NOT NULL,
    kodMDD_id INT unsigned NOT NULL,
    PRIMARY KEY(MDD_id, kodMDD_id),
    FOREIGN KEY(kodMDD_id) REFERENCES kodMDD(id),
    FOREIGN KEY(MDD_id) REFERENCES MDD(id)
);


CREATE TABLE IF NOT EXISTS LKK_CodeSpec(
    LKK_id INT unsigned NOT NULL,
    CodeSpec_id INT unsigned NOT NULL,
    PRIMARY KEY(LKK_id, CodeSpec_id),
    FOREIGN KEY(LKK_id) REFERENCES LKK(id),
    FOREIGN KEY(CodeSpec_id) REFERENCES CodeSpec(id)
);


CREATE TABLE IF NOT EXISTS Rp_Drug(
    Rp_id INT unsigned NOT NULL,
    Amblist_id INT unsigned NOT NULL,
    Drug_id INT unsigned NOT NULL,
    prescNum INT unsigned NOT NULL,
    Quantity INT unsigned NOT NULL,
    `Day` INT unsigned NOT NULL,
    PRIMARY KEY(Rp_id, Amblist_id, Drug_id),
    FOREIGN KEY(Rp_id) REFERENCES Rp(id),
    FOREIGN KEY(Drug_id) REFERENCES Drugs(id),
    FOREIGN KEY(Amblist_id) REFERENCES Amblist(id)
);


CREATE TABLE IF NOT EXISTS mkbchapter_usage(
    chapter_id int unsigned primary key,
    amblist_primary int unsigned default 0,
    amblist_primary_linked int unsigned default 0,
    amblist_secondary int unsigned default 0,
    amblist_secondary_linked int unsigned default 0,
    hlist int unsigned default 0,
    simpconsult int unsigned default 0,
    simpconsult_linked int unsigned default 0,
    vsdsimpconsult int unsigned default 0,
    vsdsimpconsult_linked int unsigned default 0,
    mdd int unsigned default 0,
    mdd_linked int unsigned default 0,
    drugs int unsigned default 0,
    hospnapr int unsigned default 0,
    hospnapr_linked int unsigned default 0
);


CREATE TABLE IF NOT EXISTS mkbset_usage(
    set_id int unsigned primary key,
    amblist_primary int unsigned default 0,
    amblist_primary_linked int unsigned default 0,
    amblist_secondary int unsigned default 0,
    amblist_secondary_linked int unsigned default 0,
    hlist int unsigned default 0,
    simpconsult int unsigned default 0,
    simpconsult_linked int unsigned default 0,
    vsdsimpconsult int unsigned default 0,
    vsdsimpconsult_linked int unsigned default 0,
    mdd int unsigned default 0,
    mdd_linked int unsigned default 0,
    drugs int unsigned default 0,
    hospnapr int unsigned default 0,
    hospnapr_linked int unsigned default 0
);


CREATE TABLE IF NOT EXISTS mkbcode_usage(
    MKB_id int unsigned primary key,
    amblist_primary int unsigned default 0,
    amblist_primary_linked int unsigned default 0,
    amblist_secondary int unsigned default 0,
    amblist_secondary_linked int unsigned default 0,
    hlist int unsigned default 0,
    simpconsult int unsigned default 0,
    simpconsult_linked int unsigned default 0,
    vsdsimpconsult int unsigned default 0,
    vsdsimpconsult_linked int unsigned default 0,
    mdd int unsigned default 0,
    mdd_linked int unsigned default 0,
    drugs int unsigned default 0,
    hospnapr int unsigned default 0,
    hospnapr_linked int unsigned default 0
);


CREATE TABLE IF NOT EXISTS mkbcode_procedure_cooccurrence(
    MKB_id int unsigned,
    procedure_id int unsigned,
    count int unsigned,
    procedureCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    PRIMARY KEY(MKB_id, procedure_id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(procedure_id) REFERENCES `procedure`(id)
);


CREATE TABLE IF NOT EXISTS mkbset_procedure_cooccurrence(
    set_id int unsigned,
    procedure_id int unsigned,
    count int unsigned,
    procedureCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    PRIMARY KEY(set_id, procedure_id),
    FOREIGN KEY(set_id) REFERENCES MKBSet(id),
    FOREIGN KEY(procedure_id) REFERENCES `procedure`(id)
);


CREATE TABLE IF NOT EXISTS mkbchpater_procedure_cooccurrence(
    chapter_id int unsigned PRIMARY KEY auto_increment,
    procedure_id int unsigned,
    count int unsigned,
    procedureCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    FOREIGN KEY(chapter_id) REFERENCES MKBChapter(id),
    FOREIGN KEY(procedure_id) REFERENCES `procedure`(id)
);


CREATE TABLE IF NOT EXISTS mkbcode_drug_cooccurrence(
    MKB_id int unsigned,
    drug_id int unsigned,
    count int unsigned,
    drugCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    PRIMARY KEY(MKB_id, drug_id),
    FOREIGN KEY(MKB_id) REFERENCES MKBCode(id),
    FOREIGN KEY(drug_id) REFERENCES drugs(id)
);


CREATE TABLE IF NOT EXISTS mkbset_drug_cooccurrence(
    set_id int unsigned,
    drug_id int unsigned,
    count int unsigned,
    drugCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    PRIMARY KEY(set_id, drug_id),
    FOREIGN KEY(set_id) REFERENCES MKBSet(id),
    FOREIGN KEY(drug_id) REFERENCES drugs(id)
);


CREATE TABLE IF NOT EXISTS mkbchapter_drug_cooccurrence(
    chapter_id int unsigned,
    drug_id int unsigned,
    count int unsigned,
    drugCount int unsigned,
    diagnoseCount int unsigned,
    totalCount int unsigned,
    PMI decimal(14, 6),
    PRIMARY KEY(chapter_id, drug_id),
    FOREIGN KEY(chapter_id) REFERENCES MKBChapter(id),
    FOREIGN KEY(drug_id) REFERENCES drugs(id)
);