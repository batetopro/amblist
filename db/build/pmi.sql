INSERT INTO mkbcode_procedure_cooccurrence(MKB_id, procedure_id, count, procedureCount, diagnoseCount, totalCount, PMI)
SELECT a.MKB_id, ap.Procedure_id, COUNT(*) AS count, procedureCount, diagnoseCount, totalCount, log((count(*) / totalCount) / ((diagnoseCount / totalCount) * (procedureCount / totalCount))) AS PMI
FROM amblist_procedure ap
JOIN amblist a on ap.Amblist_id = a.id
JOIN (SELECT COUNT(*) AS procedureCount, Procedure_id FROM amblist_procedure GROUP BY Procedure_id) P USING (Procedure_id)
JOIN (
    SELECT COUNT(*) AS diagnoseCount, MKB_id
    FROM amblist_procedure apD
    JOIN amblist aD ON apD.Amblist_id = aD.id
    GROUP BY aD.MKB_id
) A USING (MKB_id)
JOIN (SELECT COUNT(*) AS totalCount FROM amblist_procedure) T
GROUP BY a.MKB_id, ap.Procedure_id
ORDER BY a.MKB_id, PMI DESC;


INSERT INTO mkbcode_drug_cooccurrence(MKB_id, drug_id, count, drugCount, diagnoseCount, totalCount, PMI)
SELECT a.MKB_id, ad.Drug_id, COUNT(*) AS count, drugCount, diagnoseCount, totalCount, log((count(*) / totalCount) / ((diagnoseCount / totalCount) * (drugCount / totalCount))) AS PMI
FROM amblist_drug ad
JOIN amblist a on ad.Amblist_id = a.id
JOIN (SELECT Drug_id, COUNT(*) AS drugCount FROM amblist_drug GROUP BY Drug_id) P USING (Drug_id)
JOIN (SELECT MKB_id, COUNT(*) AS diagnoseCount FROM amblist_drug _ad JOIN amblist _a ON _ad.Amblist_id = _a.id GROUP BY _a.MKB_id) A USING (MKB_id)
JOIN (SELECT COUNT(*) AS totalCount FROM amblist_drug) T
GROUP BY a.MKB_id, ad.Drug_id
ORDER BY a.MKB_id, PMI DESC;


INSERT INTO mkbset_procedure_cooccurrence(set_id, procedure_id, count, procedureCount, diagnoseCount, totalCount, PMI)
SELECT c.set_id, ap.Procedure_id, COUNT(*) AS count, procedureCount, diagnoseCount, totalCount, log((count(*) / totalCount) / ((diagnoseCount / totalCount) * (procedureCount / totalCount))) AS PMI
FROM amblist_procedure ap
JOIN amblist a ON ap.Amblist_id = a.id
JOIN mkbcode c ON a.MKB_id = c.id
JOIN (SELECT COUNT(*) as procedureCount, Procedure_id FROM amblist_procedure GROUP BY Procedure_id) P USING (Procedure_id)
JOIN (
    SELECT COUNT(*) AS diagnoseCount, set_id
    FROM amblist_procedure apD
    JOIN amblist aD ON apD.Amblist_id = aD.id
    JOIN mkbcode cD ON cD.id = aD.MKB_id 
    GROUP BY set_id
) A USING (set_id)
JOIN (select COUNT(*) AS totalCount FROM amblist_procedure) T
GROUP BY c.set_id, ap.Procedure_id
ORDER BY c.set_id, PMI DESC;


INSERT INTO mkbset_drug_cooccurrence(MKB_id, drug_id, count, drugCount, diagnoseCount, totalCount, PMI)
SELECT a.set_id, ad.Drug_id, COUNT(*) AS count, drugCount, diagnoseCount, totalCount, log((count(*) / totalCount) / ((diagnoseCount / totalCount) * (drugCount / totalCount))) AS PMI
FROM amblist_drug ad
JOIN amblist a on ad.Amblist_id = a.id
JOIN MKBCode c on ad.MKB_id = c.id
JOIN (SELECT Drug_id, COUNT(*) AS drugCount FROM amblist_drug GROUP BY Drug_id) P USING (Drug_id)
JOIN (SELECT MKB_id, COUNT(*) AS diagnoseCount FROM amblist_drug _ad JOIN amblist _a ON _ad.Amblist_id = _a.id JOIN MKBCode _c GROUP BY _c.set_id) A USING (set_id)
JOIN (SELECT COUNT(*) AS totalCount FROM amblist_drug) T
GROUP BY a.MKB_id, ad.Drug_id
ORDER BY a.MKB_id, PMI DESC;


INSERT INTO mkbchapter_procedure_cooccurrence(chapter_id, procedure_id, count, procedureCount, diagnoseCount, totalCount, PMI)
SELECT c.chapter_id, ap.Procedure_id, COUNT(*) AS count, procedureCount, diagnoseCount, totalCount, log((count(*) / totalCount) / ((diagnoseCount / totalCount) * (procedureCount / totalCount))) AS PMI
FROM amblist_procedure ap
JOIN amblist a ON ap.Amblist_id = a.id
JOIN mkbcode c ON a.MKB_id = c.id
JOIN (SELECT COUNT(*) as procedureCount, Procedure_id FROM amblist_procedure GROUP BY Procedure_id) P USING (Procedure_id)
JOIN (
    SELECT COUNT(*) AS diagnoseCount, chapter_id
    FROM amblist_procedure apD
    JOIN amblist aD ON apD.Amblist_id = aD.id
    JOIN mkbcode cD ON cD.id = aD.MKB_id 
    GROUP BY chapter_id
) A USING (chapter_id)
JOIN (select COUNT(*) AS totalCount FROM amblist_procedure) T
GROUP BY c.chapter_id, ap.Procedure_id
ORDER BY c.chapter_id, PMI DESC;


