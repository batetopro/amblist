TRUNCATE mkbcode_usage;
INSERT INTO mkbcode_usage (MKB_id) SELECT id FROM mkbcode;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM amblist GROUP BY MKB_id) T USING(MKB_id) SET U.amblist_primary=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM amblist GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id SET U.amblist_primary_linked=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM amblist_mkbcode GROUP BY MKB_id) T USING(MKB_id) SET U.amblist_secondary=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM amblist_mkbcode GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id SET U.amblist_secondary_linked=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM hlist GROUP BY MKB_id) T USING(MKB_id) SET U.hlist=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM simpconsult_mkbcode GROUP BY MKB_id) T USING(MKB_id) SET U.simpconsult=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM simpconsult_mkbcode GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id SET U.simpconsult_linked=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM vsdsimpconsult_mkbcode GROUP BY MKB_id) T USING(MKB_id) SET U.vsdsimpconsult=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM vsdsimpconsult_mkbcode GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id SET U.vsdsimpconsult_linked=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM mdd GROUP BY MKB_id) T ON U.MKB_id=T.MKB_id SET U.mdd=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM mdd GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id SET U.mdd_linked=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM drugs GROUP BY MKB_id) T ON U.MKB_id=T.MKB_id SET U.drugs=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKB_id FROM hospnapr_mkbcode GROUP BY MKB_id) T ON U.MKB_id=T.MKB_id SET U.hospnapr=count;
UPDATE mkbcode_usage U JOIN (SELECT COUNT(*) AS count, MKBLink_id FROM hospnapr_mkbcode GROUP BY MKBLink_id) T ON U.MKB_id=T.MKBLink_id  SET U.hospnapr_linked=count;
UPDATE mkbcode c
JOIN mkbcode_usage U ON c.id = U.MKB_id
SET c.usages = 
    U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;


TRUNCATE mkbset_usage;
INSERT INTO mkbset_usage(set_id) SELECT id FROM mkbset;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM amblist a JOIN mkbcode c ON a.MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.amblist_primary=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM amblist a JOIN mkbcode c ON a.MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id)  SET U.amblist_primary_linked=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM amblist_mkbcode a JOIN mkbcode c ON a.MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.amblist_secondary=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM amblist_mkbcode a JOIN mkbcode c ON a.MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.amblist_secondary_linked=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM hlist JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.hlist=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM simpconsult_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.simpconsult=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM simpconsult_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.simpconsult_linked=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM vsdsimpconsult_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.vsdsimpconsult=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM vsdsimpconsult_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.vsdsimpconsult_linked=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM mdd JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.mdd=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM mdd JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.mdd_linked=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM drugs JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.drugs=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM hospnapr_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.hospnapr=count;
UPDATE mkbset_usage U JOIN (SELECT COUNT(*) AS count, c.set_id FROM hospnapr_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.set_id) T USING(set_id) SET U.hospnapr_linked=count;
UPDATE mkbset s
JOIN mkbset_usage U ON s.id = U.set_id 
SET s.usages = 
    U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;


TRUNCATE mkbchapter_usage;
INSERT INTO mkbchapter_usage(chapter_id) SELECT id FROM mkbchapter;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM amblist a JOIN mkbcode c ON a.MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.amblist_primary=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM amblist a JOIN mkbcode c ON a.MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id)  SET U.amblist_primary_linked=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM amblist_mkbcode a JOIN mkbcode c ON a.MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.amblist_secondary=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM amblist_mkbcode a JOIN mkbcode c ON a.MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.amblist_secondary_linked=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM hlist JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.hlist=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM simpconsult_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.simpconsult=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM simpconsult_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.simpconsult_linked=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM vsdsimpconsult_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.vsdsimpconsult=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM vsdsimpconsult_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.vsdsimpconsult_linked=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM mdd JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.mdd=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM mdd JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.mdd_linked=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM drugs JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.drugs=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM hospnapr_mkbcode JOIN mkbcode c ON MKB_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.hospnapr=count;
UPDATE mkbchapter_usage U JOIN (SELECT COUNT(*) AS count, c.chapter_id FROM hospnapr_mkbcode JOIN mkbcode c ON MKBLink_id = c.id GROUP BY c.chapter_id) T USING(chapter_id) SET U.hospnapr_linked=count;
UPDATE mkbchapter s
JOIN mkbchapter_usage U ON s.id = U.chapter_id 
SET s.usages = 
    U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;


-- VALIDATION
select c.usages, 
	U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked
from mkbcode c
join mkbcode_usage U on c.id = U.MKB_id
where c.usages !=
	U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;


select *
from mkbset c
join mkbset_usage U on U.set_id = c.id
where c.usages !=
	U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;


select *
from mkbchapter c
join mkbchapter_usage U on U.chapter_id = c.id
where c.usages !=
	U.amblist_primary + U.amblist_primary_linked + U.amblist_secondary + U.amblist_secondary_linked + U.hlist + 
    U.simpconsult + U.simpconsult_linked + U.vsdsimpconsult + U.vsdsimpconsult_linked + 
    U.mdd + U.mdd_linked + U.drugs + U.hospnapr + U.hospnapr_linked;



select * 
FROM(
select 
C.set_id,
sum(amblist_primary) AS amblist_primary,
sum(amblist_primary_linked) AS amblist_primary_linked,
sum(amblist_secondary) AS amblist_secondary,
sum(amblist_secondary_linked) AS amblist_secondary_linked,
sum(hlist) AS hlist,
sum(simpconsult) AS simpconsult,
sum(simpconsult_linked) AS simpconsult_linked,
sum(vsdsimpconsult) AS vsdsimpconsult,
sum(vsdsimpconsult_linked) AS vsdsimpconsult_linked,
sum(mdd) AS mdd,
sum(mdd_linked) AS mdd_linked,
sum(drugs) AS drugs,
sum(hospnapr) AS hospnapr,
sum(hospnapr_linked) AS hospnapr_linked
from mkbcode_usage UC
join mkbcode C on UC.MKB_id = C.id
group by C.set_id
) AS T
JOIN mkbset_usage U USING(set_id)
WHERE 
T.amblist_primary != U.amblist_primary
OR T.amblist_primary_linked != U.amblist_primary_linked
OR T.amblist_secondary != U.amblist_secondary
OR T.amblist_secondary_linked != U.amblist_secondary_linked
OR T.hlist != U.hlist
OR T.simpconsult != U.simpconsult
OR T.simpconsult_linked != U.simpconsult_linked
OR T.vsdsimpconsult != U.vsdsimpconsult
OR T.vsdsimpconsult_linked != U.vsdsimpconsult_linked
OR T.mdd != U.mdd
OR T.mdd_linked != U.mdd_linked
OR T.drugs != U.drugs
OR T.hospnapr != U.hospnapr
OR T.hospnapr_linked != U.hospnapr_linked;

select * 
FROM(
select 
C.chapter_id,
sum(amblist_primary) AS amblist_primary,
sum(amblist_primary_linked) AS amblist_primary_linked,
sum(amblist_secondary) AS amblist_secondary,
sum(amblist_secondary_linked) AS amblist_secondary_linked,
sum(hlist) AS hlist,
sum(simpconsult) AS simpconsult,
sum(simpconsult_linked) AS simpconsult_linked,
sum(vsdsimpconsult) AS vsdsimpconsult,
sum(vsdsimpconsult_linked) AS vsdsimpconsult_linked,
sum(mdd) AS mdd,
sum(mdd_linked) AS mdd_linked,
sum(drugs) AS drugs,
sum(hospnapr) AS hospnapr,
sum(hospnapr_linked) AS hospnapr_linked
from mkbcode_usage UC
join mkbcode C on UC.MKB_id = C.id
group by C.chapter_id
) AS T
JOIN mkbchapter_usage U USING(chapter_id)
WHERE 
T.amblist_primary != U.amblist_primary
OR T.amblist_primary_linked != U.amblist_primary_linked
OR T.amblist_secondary != U.amblist_secondary
OR T.amblist_secondary_linked != U.amblist_secondary_linked
OR T.hlist != U.hlist
OR T.simpconsult != U.simpconsult
OR T.simpconsult_linked != U.simpconsult_linked
OR T.vsdsimpconsult != U.vsdsimpconsult
OR T.vsdsimpconsult_linked != U.vsdsimpconsult_linked
OR T.mdd != U.mdd
OR T.mdd_linked != U.mdd_linked
OR T.drugs != U.drugs
OR T.hospnapr != U.hospnapr
OR T.hospnapr_linked != U.hospnapr_linked;
