<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProcedureRepository;


class ProcedureController extends Controller
{	
  protected $procedures;

  public function __construct(ProcedureRepository $procedures)
  {
      $this->procedures = $procedures;
  }

	public function procedures()
	{
    $procedures = $this->procedures->all();

    return view('procedures', [
      'tab'        => 'procedures',
      'procedures' => $procedures, 
      'procedure'  => null,
      'title'      => 'Процедури',
    ]);
	}
  
 	public function procedure($id)
  {
    $procedures = $this->procedures->all();
    $procedure  = $this->procedures->single($id);

    if(empty($procedure)){
      abort(404);
    }

    if(empty($procedure)){ abort(404); }
    
    return view('procedures', [
      'tab'           => 'procedures',
      'procedures'    => $procedures,
      'procedure'     => $procedure,
      'title'         => 'Процедурa - ' . $procedure['code'],
    ]);
 	}
}
