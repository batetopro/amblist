<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DoctorRepository;


class DoctorController extends Controller
{	
  protected $doctors;

  public function __construct(DoctorRepository $doctors)
  {
      $this->doctors = $doctors;
  }

	public function doctors()
	{
    $doctors = $this->doctors->all();
    return view('home', [
      'doctors' => $doctors, 
      'tab' => 'practice'
    ]);
	}
  
 	public function doctor($id)
  {
    $doctor = $this->doctors->single($id);
    if(empty($doctor)){ abort(404); }
    return view('doctor', [
      'doctor'    => $doctor,
      'tab'       => 'doctor',
      'amblists'  => $this->doctors->get_amblists($doctor['id']),
    ]);
 	}
}
