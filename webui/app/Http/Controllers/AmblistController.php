<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AmblistRepository;


class AmblistController extends Controller
{	
  
  protected $amblists;

  public function __construct(AmblistRepository $amblists)
  {
      $this->amblists = $amblists;
  }

  public function amblist_api($id)
  {
    return $this->amblists->build($id);
  }

 	public function amblist($id)
  {
    $amblist = $this->amblists->build($id);

    return view('amblist', [
      'amblist'   => $amblist,
      'tab'       => 'amblist',
      'title'     => 'Амбулаторен лист - ' . $amblist['number'],
    ]);
 	}

  public function template()
  {
    return view('template', [
      'tab'       => 'amblist',
      'title'     => 'Амбулаторен лист - template',
    ]);
  }
}
