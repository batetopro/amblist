<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ImmunizationRepository;


class ImmunizationController extends Controller
{	
  protected $immunizations;

  public function __construct(ImmunizationRepository $immunizations)
  {
      $this->immunizations = $immunizations;
  }

	public function immunizations()
	{
    $immunizations = $this->immunizations->all();
    return view('immunization', [
      'immunizations'    => $immunizations, 
      'immunization'     => null,
      'title'            => 'Имунизации',
      'tab'              => 'immunizations',
    ]);
	}
  
 	public function immunization($id)
  {
    $immunizations = $this->immunizations->all();
    $immunization  = $this->immunizations->single($id);
    
    if(empty($immunization)){ abort(404); }
    
    return view('immunization', [
      'immunizations'    => $immunizations,
      'immunization'     => $immunization,
      'title'            => 'Имунизация - ' . $immunization['code'],
      'tab'              => 'immunizations',
    ]);
 	}
}
