<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MorbidityRepository;


class MorbidityController extends Controller
{	

  protected $repo;

  public function __construct(MorbidityRepository $repo)
  {
      $this->repo = $repo;
  }

  public function all()
  {
    return view('morbidity', [
      'items'     => $this->repo->all(),
      'morbidity' => null,
      'tab'       => 'morbidity',
      'title'     => 'Морбидност',
    ]);
  }

 	public function single($id)
  {
    $item = $this->repo->single($id);

    if(empty($item)){ abort(); }

    return view('morbidity', [
      'items'     => $this->repo->all(),
      'morbidity' => $item,
      'tab'       => 'morbidity',
      'title'     => 'Морбидност - ' . $item['name'],
    ]);
 	}
}
