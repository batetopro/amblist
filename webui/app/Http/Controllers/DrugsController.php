<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DrugsRepository;


class DrugsController extends Controller
{
  
  protected $drugs;

  public function __construct(DrugsRepository $drugs)
  {
    $this->drugs = $drugs;
  }

  public function drugs()
  {
    return view('drugs', [
    	'title'      => 'Лекартства',
    	'tab'        => 'drugs',
    	'drug_code'  => null,
    	'drugs'      => $this->drugs->all(),
    ]);
  }
  
  public function drug($drug_code)
  {
    return view('drugs', [
        'title'          => 'Лекартства',
        'tab'            => 'drugs',
        'drug_code'      => $drug_code,
    	'drugs'          => $this->drugs->all(),
    ]);
  }

  public function api_drug($drug_code){
  	return $this->drugs->single($drug_code);
  }
}
