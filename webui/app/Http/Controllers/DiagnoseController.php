<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DiagnoseRepository;


class DiagnoseController extends Controller
{	

  protected $diagnoses;

  public function __construct(DiagnoseRepository $diagnoses)
  {
      $this->diagnoses = $diagnoses;
  }

  public function diagnoses()
  {
    return view('diagnose', [
      'diagnose'  => null,
      'tab'       => 'diagnoses',
      'title'     => 'Диагнози',
    ]);
  }

 	public function diagnose($id)
  {
    return view('diagnose', [
      'diagnose'  => $id,
      'tab'       => 'diagnoses',
      'title'     => 'Диагнози',
    ]);
 	}

  public function api_chapters(Request $request)
  { 
    $with_usage_only = boolval($request->input('with_usage_only', false));
    return $this->diagnoses->getChapters($with_usage_only);
  }

  public function api_chapter(Request $request, $chapter_id)
  {
    $result = $this->diagnoses->getChapter($chapter_id);

    if(empty($result)){ return []; }

    $with_usage_only          = boolval($request->input('with_usage_only', false));
    $result['sets']           = $this->diagnoses->getSets($chapter_id, $with_usage_only);
    $result['usage']          = $this->diagnoses->getUsage($chapter_id, 'mkbchapter_usage', 'chapter_id');
    $result['cooccurrence']   = [
      'procedures' => $this->diagnoses->getProcedureCooccurrence($chapter_id, 'mkbchapter_procedure_cooccurrence', 'chapter_id'),

    ];

    return $result;
  }

  public function api_set(Request $request, $set_id)
  {
    $result = $this->diagnoses->getSet($set_id);
    
    if(empty($result)){ return []; }

    $with_usage_only          = boolval($request->input('with_usage_only', false));
    $result['diagnoses']      = $this->diagnoses->getDiagnoses($set_id, $with_usage_only);
    $result['usage']          = $this->diagnoses->getUsage($set_id, 'mkbset_usage', 'set_id');
    $result['cooccurrence']   = [
      'procedures' => $this->diagnoses->getProcedureCooccurrence($set_id, 'mkbset_procedure_cooccurrence', 'set_id'),

    ];

    return $result; 
  }

  public function api_diagnose($diagnose_id)
  {
    $result = $this->diagnoses->single($diagnose_id);

    if(empty($result)){ return []; }

    $result['usage']          = $this->diagnoses->getUsage($diagnose_id, 'mkbcode_usage', 'MKB_id');
    $result['cooccurrence']   = [
      'procedures' => $this->diagnoses->getProcedureCooccurrence($diagnose_id, 'mkbcode_procedure_cooccurrence', 'MKB_id'),
      
    ];
    
    return $result;
  }
}
