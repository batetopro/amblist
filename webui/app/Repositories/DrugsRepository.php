<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class DrugsRepository
{
  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }

  public function all()
   {       
      $rows = DB::table('druginfo')
            ->select([
              'druginfo.id', 
              'druginfo.Code',
              'druginfo.Name',
              'druginfo.MarketName',
              'druginfo.Form',
              'druginfo.Quantity',
              'druginfo.Unit',
            ])
            ->orderBy('Code', 'asc')
            ->get();

      $result = [];

      foreach($rows as $row){
         $result[] = [
            'id'              => $row->id,
            'code'            => $row->Code,
            'name'            => $row->Name,
            'market_name'     => $row->MarketName,
            'form'            => $row->Form,
            'quantity'        => $row->Quantity,
            'unit'            => $row->Unit,
         ];
      }

      return $result;
  }

  public function single($drug_code)
  { 
    $druginfo = DB::table('druginfo')
            ->select([
              'druginfo.id', 
              'druginfo.Code',
              'druginfo.Name',
              'druginfo.MarketName',
              'druginfo.Form',
              'druginfo.Quantity',
              'druginfo.Unit',
            ])
            ->where('Code', '=' , $drug_code)
            ->first();

    $result = [
      'code'            => $drug_code,
      'info'            => null,
      'diagnoses'       => [],
      'amblists'        => [],
      'patients'        => [],
    ];

    if(empty($druginfo)){
      $drugs = DB::table('drugs')
            ->select(['id'])
            ->where('DrugCode', '=' , $drug_code)
            ->get();

      $drugIds = [];
      foreach ($drugs as $drug) {
        $drugIds[] = $drug->id;
      }

      if(empty($drugIds)){
        return $result;
      }
    } else {
      $drugIds = $this->get_drugs($druginfo->id);

      $result['info'] = [
        'id'              => $druginfo->id,
        'code'            => $druginfo->Code,
        'name'            => $druginfo->Name,
        'market_name'     => $druginfo->MarketName,
        'form'            => $druginfo->Form,
        'quantity'        => $druginfo->Quantity,
        'unit'            => $druginfo->Unit,
      ];
    }

    $result['diagnoses'] = $this->get_diagnoses($drugIds);
    $result['amblists']  = $this->get_amblists($drugIds);
    $result['patients']  = $this->get_patients($drugIds);

    return $result;
  }

  public function get_drugs($druginfo_id)
  {
    $data = DB::table('drugs')
        ->where('DrugInfo_id', '=', $druginfo_id)
        ->select(['id'])
        ->get();

    $result = [];

    foreach ($data as $row) { $result[] = $row->id;}

    return $result;
  }

  public function get_diagnoses($drugs)
  {
    $diagnoses = DB::table('drugs')
        ->join('MKBCode', 'MKBCode.id', '=', 'drugs.MKB_id')
        ->whereIn('drugs.id', $drugs)
        ->select([
          'MKBCode.id',
          'MKBCode.chapter_id',
          'MKBCode.set_id',
          'MKBCode.morbidity_id',
          'MKBCode.code',
          'MKBCode.name',
          'MKBCode.name_latin',
        ])
        ->get();

    $result = [];

    foreach ($diagnoses as $diagnose) {
      $result[] = [
        'id'                => $diagnose->id,
        'chapter_id'        => $diagnose->chapter_id,
        'set_id'            => $diagnose->set_id,
        'code'              => $diagnose->code,
        'name'              => $diagnose->name,
        'name_latin'        => $diagnose->name_latin,
      ];
    }

    return $result;
  }

  public function get_patients($drugs)
  {
    $data = DB::table('rp_drug')
        ->join('rp', 'rp_drug.Rp_id', '=', 'rp.id')
        ->join('patient', 'patient.id', '=', 'rp.Patient_id')
        ->whereIn('rp_drug.Drug_id', $drugs)
        ->select([
          'rp_drug.Quantity',
          'rp_drug.Day',
          'patient.id',
          'patient.EGN',
          'patient.RZOK',
          'patient.ZdrRajon',
          'patient.NameGiven',
          'patient.NameSur',
          'patient.NameFamily',
          'patient.Address',
          'patient.IsHealthInsurance',
        ])
        ->get();

    $result = [];
        
    foreach($data as $row){
      $result[] = [
        'quantity'             => $row->Quantity,
        'day'                  => $row->Day,
        'id'                   => $row->id,
        'EGN'                  => $row->EGN,
        'RZOK'                 => $row->RZOK,
        'zdr_rajon'            => $row->ZdrRajon,
        'name'                 => [
          'given'   => $row->NameGiven,
          'sur'     => $row->NameSur,
          'family'  => $row->NameFamily,
        ],
        'address'              => $row->Address,
        'is_health_insurance'  => boolval($row->IsHealthInsurance),
      ];
    }

    return $result;
  }

  public function get_amblists($drugs)
  {
    $amblists = DB::table('amblist_drug')
          ->select([
            'amblist_drug.Quantity',
            'amblist_drug.Day',
            'amblist.id',
            'amblist.NoAl',
            'amblist.dataAl',
            'amblist.time',
            'amblist.ExamType',
            'amblist.Doctor_id',
            'doctor.SIMPCode',
            'MKBCode.id AS MD_id', 
            'MKBCode.chapter_id AS MD_chapter_id',
            'MKBCode.set_id AS MD_set_id',
            'MKBCode.morbidity_id AS MD_morbidity_id',
            'MKBCode.code AS MD_code',
            'MKBCode.name AS MD_name',
            'MKBCode.name_latin AS MD_name_latin',
          ])
          ->join('amblist', 'amblist.id', '=', 'amblist_drug.Amblist_id')
          ->join('MKBCode', 'MKBCode.id', '=', 'amblist.MKB_id')
          ->join('doctor', 'doctor.id', '=', 'amblist.Doctor_id')
          ->whereIn('amblist_drug.Drug_id', $drugs)
          ->orderBy('MKBCode.code', 'asc')
          ->orderBy('amblist.dataAl', 'asc')
          ->orderBy('amblist.time', 'asc')
          ->get();

      $result = [];
      foreach ($amblists as $amblist) {
        $result[] = [
          'day'                 => $amblist->Day,
          'quantity'            => $amblist->Quantity,
          'id'                  => $amblist->id,
          'number'              => $amblist->NoAl,
          'date'                => $amblist->dataAl,
          'time'                => $amblist->time,
          'exam_type'           => $amblist->ExamType,
          'exam_type_name'      => $this->codes->getExamTypeName($amblist->ExamType),
          'doctor'              => [
            'id'                => $amblist->Doctor_id,
            'code_name'         => $this->codes->getSIMPCodeName($amblist->SIMPCode),
            'code'              => $amblist->SIMPCode,
          ],
          'main_diagnose'   => [
            'id'                => $amblist->MD_id,
            'chapter_id'        => $amblist->MD_chapter_id,
            'set_id'            => $amblist->MD_set_id,
            'code'              => $amblist->MD_code,
            'name'              => $amblist->MD_name,
            'name_latin'        => $amblist->MD_name_latin,
          ],
        ];
      }

      return $result;
  }
}
