<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class ProcedureRepository
{
  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }

	public function all()
   {
		  $rows = DB::table('procedure')
            ->select(['id', 'imeP', 'kodP'])
            ->orderBy('kodP', 'asc')
            ->get();

      $result = [];

      foreach($rows as $row){
         $result[] = [
            'id'              => $row->id,
            'name'            => $row->imeP,
            'code'            => $row->kodP,
         ];
      }

      return $result;
	}

	public function single($id)
  {
		$procedure = DB::table('procedure')
              ->where('procedure.id', '=', $id)
              ->select(['id', 'imeP', 'kodP'])
              ->first();

		if(empty($procedure)){ return false; }
      
    $result = [
      'id'              => $procedure->id,
      'name'            => $procedure->imeP,
      'code'            => $procedure->kodP,
      'amblists'        => $this->get_amblists($procedure->id)
    ];
    
    return $result;
	}

  public function get_amblists($procedure_id)
  {
    $amblists = DB::table('amblist_procedure')
          ->select([
            'amblist_procedure.CountP',
            'amblist.id',
            'amblist.NoAl',
            'amblist.dataAl',
            'amblist.time',
            'amblist.ExamType',
            'amblist.Doctor_id',
            'doctor.SIMPCode',
            'MKBCode.id AS MD_id', 
            'MKBCode.chapter_id AS MD_chapter_id',
            'MKBCode.set_id AS MD_set_id',
            'MKBCode.morbidity_id AS MD_morbidity_id',
            'MKBCode.code AS MD_code',
            'MKBCode.name AS MD_name',
            'MKBCode.name_latin AS MD_name_latin',
          ])
          ->join('amblist', 'amblist.id', '=', 'amblist_procedure.Amblist_id')
          ->join('MKBCode', 'MKBCode.id', '=', 'amblist.MKB_id')
          ->join('doctor', 'doctor.id', '=', 'amblist.Doctor_id')
          ->where('amblist_procedure.Procedure_id', '=', $procedure_id)
          ->orderBy('MKBCode.code', 'asc')
          ->orderBy('amblist.dataAl', 'asc')
          ->orderBy('amblist.time', 'asc')
          ->get();

      $result = [];
      foreach ($amblists as $amblist) {
        $result[] = [
          'id'                  => $amblist->id,
          'count'               => $amblist->CountP,
          'number'              => $amblist->NoAl,
          'date'                => $amblist->dataAl,
          'time'                => $amblist->time,
          'exam_type'           => $amblist->ExamType,
          'exam_type_name'      => $this->codes->getExamTypeName($amblist->ExamType),
          'doctor'              => [
            'id'                => $amblist->Doctor_id,
            'code_name'         => $this->codes->getSIMPCodeName($amblist->SIMPCode),
            'code'          => $amblist->SIMPCode,
          ],
          'main_diagnose'   => [
            'id'                => $amblist->MD_id,
            'chapter_id'        => $amblist->MD_chapter_id,
            'set_id'            => $amblist->MD_set_id,
            'code'              => $amblist->MD_code,
            'name'              => $amblist->MD_name,
            'name_latin'        => $amblist->MD_name_latin,
          ],
        ];
      }

      return $result;
  }
}
