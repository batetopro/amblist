<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class DiagnoseRepository
{
  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }

	public function getChapters($with_usage_only=false)
  {
		$query = DB::table('mkbchapter')
            ->select(['id', 'code', 'name', 'usages'])
            ->orderBy('code', 'asc');
    
    if($with_usage_only)
    {
      $query = $query->where('usages', '>', 0);
    }

    $rows = $query->get();

    $result = [];

    foreach($rows as $row){
       $result[] = [
          'id'        => $row->id,
          'usages'    => $row->usages,
          'code'      => $row->code,
          'name'      => $row->name,
       ];
    }

    return $result;
  }

  public function getChapter($chapter_id)
  {
    $chapter = DB::table('mkbchapter')->select(['id', 'code', 'name'])->where('id', '=', $chapter_id)->first();

    if(empty($chapter)){ return false; }

    return [
      'id'           => $chapter->id,
      'code'         => $chapter->code,
      'name'         => $chapter->name,
    ];
  }

  public function getSets($chapter_id, $with_usage_only=false)
  {
    $query = DB::table('mkbset')
            ->select(['id', 'chapter_id', 'code', 'name', 'usages'])
            ->where('chapter_id', '=', $chapter_id);

    if($with_usage_only)
    {
      $query = $query->where('usages', '>', 0);
      $query = $query->orderBy('usages', 'desc');
    }
    else
    {
      $query = $query->orderBy('code', 'asc');
    }

    $rows = $query->get();

    $result = [];

    foreach($rows as $row){
       $result[] = [
          'id'         => $row->id,
          'chapter_id' => $row->chapter_id,
          'code'       => $row->code,
          'name'       => $row->name,
          'usages'     => $row->usages,
       ];
    }

    return $result;
  }

  public function getSet($set_id)
  {
    $set = DB::table('mkbset')->select(['id', 'chapter_id', 'code', 'name'])->where('id', '=', $set_id)->first();

    if(empty($set)){ return false; }

    return [
      'id'           => $set->id,
      'chapter_id'   => $set->chapter_id,
      'code'         => $set->code,
      'name'         => $set->name,
    ];
  }

  public function getDiagnoses($set_id, $with_usage_only=false)
  {
    $query = DB::table('mkbcode')
            ->select(['id', 'chapter_id', 'set_id', 'morbidity_id', 'code', 'name', 'name_latin', 'usages'])
            ->where('set_id', '=', $set_id);
    
    if($with_usage_only)
    {
      $query = $query->where('usages', '>', 0);
      $query = $query->orderBy('usages', 'desc');
    }
    else
    {
      $query = $query->orderBy('code', 'asc');
    }

    $rows = $query->get();


    $result = [];

    foreach($rows as $row){
       $result[] = [
          'id'           => $row->id,
          'chapter_id'   => $row->chapter_id,
          'set_id'       => $row->set_id,
          'morbidity_id' => $row->morbidity_id,
          'code'         => $row->code,
          'name'         => $row->name,
          'name_latin'   => $row->name_latin,
          'usages'       => $row->usages,
       ];
    }

    return $result;
  }

  public function getUsage($id, $db_table, $db_column)
  {
    $row = DB::table($db_table)
            ->select([
              $db_column, 
              'amblist_primary',
              'amblist_primary_linked',
              'amblist_secondary',
              'amblist_secondary_linked',
              'hlist',
              'simpconsult',
              'simpconsult_linked',
              'vsdsimpconsult',
              'vsdsimpconsult_linked',
              'mdd',
              'mdd_linked',
              'drugs',
              'hospnapr',
              'hospnapr_linked'
            ])
            ->where($db_column, '=', $id)
            ->first();

    $result =[
      'amblist_primary'           => $row->amblist_primary,
      'amblist_primary_linked'    => $row->amblist_primary_linked,
      'amblist_secondary'         => $row->amblist_secondary,
      'amblist_secondary_linked'  => $row->amblist_secondary_linked,
      'hlist'                     => $row->hlist,
      'simpconsult'               => $row->simpconsult,
      'simpconsult_linked'        => $row->simpconsult_linked,
      'vsdsimpconsult'            => $row->vsdsimpconsult,
      'vsdsimpconsult_linked'     => $row->vsdsimpconsult_linked,
      'mdd'                       => $row->mdd,
      'mdd_linked'                => $row->mdd_linked,
      'drugs'                     => $row->drugs,
      'hospnapr'                  => $row->hospnapr,
      'hospnapr_linked'           => $row->hospnapr_linked
    ];

    return $result;
  }

  public function getProcedureCooccurrence($id, $db_table, $db_column)
  {

     $rows = DB::table($db_table)
            ->select([
              $db_column,
              'procedure_id',
              'count',
              'procedureCount',
              'diagnoseCount',
              'totalCount',
              'PMI',
              'imeP',
              'kodP'
            ])
            ->join('procedure', 'procedure.id', '=', $db_table . '.procedure_id')
            ->where($db_column, '=', $id)
            ->orderBy('PMI', 'desc')
            ->get();
    
    $result = [];

    foreach ($rows as $row)
    {
      $result[] = [
        'procedure'                 => [
          'id'     => $row->procedure_id,
          'name'   => $row->imeP,
          'code'   => $row->kodP,
        ],
        'count'                     => $row->count,
        'procedureCount'            => $row->procedureCount,
        'diagnoseCount'             => $row->diagnoseCount,
        'totalCount'                => $row->totalCount,
        'PMI'                       => $row->PMI,
      ];
    }

    return $result;
  }

	public function single($id)
  {
		$code = DB::table('mkbcode')
         ->where('mkbcode.id', '=', $id)
         ->leftJoin('mkbmorbidity', 'mkbmorbidity.id', '=', 'mkbcode.morbidity_id')
         ->select([
            'mkbcode.id',
            'mkbcode.chapter_id',
            'mkbcode.set_id',
            'mkbcode.code',
            'mkbcode.name',
            'mkbcode.name_latin',
            'mkbmorbidity.id AS morbidity_id',
            'mkbmorbidity.name AS morbidity_name',
            'mkbmorbidity.code AS morbidity_code',
         ])
         ->first();

		if(empty($code)){ return []; }
      
    $result = [
        'id'                   => $code->id,
        'chapter_id'           => $code->chapter_id,
        'set_id'               => $code->set_id,
        'code'                 => $code->code,
        'name'                 => $code->name,
        'name_latin'           => $code->name_latin,
        'morbidity' => [
          'id'      => $code->morbidity_id,
          'code'    => $code->morbidity_code,
          'name'    => $code->morbidity_name,
        ],
      ];
      
      return $result;
	}

  public function get_amblists($doctor_id, $db_column){
    $amblists = DB::table('amblist')
          ->select([
            'amblist.id',
            'amblist.NoAl',
            'amblist.dataAl',
            'amblist.time',
            'amblist.ExamType',
            'amblist.HasPrimaryVisit',
            'amblist.HasSecondaryVisit',
            'MKBCode.id AS MD_id', 
            'MKBCode.chapter_id AS MD_chapter_id',
            'MKBCode.set_id AS MD_set_id',
            'MKBCode.morbidity_id AS MD_morbidity_id',
            'MKBCode.code AS MD_code',
            'MKBCode.name AS MD_name',
            'MKBCode.name_latin AS MD_name_latin',
          ])
          ->join('MKBCode', 'MKBCode.id', '=', 'amblist.MKB_id')
          ->where('Doctor_id', '=', $doctor_id)
          ->orderBy('amblist.dataAl', 'asc')
          ->orderBy('amblist.time', 'asc')
          ->get();

      $result = [];
      foreach ($amblists as $amblist) {
        $result[] = [
          'id'                  => $amblist->id,
          'number'              => $amblist->NoAl,
          'date'                => $amblist->dataAl,
          'time'                => $amblist->time,
          'exam_type'           => $amblist->ExamType,
          'exam_type_name'      => $this->codes->getExamTypeName($amblist->ExamType),
          'has_primary_visit'   => boolval($amblist->HasPrimaryVisit),
          'has_secondary_visit' => boolval($amblist->HasSecondaryVisit),
          'main_diagnose'       => [
                                  'id'                => $amblist->MD_id,
                                  'chapter_id'        => $amblist->MD_chapter_id,
                                  'set_id'            => $amblist->MD_set_id,
                                  'code'              => $amblist->MD_code,
                                  'name'              => $amblist->MD_name,
                                  'name_latin'        => $amblist->MD_name_latin,
                                ],
        ];
      }

      return $result;
  }
}
