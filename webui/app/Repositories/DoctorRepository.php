<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class DoctorRepository
{
  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }

	public function all()
   {
		$rows = DB::table('doctor')
            ->join('practice', 'practice.id', '=', 'doctor.practice_id')
            ->select([
                'doctor.id',
               'doctor.UIN',
               'doctor.SIMPCode',
               'practice.practiceCode',
               'practice.practiceName',
            ])
            ->get();
      
      $result = [];

      foreach($rows as $row){
         $result[] = [
            'id'              => $row->id,
            'UIN'             => $row->UIN,
            'SIMPCode'        => $row->SIMPCode,
            'SIMP'            => $this->codes->getSimpCodeName($row->SIMPCode),
            'practice' => [
               'code'      => $row->practiceCode,
               'name'      => $row->practiceName,
            ],
         ];
      }

      return $result;
	}

	public function single($id)
  {
		$doctor = DB::table('doctor')
         ->join('practice', 'practice.id', '=', 'doctor.Practice_id')
         ->where('doctor.id', '=', $id)
         ->select([
            'doctor.id', 
            'doctor.Practice_id', 
            'doctor.FullName', 
            'doctor.UIN', 
            'doctor.EGN', 
            'doctor.SIMPCode',
            'practice.practiceCode',
            'practice.practiceName'
         ])
         ->first();

		if(empty($doctor)){ return false; }
      
      $result = [
        'id'         => $doctor->id,
        'name'       => $doctor->FullName,
        'UIN'        => $doctor->UIN,
        'EGN'        => $doctor->EGN,
        'SIMPCode'   => $doctor->SIMPCode,
        'SIMP'       => $this->codes->getSimpCodeName($doctor->SIMPCode),
        'practice'   => [
           'id'   => $doctor->Practice_id,
           'code' => $doctor->practiceCode,
           'name' => $doctor->practiceName,
        ]
      ];
      
      return $result;
	}

  public function get_amblists($doctor_id){
    $amblists = DB::table('amblist')
          ->select([
            'amblist.id',
            'amblist.NoAl',
            'amblist.dataAl',
            'amblist.time',
            'amblist.ExamType',
            'amblist.HasPrimaryVisit',
            'amblist.HasSecondaryVisit',

            'MKBCode.id AS MD_id', 
            'MKBCode.chapter_id AS MD_chapter_id',
            'MKBCode.set_id AS MD_set_id',
            'MKBCode.morbidity_id AS MD_morbidity_id',
            'MKBCode.code AS MD_code',
            'MKBCode.name AS MD_name',
            'MKBCode.name_latin AS MD_name_latin',
          ])
          ->join('MKBCode', 'MKBCode.id', '=', 'amblist.MKB_id')
          ->where('Doctor_id', '=', $doctor_id)
          ->orderBy('amblist.dataAl', 'asc')
          ->orderBy('amblist.time', 'asc')
          ->get();

      $result = [];
      foreach ($amblists as $amblist) {
        $result[] = [
          'id'                  => $amblist->id,
          'number'              => $amblist->NoAl,
          'date'                => $amblist->dataAl,
          'time'                => $amblist->time,
          'exam_type'           => $amblist->ExamType,
          'exam_type_name'      => $this->codes->getExamTypeName($amblist->ExamType),
          'has_primary_visit'   => boolval($amblist->HasPrimaryVisit),
          'has_secondary_visit' => boolval($amblist->HasSecondaryVisit),
          'main_diagnose'   => [
                                'id'                => $amblist->MD_id,
                                'chapter_id'        => $amblist->MD_chapter_id,
                                'set_id'            => $amblist->MD_set_id,
                                'code'              => $amblist->MD_code,
                                'name'              => $amblist->MD_name,
                                'name_latin'        => $amblist->MD_name_latin,
                              ],
        ];
      }

      return $result;
  }
}
