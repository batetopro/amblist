<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class MorbidityRepository
{  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }

  public function all()
  {
    $rows = DB::table('mkbmorbidity')
            ->select(['id', 'code', 'name'])
            ->orderBy('code', 'asc')
            ->get();

        $result = [];

    foreach($rows as $row){
     $result[] = [
        'id'              => $row->id,
        'code'            => $row->code,
        'name'            => $row->name,
     ];
    }

    return $result;
  }

  public function single($id)
  {
    $row = DB::table('mkbmorbidity')
            ->select(['id', 'code', 'name'])
            ->where('id', '=', $id)
            ->first();

        if(empty($row)) { return false; }

    $result = [
      'id'        => $row->id,
        'code'      => $row->code,
        'name'      => $row->name,
        'diagnoses'  => [],
    ];

    $diagnoses = DB::table('mkbcode')
            ->select(['id', 'chapter_id', 'set_id', 'morbidity_id', 'code', 'name', 'name_latin'])
            ->where('morbidity_id', '=', $id)
            ->orderBy('code', 'asc')
            ->get();

        foreach ($diagnoses as $diagnose) {
          $result['diagnoses'][] = [
            'id'                => $diagnose->id,
            'chapter_id'        => $diagnose->chapter_id,
            'set_id'            => $diagnose->set_id,
            'morbidity_id'      => $diagnose->morbidity_id,
            'code'              => $diagnose->code,
            'name'              => $diagnose->name,
          'name_latin'        => $diagnose->name_latin,
          ];
        }

    return $result;
  }
}

