<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\CodesRepository;


class AmblistRepository
{  
  protected $codes;

  public function __construct(CodesRepository $codes)
  {
    $this->codes = $codes;
  }
  
  public function build($id)
  {
    $data = $this->single($id);

    $result = [
      'id'            => $data->amblist_id,
      'number'        => $data->NoAl,
      'date'          => $data->dataAl,
      'time'          => $data->time,
      'pay'           => boolval($data->Pay),
      'disadvantage'  => boolval($data->Disadv),
      'incidentally'  => boolval($data->Incidentally),
      'anamnesa'      => $data->Anamnesa,
      'hstate'        => $data->HState,
      'examine'       => $data->Examine,
      'exam_type'     => $data->ExamType,
      'exam_type_name'=> $this->codes->getExamTypeName($data->ExamType),
      'signature'     => boolval($data->Sign),
      'nonreimburce'  => $data->Therapy_Nonreimburce,
      'talon_telk'    => boolval($data->TalonTELK),
      'izvestie'      => boolval($data->Izvestie),
      'et_epikriza'   => boolval($data->EtEpikriza),
      'medical_note'  => boolval($data->MedicalNote),
      'practice'      => [
        'code' => $data->practiceCode,
              'name' => $data->practiceName,
      ],
      'doctor' => [
        'id'       => $data->doctor_id,
        'name'     => $data->doctor_FullName,
        'UIN'      => $data->doctor_UIN,
        'EGN'      => $data->doctor_EGN,
        'SIMPCode' => $data->doctor_SIMPCode,
        'SIMP'     => $this->codes->getSimpCodeName($data->doctor_SIMPCode),
       ],
       'patient' => [
         'id'                   => $data->patient_id,
         'EGN'                  => $data->patient_EGN,
         'RZOK'                 => $data->patient_RZOK,
         'zdr_rajon'            => $data->patient_ZdrRajon,
         'name_given'           => $data->patient_NameGiven,
         'name_sur'             => $data->patient_NameSur,
         'name_family'          => $data->patient_NameFamily,
         'address'              => $data->patient_Address,
         'is_health_insurance'  => boolval($data->patient_IsHealthInsurance),
       ],
       'primary_visit'   => null,
       'secondary_visit' => null,
       'main_diagnose'        => [
         'id'                => $data->MD_id,
        'chapter_id'        => $data->MD_chapter_id,
        'set_id'            => $data->MD_set_id,
        'code'              => $data->MD_code,
        'name'              => $data->MD_name,
        'name_latin'        => $data->MD_name_latin,
       ],
       'linked_diagnose' => null,
       'visit_for' => [
         'id'                => $data->visitfor_id,
         'consult'           => boolval($data->Consult),
         'disp'              => boolval($data->Disp),
         'hosp'              => boolval($data->Hosp),
         'rp_hosp'           => boolval($data->RpHosp),
         'lkk_visit'         => boolval($data->LKKVisit),
         'lkk_members_count' => $data->CountLKKMembers,
         'telk'              => boolval($data->Telk),
         'profilact'         => null,
       ],
       'vaccine'     => [
         'mantu'    => $data->Mantu,
         'number'   => $data->VaccineNo,
         'type'     => $data->VaccineType,
       ],
       'procedures'           => $this->getProcedures($id),
       'drugs'                => $this->getDrugs($id),
       'diagnoses'            => $this->getDiagnoses($id, 'amblist_id', 'amblist_mkbcode'),
       'immunizations'        => $this->getImmunizations($id),
       'simp_consults'        => [],
       'vsdsimp_consults'     => [],
       'mdds'                 => [],
       'lkk'                  => null,
       'rp'                   => null,
       'hlists'               => [],
       'hosp_napr'            => null, 
    ];

    if(!empty($data->profilact_id)){
      $result['visit_for']['profilact'] = [
        'id'           => $data->profilact_id,
        'child'        => $data->Child,
        'mother'       => $data->Mother,
        'over18'       => $data->Over18,
        'risk'         => $data->Risk,
        'gest_week'    => $data->ProfilactGestWeek,
      ];
    }

    if(!empty($data->HasPrimaryVisit)){
      $result['primary_visit'] = $this->getPrimaryVisit($id);
    }

    if(!empty($data->HasSecondaryVisit)){
      $result['secondary_visit'] = $this->getSecondaryVisit($id);
    }

    if(!empty($data->MDL_id)){
      $result['linked_diagnose'] = [
        'id'                => $data->MDL_id,
        'chapter_id'        => $data->MDL_chapter_id,
        'set_id'            => $data->MDL_set_id,
        'code'              => $data->MDL_code,
        'name'              => $data->MDL_name,
        'name_latin'        => $data->MDL_name_latin,
      ];
    }

    if($data->CountSIMPConsults > 0){
      $result['simp_consults'] = $this->getSIMPConsults($id);
    }

    if($data->CountVSDSIMPConsults > 0){
      $result['vsdsimp_consults'] = $this->getVSDSIMPConsults($id);
    }

    if($data->CountMDD > 0){
      $result['mdds'] = $this->getMDDs($id);
    }

    if($data->CountLKK > 0){
      $result['lkk'] = $this->getLKK($id);
    }

    if($data->CountHlist > 0){
      $result['hlists'] = $this->getHlists($id);
    }

    if($data->CountRp > 0){
      $result['rp'] = $this->getRp($data->patient_id, $id);
    }
    
    if($data->CountHospNapr > 0){
      $result['hosp_napr'] = $this->getHospNapr($id);
    }

    return $result;
  }

  public function single($id)
  {
    $amblist = DB::table('amblist')
      ->where('amblist.id', '=', $id)
      ->join('doctor', 'doctor.id', '=', 'amblist.Doctor_id')
      ->join('practice', 'practice.id', '=', 'doctor.Practice_id')
      ->join('patient', 'patient.id', '=', 'amblist.Patient_id')
      ->join('visitfor', 'visitfor.id', '=', 'amblist.VisitFor_id')
      ->leftJoin('profilact', 'profilact.id', '=', 'visitfor.Profilact_id')
      ->join('MKBCode', 'MKBCode.id', '=', 'amblist.MKB_id')
      ->leftJoin('MKBCode AS MKBCodeLink', 'MKBCodeLink.id', '=', 'amblist.MKBLink_id')
      ->select([
        'amblist.id AS amblist_id',
        'amblist.HasPrimaryVisit',
        'amblist.HasSecondaryVisit',
        'amblist.NoAl',
        'amblist.dataAl',
        'amblist.time',
        'amblist.Pay',
        'amblist.Disadv',
        'amblist.Incidentally',
        'amblist.Anamnesa',
        'amblist.HState',
        'amblist.Examine',
        'amblist.ExamType',
        'amblist.Sign',
        'amblist.Mantu',
        'amblist.VaccineNo',
        'amblist.VaccineType',
        'amblist.Therapy_Nonreimburce',
        'amblist.TalonTELK',
        'amblist.Izvestie',
        'amblist.EtEpikriza',
        'amblist.MedicalNote',
        'amblist.ProfilactGestWeek',
        'amblist.CountLKKMembers',
        'amblist.CountSIMPConsults',
        'amblist.CountVSDSIMPConsults',
        'amblist.CountMDD',
        'amblist.CountLKK',
        'amblist.CountHlist',
        'amblist.CountRp',
        'amblist.CountHospNapr',
        'doctor.id AS doctor_id',
        'doctor.FullName AS doctor_FullName',
        'doctor.UIN AS doctor_UIN',
        'doctor.EGN AS doctor_EGN',
        'doctor.SIMPCode AS doctor_SIMPCode',
        'practice.practiceCode',
        'practice.practiceName',
        'patient.id AS patient_id',
        'patient.EGN AS patient_EGN',
        'patient.RZOK AS patient_RZOK',
        'patient.ZdrRajon AS patient_ZdrRajon',
        'patient.NameGiven AS patient_NameGiven',
        'patient.NameSur AS patient_NameSur',
        'patient.NameFamily AS patient_NameFamily',
        'patient.Address AS patient_Address',
        'patient.IsHealthInsurance AS patient_IsHealthInsurance',
        'visitfor.id AS visitfor_id',
        'visitfor.Consult',
        'visitfor.Disp',
        'visitfor.Hosp',
        'visitfor.RpHosp',
        'visitfor.LKKVisit',
        'visitfor.Telk',
        'profilact.id AS profilact_id',
        'profilact.Child',
        'profilact.Mother',
        'profilact.Over18',
        'profilact.Risk',
        'MKBCode.id AS MD_id', 
        'MKBCode.chapter_id AS MD_chapter_id',
        'MKBCode.set_id AS MD_set_id',
        'MKBCode.morbidity_id AS MD_morbidity_id',
        'MKBCode.code AS MD_code',
        'MKBCode.name AS MD_name',
        'MKBCode.name_latin AS MD_name_latin',
        'MKBCodeLink.id AS MDL_id', 
        'MKBCodeLink.chapter_id AS MDL_chapter_id',
        'MKBCodeLink.set_id AS MDL_set_id',
        'MKBCodeLink.morbidity_id AS MDL_morbidity_id',
        'MKBCodeLink.code AS MDL_code',
        'MKBCodeLink.name AS MDL_name',
        'MKBCodeLink.name_latin AS MDL_name_latin',
      ])
      ->first();

      if(empty($amblist)){ return false; }

      return $amblist;
  }

  public function getSIMPConsults($id)
  {
    $simp_consults = DB::table('simpconsult')
      ->select(['id', 'kodSp', 'SpFor', 'NoN'])
      ->where('Amblist_id', '=', $id)
      ->get();

    $result = [];
    foreach($simp_consults as $row){
      $item = [
        'number'           => $row->NoN,
        'SIMPCode'         => $row->kodSp,
        'SIMP'             => $this->codes->getSimpCodeName($row->kodSp),
        'for'              => $row->SpFor,
        'for_name'         => $this->codes->getSimpForName($row->SpFor),
        'diagnoses'        => $this->getDiagnoses($row->id, 'SIMPConsult_id', 'simpconsult_mkbcode'),
      ];
      $result[] = $item;
    }

    return $result;
  }

  public function getVSDSIMPConsults($id)
  {
    $simp_consults = DB::table('vsdsimpconsult')
      ->select(['id', 'kodSpVSD', 'VSDFor', 'NoNVSD', 'KodVSD', 'HospVSD'])
      ->where('Amblist_id', '=', $id)
      ->get();

    $result = [];
    foreach($simp_consults as $row){
      $item = [
        'number'           => $row->NoNVSD,
        'KodVSD'           => $row->KodVSD,
        'SIMPCode'         => $row->kodSpVSD,
        'SIMP'             => $this->codes->getSimpCodeName($row->kodSpVSD),
        'for'              => $row->VSDFor,
        'for_name'         => $this->codes->getSimpForName($row->VSDFor),
        'hosp'             => boolval($row->HospVSD),
        'diagnoses'        => $this->getDiagnoses($row->id, 'SIMPConsult_id', 'simpconsult_mkbcode'),
      ];
      $result[] = $item;
    }
    
    return $result;
  }

  public function getMDDs($id)
  {
    $mdds = DB::table('mdd')
      ->join('MKBCode', 'MKBCode.id', '=', 'mdd.MKB_id')
      ->leftJoin('MKBCode AS MKBCodeLink', 'MKBCodeLink.id', '=', 'mdd.MKBLink_id')
      ->select([
        'mdd.id',
        'mdd.NoMDD', 
        'mdd.MDDFor', 
        'MKBCode.id AS MD_id',
        'MKBCode.chapter_id AS MD_chapter_id',
        'MKBCode.set_id AS MD_set_id',
        'MKBCode.morbidity_id AS MD_morbidity_id',
        'MKBCode.code AS MD_code',
        'MKBCode.name AS MD_name',
        'MKBCode.name_latin AS MD_name_latin',
        'MKBCodeLink.id AS MDL_id',
        'MKBCodeLink.chapter_id AS MDL_chapter_id',
        'MKBCodeLink.set_id AS MDL_set_id',
        'MKBCodeLink.morbidity_id AS MDL_morbidity_id',
        'MKBCodeLink.code AS MDL_code',
        'MKBCodeLink.name AS MDL_name',
        'MKBCodeLink.name_latin AS MDL_name_latin',
      ])
      ->where('Amblist_id', '=', $id)
      ->get();

    $result = [];
    foreach($mdds as $row){
      $item = [
        'id'               => $row->id,
        'number'           => $row->NoMDD,
        'for'              => $row->MDDFor,
        'for_name'         => $this->codes->getSimpForName($row->MDDFor),
        'diagnose'         => [
          'id'                => $row->MD_id,
          'chapter_id'        => $row->MD_chapter_id,
          'set_id'            => $row->MD_set_id,
          'code'              => $row->MD_code,
          'name'              => $row->MD_name,
          'name_latin'        => $row->MD_name_latin,
        ],
        'linked_diagnose'           => null,
        'codes'            => [],
      ];

      if(!empty($row->MDL_id)){
        $item['linked_diagnose'] = [
          'id'                => $row->MDL_id,
          'chapter_id'        => $row->MDL_chapter_id,
          'set_id'            => $row->MDL_set_id,
          'code'              => $row->MDL_code,
          'name'              => $row->MDL_name,
          'name_latin'        => $row->MDL_name_latin,
        ];
      }
            
      $codes = DB::table('mdd_kodmdd')
        ->join('kodmdd', 'kodmdd.id', '=', 'mdd_kodmdd.kodMDD_id')
        ->select(['kodmdd.id', 'kodmdd.Code'])
        ->where('mdd_kodmdd.MDD_id', '=', $id)
        ->orderBy('Code', 'asc')
        ->get();

      foreach($codes as $code){
        $item['codes'][] = [
          'id'    => $code->id,
          'value'  => $code->Code,
        ];
      }

      $result[] = $item;
    }

    return $result;
  }

  public function getLKK($id)
  {
    $lkk = DB::table('lkk')
      ->select([
        'lkk.id',
        'lkk.NoLKK', 
        'lkk.TypeLKK', 
      ])
      ->where('Amblist_id', '=', $id)
      ->first();

    $result = [
      'id'           => $lkk->id,
      'number'       => $lkk->NoLKK,
      'type'         => $lkk->TypeLKK,
      'type_name'    => $this->codes->getLkkTypeName($lkk->TypeLKK),
      'codes'        => []
    ];

    $codes = DB::table('lkk_codespec')
      ->join('codespec', 'lkk_codespec.CodeSpec_id', '=', 'codespec.id')
      ->where('lkk_codespec.LKK_id', '=', $lkk->id)
      ->select(['codespec.id', 'codespec.Code'])
      ->get();

    foreach ($codes as $code) {
      $result['codes'][] = [
        'id'    => $code->id,
        'value' => $code->Code,
           'name'  => $this->codes->getSimpCodeName($code->Code),
      ];
    }

    return $result;
  }

  public function getHlists($id)
  {
    $hlists = DB::table('hlist')
      ->select([
        'hlist.id',
        'hlist.NoBl', 
        'hlist.days',
        'hlist.HLFromDate',
        'hlist.HLToDate',
        'hlist.HLType',
        'MKBCode.id AS MD_id',
        'MKBCode.chapter_id AS MD_chapter_id',
        'MKBCode.set_id AS MD_set_id',
        'MKBCode.morbidity_id AS MD_morbidity_id',
        'MKBCode.code AS MD_code',
        'MKBCode.name AS MD_name',
        'MKBCode.name_latin AS MD_name_latin',
      ])
      ->where('Amblist_id', '=', $id)
      ->join('MKBCode', 'MKBCode.id', '=', 'hlist.MKB_id')
      ->get();

    $result = [];

    foreach ($hlists as $row) {
      $result[] = [
        'id'                   => $row->id,
        'number'               => $row->NoBl,
        'days'                 => $row->days,
        'from'                 => $row->HLFromDate,
        'to'                   => $row->HLToDate,
        'type'                 => $row->HLType,
        'type_name'            => $row->HLType ? 'продължение' : 'първичен',
        'diagnose'             => [
          'id'                => $row->MD_id,
          'chapter_id'        => $row->MD_chapter_id,
          'set_id'            => $row->MD_set_id,
          'code'              => $row->MD_code,
          'name'              => $row->MD_name,
          'name_latin'        => $row->MD_name_latin,
        ]
      ];
    }

    return $result;
  }

  public function getRp($patient_id, $amblist_id)
  {
    $rp = DB::table('rp')
      ->select(['id', 'RpBook'])
      ->where('Patient_id', '=', $patient_id)
      ->first();

    $result = [
      'id'     => $rp->id,
      'book'   => $rp->RpBook,
      'drugs'  => []
    ];

    $drugs = DB::table('rp_drug')
      ->join('drugs', 'drugs.id', '=', 'rp_drug.Drug_id')
      ->join('MKBCode', 'MKBCode.id', '=', 'drugs.MKB_id')
      ->leftJoin('druginfo', 'druginfo.id', '=', 'drugs.DrugInfo_id')
      ->where('rp_drug.Rp_id', '=', $rp->id)
      ->where('rp_drug.amblist_id', '=', $amblist_id)
      ->select([
        'drugs.id',
        'drugs.DrugCode',
        'rp_drug.prescNum',
        'rp_drug.Quantity',
        'rp_drug.Day',
        'druginfo.id as druginfo_id',
        'druginfo.Name',
        'druginfo.MarketName',
        'druginfo.Form',
        'druginfo.Quantity AS druginfo_quantity',
        'druginfo.Unit AS druginfo_unit',
        'MKBCode.id AS MKB_id', 
        'MKBCode.chapter_id AS MKB_chapter_id',
        'MKBCode.set_id AS MKB_set_id',
        'MKBCode.morbidity_id AS MKB_morbidity_id',
        'MKBCode.code AS MKB_code',
        'MKBCode.name AS MKB_name',
        'MKBCode.name_latin AS MKB_name_latin',
      ])
      ->orderBy('prescNum', 'asc')
      ->get();

    foreach($drugs as $drug){
      $item = [
        'id'            => $drug->id,
          'code'          => $drug->DrugCode,
          'number'        => $drug->prescNum,
          'quantity'      => $drug->Quantity,
          'day'           => $drug->Day,
        'diagnose'      => [
          'id'                => $drug->MKB_id,
          'chapter_id'        => $drug->MKB_chapter_id,
          'set_id'            => $drug->MKB_set_id,
          'code'              => $drug->MKB_code,
          'name'              => $drug->MKB_name,
          'name_latin'        => $drug->MKB_name_latin,
        ]
      ];

      if($drug->druginfo_id)
      {
        $item['druginfo'] = [
          'id'              => $drug->druginfo_id,
            'code'            => $drug->DrugCode,
            'name'            => $drug->Name,
            'market_name'     => $drug->MarketName,
          'form'            => $drug->Form,
          'quantity'        => $drug->druginfo_quantity,
          'unit'            => $drug->druginfo_unit,
        ];
      }

      $result['drugs'][] = $item;
    }

    return $result;
  }

  public function getHospNapr($id)
  {
    $hosp_napr = DB::table('hospnapr')
      ->select(['id', 'PathNum', 'NaprNo', 'DateNapr'])
      ->where('Amblist_id', '=', $id)
      ->first();

    $result = [
      'id'           => $hosp_napr->id,
      'path'         => $hosp_napr->PathNum,
      'number'       => $hosp_napr->NaprNo,
      'date'         => $hosp_napr->DateNapr,
      'diagnoses'    => $this->getDiagnoses($hosp_napr->id, 'HospNapr_id', 'hospnapr_mkbcode'),
    ];
    
    return $result;
  }

  public function getImmunizations($id)
  {
    $data = DB::table('amblist_imun')
      ->join('imun', 'imun.id', '=', 'amblist_imun.Imun_id')
      ->where('amblist_imun.amblist_id', '=', $id)
      ->select('imun.id', 'imun.Code')
      ->get();

    $result = [];
    foreach ($data as $immunization) {
      $result[] = [
        'id'     => $immunization->id,
        'code'   => $immunization->Code,
      ];
    }

    return $result;
  }

  public function getPrimaryVisit($id)
  {
    $primary_visit = DB::table('primaryvisit')
      ->where('primaryvisit.amblist_id', '=', $id)
      ->leftJoin('sendedfrom', 'sendedfrom.id', '=', 'primaryvisit.SendedFrom_id')
      ->select([
        'primaryvisit.id AS primaryvisit_id',
        'primaryvisit.VidNapr AS VidNapr',
        'primaryvisit.NaprFor AS NaprFor',
        'primaryvisit.NoNapr AS NoNapr',
        'primaryvisit.dateNapr AS dateNapr',
        'sendedfrom.id AS sendedfrom_id',
        'sendedfrom.PracticeCode AS PracticeCode',
        'sendedfrom.UIN AS UIN',
        'sendedfrom.SIMPCode AS SIMPCode',
        'sendedfrom.H_S AS H_S',
        'sendedfrom.UINH_S AS UINH_S',
      ])
      ->first();

    return [
      'id'               => $primary_visit->primaryvisit_id,
      'vid_napr'         => $primary_visit->VidNapr,
      'napr_for'         => $primary_visit->NaprFor,
      'napr_for_name'    => $this->codes->getNaprForName($primary_visit->NaprFor),
      'number'           => $primary_visit->NoNapr,
      'date'             => $primary_visit->dateNapr,
      'sended_from'      => [
        'id'             => $primary_visit->sendedfrom_id,
        'practice_code'  => $primary_visit->PracticeCode,
        'UIN'            => $primary_visit->UIN,
        'SIMPCode'       => $primary_visit->SIMPCode,
        'SIMP'           => $this->codes->getSimpCodeName($primary_visit->SIMPCode),
        'H_S'            => $primary_visit->H_S,
        'UINH_S'         => $primary_visit->UINH_S,
      ],
    ];
  }

  public function getSecondaryVisit($id)
  {
    $secondary_visit =  DB::table('secondaryvisit')
      ->where('secondaryvisit.amblist_id', '=', $id)
      ->select([
        'secondaryvisit.id',
        'secondaryvisit.NoAmbL',
        'secondaryvisit.dateAmbL',
      ])
      ->first();

    return [
      'id'          => $secondary_visit->id,
      'number'      => $secondary_visit->NoAmbL,
      'date'        => $secondary_visit->dateAmbL,
    ];
  }

  public function getDiagnoses($value, $db_column, $db_table)
  {
    $data = DB::table($db_table)
      ->join('MKBCode', 'MKBCode.id', '=', $db_table.'.MKB_id')
      ->leftJoin('MKBCode AS MKBCodeLink', 'MKBCodeLink.id', '=', $db_table.'.MKBLink_id')
      ->where($db_table.'.'.$db_column, '=', $value)
      ->select([
         'MKBCode.id AS MD_id', 
         'MKBCode.chapter_id AS MD_chapter_id',
         'MKBCode.set_id AS MD_set_id',
         'MKBCode.morbidity_id AS MD_morbidity_id',
         'MKBCode.code AS MD_code',
         'MKBCode.name AS MD_name',
         'MKBCode.name_latin AS MD_name_latin',
         'MKBCodeLink.id AS MDL_id', 
         'MKBCodeLink.chapter_id AS MDL_chapter_id',
         'MKBCodeLink.set_id AS MDL_set_id',
         'MKBCodeLink.morbidity_id AS MDL_morbidity_id',
         'MKBCodeLink.code AS MDL_code',
         'MKBCodeLink.name AS MDL_name',
         'MKBCodeLink.name_latin AS MDL_name_latin',
      ])
      ->get();

    $result = [];

    foreach ($data as $diagnose)
    {
      $item = [
        'main' => [
           'id'                => $diagnose->MD_id,
          'chapter_id'        => $diagnose->MD_chapter_id,
          'set_id'            => $diagnose->MD_set_id,
          'code'              => $diagnose->MD_code,
          'name'              => $diagnose->MD_name,
          'name_latin'        => $diagnose->MD_name_latin,
        ],
        'linked' => null,
      ];

      if(!empty($diagnose->MDL_id))
      {
        $item['linked'] = [
          'id'                => $diagnose->MDL_id,
          'chapter_id'        => $diagnose->MDL_chapter_id,
          'set_id'            => $diagnose->MDL_set_id,
          'code'              => $diagnose->MDL_code,
          'name'              => $diagnose->MDL_name,
          'name_latin'        => $diagnose->MDL_name_latin,
        ];
      }

      $result[] = $item;
    }

    return $result;
  }

  public function getProcedures($id)
  {
    $data = DB::table('amblist_procedure')
      ->join('procedure', 'procedure.id', '=', 'amblist_procedure.Procedure_id')
      ->where('amblist_procedure.amblist_id', '=', $id)
      ->select(
        'procedure.id',
        'amblist_procedure.countP',
        'procedure.imeP',
        'procedure.kodP'
      )
      ->get();
    
    $result = [];

    foreach ($data as $procedure)
    {
      $result[] = [
        'id'       => $procedure->id,
        'code'     => $procedure->kodP,
        'count'    => $procedure->countP,
        'name'     => $procedure->imeP,
      ];
    }

    return $result;
  }

  public function getDrugs($id)
  {
    $data = DB::table('amblist_drug')
      ->join('drugs', 'drugs.id', '=', 'amblist_drug.Drug_id')
      ->join('MKBCode', 'MKBCode.id', '=', 'drugs.MKB_id')
      ->leftJoin('druginfo', 'druginfo.id', '=', 'drugs.DrugInfo_id')
      ->where('amblist_drug.amblist_id', '=', $id)
      ->select([
        'drugs.id',
        'drugs.DrugCode',
        'amblist_drug.Quantity',
        'amblist_drug.Day',
        'druginfo.id as druginfo_id',
        'druginfo.Name',
        'druginfo.MarketName',
        'druginfo.Form',
        'druginfo.Quantity AS druginfo_quantity',
        'druginfo.Unit AS druginfo_unit',
        'MKBCode.id AS MKB_id', 
        'MKBCode.chapter_id AS MKB_chapter_id',
        'MKBCode.set_id AS MKB_set_id',
        'MKBCode.morbidity_id AS MKB_morbidity_id',
        'MKBCode.code AS MKB_code',
        'MKBCode.name AS MKB_name',
        'MKBCode.name_latin AS MKB_name_latin',
      ])
      ->get();

    $result = [];

    foreach ($data as $drug)
    {
      $item = [
        'id'       => $drug->id,
        'code'       => $drug->DrugCode,
        'quantity'     => $drug->Quantity,
        'day'       => $drug->Day,
        'druginfo'      => null,
        'diagnose'      => [
          'id'         => $drug->MKB_id,
          'chapter_id'    => $drug->MKB_chapter_id,
          'set_id'        => $drug->MKB_set_id,
          'morbidity_id'    => $drug->MKB_morbidity_id,
          'code'              => $drug->MKB_code,
          'name'              => $drug->MKB_name,
          'name_latin'        => $drug->MKB_name_latin,
        ],
      ];

      if($drug->druginfo_id)
      {
        $item['druginfo'] = [
          'id'              => $drug->druginfo_id,
          'code'            => $drug->DrugCode,
          'name'            => $drug->Name,
          'market_name'     => $drug->MarketName,
          'form'            => $drug->Form,
          'quantity'        => $drug->druginfo_quantity,
          'unit'            => $drug->druginfo_unit,
        ];
      }

      $result[] = $item;
    }

    return $result;
  }
}

