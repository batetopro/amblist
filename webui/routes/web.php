<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                     'DoctorController@doctors');
Route::get('/doctor/{id}',          'DoctorController@doctor');

Route::get('/amblist/{id}',         'AmblistController@amblist');
Route::get('/api/amblist/{id}',     'AmblistController@amblist_api');

Route::get('/procedures/',          'ProcedureController@procedures');
Route::get('/procedure/{id}',       'ProcedureController@procedure');

Route::get('/immunizations/',       'ImmunizationController@immunizations');
Route::get('/immunization/{id}',    'ImmunizationController@immunization');

Route::get('/drugs/',               'DrugsController@drugs');
Route::get('/drug/{code}',          'DrugsController@drug');
Route::get('/api/drug/{code}',      'DrugsController@api_drug');

Route::get('/morbidity/',                       		'MorbidityController@all');
Route::get('/morbidity/{id}',                   	    'MorbidityController@single');

Route::get('/diagnoses/',                       		'DiagnoseController@diagnoses');
Route::get('/diagnose/{id}',                   			'DiagnoseController@diagnose');
Route::get('/api/diagnose/chapters',                    'DiagnoseController@api_chapters');
Route::get('/api/diagnose/chapter/{id}',                'DiagnoseController@api_chapter');
Route::get('/api/diagnose/set/{id}',                    'DiagnoseController@api_set');
Route::get('/api/diagnose/{id}',          				'DiagnoseController@api_diagnose');


Route::get('/template',          				'AmblistController@template');


// Route::get('/', function () { return view('home'); });
