@extends('master')

@section('title', $title)

@section('content')

<h3>Морбидност</h3>

<div class="row"> 
  <div class="col-md-3">
      <input type="text" class="form-control" placeholder="Код на лекарство" aria-label="Код на лекарство" id="drug-code">
      <ul class="list-group">
        @foreach($items as $item)
          @if ($morbidity && $item['id'] == $morbidity['id'])
            <li class="list-group-item active">
          @else
            <li class="list-group-item">
          @endif
            <a href="/morbidity/{{$item['id']}}">
              <strong>{{$item['code']}} - {{$item['name']}}</strong>
            </a>
          </li>
        @endforeach
      </ul>
  </div>
  <div class="col-md-9">
    @if ($morbidity)
      <h4>{{$morbidity['code']}} - {{$morbidity['name']}}</h4>
      
      <table>
        <tr>
          <th>
            Диагноза
          </th>
        </tr>
        @foreach($morbidity['diagnoses'] as $diagnose)
          <tr>
            <td>
              <a href="/diagnose/{{$diagnose['id']}}">
                <strong>{{$diagnose['code']}}</strong>
                {{$diagnose['name']}}
              </a>
              <br>
              <small>{{$diagnose['name_latin']}}</small>
            </td>
          </tr>
        @endforeach
      </table>
    @else
      <div class="well">Моля, изберете морбидност от списъка.</div>
    @endif
  </div>
</div>
@endsection
