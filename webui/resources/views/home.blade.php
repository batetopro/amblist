@extends('master')

@section('title', 'Home')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <h3>Практики</h3>

          <table class="table table-condensed">
            <thead>
              <th>SIMP</th>
              <th>UIN</th>
              <th>Практика</th>
            </thead>
            <tbody>
              @foreach ($doctors as $doctor)
                <tr>
                  <td>{{ $doctor['SIMP'] }} ({{ $doctor['SIMPCode'] }})</td>
                  <td>
                    <a href="/doctor/{{ $doctor['id'] }}">
                      {{ $doctor['UIN'] }}
                    </a>
                  </td>
                  <td>
                    {{ $doctor['practice']['code'] }}
                    <br>
                    {{ $doctor['practice']['name'] }}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>

@endsection