@extends('master')

@section('title', $title)

@section('content')

<style type="text/css">
    th, td{
        font-size: 0.8em;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <div class="row">
                    <h3 class="pull-left">Амбулаторен лист</h3>
                    <div class="pull-right">
                        <h3>Номер # {{ $amblist['number'] }}</h3>
                        {{ $amblist['date'] }}
                        <small>{{ $amblist['time'] }}</small>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <strong>Доктор:</strong>
                    <address>
                        Име: {{ $amblist['doctor']['name'] }}
                        <br>
                        ЕГН: {{ $amblist['doctor']['EGN'] }}
                        <br>
                        UIN: <a href="/doctor/{{ $amblist['doctor']['id'] }}">{{ $amblist['doctor']['UIN'] }}</a>
                        <br>
                        SIMP код: {{$amblist['doctor']['SIMP']}} ({{ $amblist['doctor']['SIMPCode'] }})
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                    <strong>Пациент:</strong>
                    <br>
                    Име:
                    {{ $amblist['patient']['name_given'] }}
                    {{ $amblist['patient']['name_sur'] }}
                    {{ $amblist['patient']['name_family'] }}
                    <br>
                    ЕГН: 
                    {{ $amblist['patient']['EGN'] }}
                    <br>
                    {{ $amblist['patient']['address'] }}
                    <br>
                    РЗОК:
                    {{ $amblist['patient']['RZOK'] }}
                    <br>
                    Здравен район:
                    {{ $amblist['patient']['zdr_rajon'] }}
                    <br>
                    Застрахован:
                    @if($amblist['patient']['is_health_insurance'])
                        Да
                    @else
                        Не
                    @endif
                    </address>
                </div>
            </div>
        </div>
    </div>

    <hr/>

    @if($amblist['primary_visit'])
        <h4>Данни за първичния преглед</h4>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            <strong>Направление</strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <th class="text-center">Вид</th>
                                    <td>
                                        {{ $amblist['primary_visit']['vid_napr'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center">Номер</th>
                                    <td>{{ $amblist['primary_visit']['number'] }}</td>
                                </tr>
                                <tr>
                                    <th class="text-center">За</th>
                                    <td>
                                        {{ $amblist['primary_visit']['napr_for_name'] }}
                                        ({{ $amblist['primary_visit']['napr_for'] }})
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center">Дата</th>
                                    <td>{{ $amblist['primary_visit']['date'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            <strong>Изпращащ лекар</strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <th class="text-center">Код на практика</th>
                                    <td>{{ $amblist['primary_visit']['sended_from']['practice_code'] }}</td>
                                </tr>
                                <tr>
                                    <th class="text-center">UIN</th>
                                    <td>{{ $amblist['primary_visit']['sended_from']['UIN'] }}</td>
                                </tr>
                                <tr>
                                    <th class="text-center">Специалист</th>
                                    <td>
                                        {{ $amblist['primary_visit']['sended_from']['SIMP'] }}
                                        ({{ $amblist['primary_visit']['sended_from']['SIMPCode'] }})
                                    </td>
                                </tr>
                                @if($amblist['primary_visit']['sended_from']['H_S'])
                                <tr>
                                    <th class="text-center">Заместник</th>
                                    <td>{{ $amblist['primary_visit']['sended_from']['H_S'] }}</td>
                                </tr>
                                <tr>
                                    <th class="text-center">UIN на заместник</th>
                                    <td>{{ $amblist['primary_visit']['sended_from']['UINH_S'] }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <hr/>
    @endif

    @if($amblist['secondary_visit'])
        <h4>Данни за вторичния преглед</h4>

        <div class="row">
            <div class="col-md-12"> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            <strong>Амбулаторен лист</strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <th class="text-center">Номер</th>
                                    <td>{{$amblist['secondary_visit']['number']}}</td>
                                    <th class="text-center">Дата</th>
                                    <td>{{$amblist['secondary_visit']['date']}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <hr/>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <strong>Основна информация</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Посещение за</th>
                                <td colspan="3">{{ $amblist['exam_type_name'] }} ({{ $amblist['exam_type'] }})</td>

                                <th class="text-center">Профилактика</th>
                                <td>
                                    @if($amblist['visit_for']['profilact'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Консултация</th>
                                <td>
                                    @if($amblist['visit_for']['consult'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Платено</th>
                                <td>
                                    @if($amblist['pay'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Неблагоприятни условия</th>
                                <td>
                                    @if($amblist['disadvantage'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Инцидентно посещение</th>
                                <td>
                                    @if($amblist['incidentally'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Подпис на пациента</th>
                                <td>
                                    @if($amblist['signature'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Диспансерен преглед</th>
                                <td>
                                    @if($amblist['visit_for']['disp'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Хоспитализация</th>
                                <td>
                                    @if($amblist['visit_for']['hosp'] || $amblist['visit_for']['rp_hosp'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Експертиза на работоспособността</th>
                                <td>
                                    @if($amblist['visit_for']['lkk_visit'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">ТЕЛК</th>
                                <td>
                                    @if($amblist['visit_for']['telk'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Талон за ЛКК</th>
                                <td>
                                    @if($amblist['talon_telk'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Бързо известие</th>
                                <td>
                                    @if($amblist['izvestie'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Етапна епикриза</th>
                                <td>
                                    @if($amblist['et_epikriza'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                                <th class="text-center">Издадена медицинска бележка</th>
                                <td>
                                    @if($amblist['medical_note'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                            </tr>

                            @if($amblist['visit_for']['profilact'])
                                <tr>
                                    <th class="text-center">Детско здравеопазване</th>
                                    <td>
                                        @if($amblist['visit_for']['profilact']['child'])
                                            Да
                                        @else
                                            Не
                                        @endif
                                    </td>
                                    <th class="text-center">Майчино здравеопазване</th>
                                    <td>
                                        @if($amblist['visit_for']['profilact']['mother'])
                                            Да, {{$amblist['visit_for']['profilact']['gest_week']}} гестационна седмица 
                                        @else
                                            Не
                                        @endif
                                    </td>
                                    <th class="text-center">Профилактика на ЗЗОЛ над 18г</th>
                                    <td>
                                        @if($amblist['visit_for']['profilact']['over18'])
                                            Да
                                        @else
                                            Не
                                        @endif
                                    </td>
                                    <th class="text-center">Рискова група</th>
                                    <td>
                                        @if($amblist['visit_for']['profilact']['risk'])
                                            Да
                                        @else
                                            Не
                                        @endif
                                    </td>
                                </tr>
                            @endif

                            <tr>
                                <th class="text-center">Основна диагноза</th>
                                <td colspan="7">
                                    <a href="/diagnose/{{$amblist['main_diagnose']['id']}}">
                                        <strong>{{$amblist['main_diagnose']['code']}}</strong>
                                        {{$amblist['main_diagnose']['name']}}
                                    </a>
                                    <br>
                                    <small>{{$amblist['main_diagnose']['name_latin']}}</small>
                                </td>
                            </tr>
                            @if ($amblist['linked_diagnose'])
                                <tr>
                                    <th class="text-center">Свързана диагноза</th>
                                    <td colspan="7">
                                        <a>
                                            <strong>{{$amblist['linked_diagnose']['code']}}</strong>
                                            {{$amblist['linked_diagnose']['name']}}
                                        </a>
                                        <br>
                                        <small>{{$amblist['linked_diagnose']['name_latin']}}</small>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <th class="text-center">Анамнеза</th>
                                <td colspan="7">
                                    @if($amblist['anamnesa'])
                                        <pre><code>{{ $amblist['anamnesa'] }}</code></pre>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Обективно състояние</th>
                                <td colspan="7">
                                    @if($amblist['hstate'])
                                        <pre><code>{{ $amblist['hstate'] }}</code></pre>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Изследвания</th>
                                <td colspan="7">
                                    @if($amblist['examine'])
                                        <pre><code>{{ $amblist['examine'] }}</code></pre>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Нереимбурсна терапия</th>
                                <td colspan="7">
                                    @if($amblist['examine'])
                                        <pre><code>{{ $amblist['nonreimburce'] }}</code></pre>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <hr>

    @if ($amblist['vaccine']['number'])
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            <strong>Ваксина</strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <th class="text-center">Манту</th>
                                    <td>{{ $amblist['vaccine']['mantu'] }}</td>
                                    <th class="text-center">Вид</th>
                                    <td>{{ $amblist['vaccine']['type'] }}</td>
                                    <th class="text-center">Номер</th>
                                    <td>{{ $amblist['vaccine']['number'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        <hr/>
    @endif

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <strong>Придружаващи заболявания</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($amblist['diagnoses']) > 0)
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center">Основно заболяване</th>
                                    <th class="text-center">Свързано заболяване</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($amblist['diagnoses'] as $diag)
                                    <tr>
                                        <td>
                                            <a href="/diagnose/{{$diag['main']['id']}}">
                                                <strong>{{ $diag['main']['code'] }}</strong>
                                                {{ $diag['main']['name'] }}
                                            </a>
                                            <br>
                                            <small>{{ $diag['main']['name_latin'] }}</small>
                                        </td>
                                        <td>
                                            @if ($diag['linked'])
                                                <a href="/diagnose/{{$diag['linked']['id']}}">
                                                    <strong>{{ $diag['linked']['code'] }}</strong>
                                                    {{ $diag['linked']['name'] }}
                                                </a>
                                                <br>
                                                <small>{{ $diag['linked']['name_latin'] }}</small>
                                            @else
                                                &nbsp;
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">
                            <strong>Няма наблюдавани придружаващи заболявания</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <strong>Реимбурсна терапия</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($amblist['drugs']) > 0)
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center">Код</th>
                                    <th class="text-center">Диагноза</th>
                                    <th class="text-center">Количество</th>
                                    <th class="text-center">Дни</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($amblist['drugs'] as $drug)
                                    <tr>
                                        <td class="text-center">
                                            <a href="/drug/{{$drug['code']}}">
                                                <strong>{{ $drug['code'] }}</strong>
                                                @if($drug['druginfo'])
                                                    {{$drug['druginfo']['name']}}
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/diagnose/{{$drug['diagnose']['id']}}">
                                                <strong>{{ $drug['diagnose']['code'] }}</strong>
                                                {{ $drug['diagnose']['name'] }}
                                            </a>
                                            <br>
                                            <small>{{ $drug['diagnose']['name_latin'] }}</small>
                                        </td>
                                        <td class="text-center">{{ $drug['quantity'] }}</td>
                                        <td class="text-center">{{ $drug['day'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">
                            <strong>Няма назначена реимбурсна терапия</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <strong>Проведени процедури</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($amblist['procedures']) > 0)
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center">Наименование</th>
                                    <th class="text-center">Код</th>
                                    <th class="text-center">Брой</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($amblist['procedures'] as $procedure)
                                    <tr>
                                        <td><a href="/procedure/{{$procedure['id']}}">{{ $procedure['name'] }}</a></td>
                                        <td class="text-center">{{ $procedure['code'] }}</td>
                                        <td class="text-center">{{ $procedure['count'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">
                            <strong>Няма проведени процедури</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <strong>Проведена имунизация</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($amblist['immunizations']) > 0)
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center">Код</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($amblist['immunizations'] as $imun)
                                    <tr>
                                        <td class="text-center"><a>{{$imun['code']}}</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">
                            <strong>Няма проведени имунизации</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <hr>

    @if(count($amblist['simp_consults']) > 0)
        <h4>Направление за СИМП</h4>
        <div class="row">
            @foreach($amblist['simp_consults'] as $item)
                <div class="col-md-3">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Номер</th>
                                <td>{{$item['number']}}</td>
                            </tr>
                            <tr>
                                <th class="text-center">Специалност</th>
                                <td>{{$item['SIMP']}} ({{$item['SIMPCode']}})</td>
                            </tr>
                            <tr>
                                <th class="text-center">За</th>
                                <td>{{$item['for_name']}} ({{$item['for']}})</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">Основно заболяване</th>
                                <th class="text-center">Свързано заболяване</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item['diagnoses'] as $diag)
                                <tr>
                                    <td>
                                        <a href="/diagnose/{{$diag['main']['id']}}">
                                            <strong>{{ $diag['main']['code'] }}</strong>
                                            {{ $diag['main']['name'] }}
                                        </a>
                                        <br>
                                        <small>{{ $diag['main']['name_latin'] }}</small>
                                    </td>
                                    <td>
                                        @if ($diag['linked'])
                                            <a href="/diagnose/{{$diag['linked']['id']}}">
                                                <strong>{{ $diag['linked']['code'] }}</strong>
                                                {{ $diag['linked']['name'] }}
                                            </a>
                                            <br>
                                            <small>{{ $diag['linked']['name_latin'] }}</small>
                                        @else
                                            &nbsp;
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            @endforeach
        </div>

        <hr>

    @endif

    @if(count($amblist['vsdsimp_consults']) > 0)
        <h4>Направление за ВСД</h4>
        <div class="row">
            @foreach($amblist['vsdsimp_consults'] as $item)
                <div class="col-md-6">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Номер</th>
                                <td>{{$item['number']}}</td>
                                <th class="text-center">Код</th>
                                <td>{{$item['KodVSD']}}</td>
                            </tr>
                            <tr>
                                <th class="text-center">Специалност</th>
                                <td>{{$item['SIMP']}} ({{$item['SIMPCode']}})</td>
                                <th class="text-center">Хоспитализация</th>
                                <td>
                                    @if($item['hosp'])
                                        Да
                                    @else
                                        Не
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">За</th>
                                <td>{{$item['for_name']}} ({{$item['for']}})</td>
                                <th class="text-center">&nbsp;</th>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">Основно заболяване</th>
                                <th class="text-center">Свързано заболяване</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item['diagnoses'] as $diag)
                                <tr>
                                    <td>
                                        <a href="/diagnose/{{$diag['main']['id']}}">
                                            <strong>{{ $diag['main']['code'] }}</strong>
                                            {{ $diag['main']['name'] }}
                                        </a>
                                        <br>
                                        <small>{{ $diag['main']['name_latin'] }}</small>
                                    </td>
                                    <td>
                                        @if ($diag['linked'])
                                            <a href="/diagnose/{{$diag['linked']['id']}}">
                                                <strong>{{ $diag['linked']['code'] }}</strong>
                                                {{ $diag['linked']['name'] }}
                                            </a>
                                            <br>
                                            <small>{{ $diag['linked']['name_latin'] }}</small>
                                        @else
                                            &nbsp;
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            @endforeach
        </div>

        <hr>

    @endif

    @if(count($amblist['mdds']) > 0)
        <h4>МДД</h4>
        <div class="row">
            @foreach($amblist['mdds'] as $item)
                <div class="col-md-3">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Номер</th>
                                <td>{{$item['number']}}</td>
                            </tr>
                            <tr>
                                <th class="text-center">За</th>
                                <td>{{$item['for_name']}} ({{$item['for']}})</td>
                            </tr>
                            <tr>
                                <th class="text-center">Основна диагноза</th>
                                <td>
                                    <a href="/diagnose/{{$item['diagnose']['id']}}">
                                        <strong>{{ $item['diagnose']['code'] }}</strong>
                                        {{ $item['diagnose']['name'] }}
                                    </a>
                                    <br>
                                    <small>{{ $item['diagnose']['name_latin'] }}</small>
                                </td>
                            </tr>
                            @if($item['linked_diagnose'])
                                <tr>
                                    <th class="text-center">Свързана диагноза</th>
                                    <td>
                                        <a href="/diagnose/{{$amblist['linked_diagnose']['id']}}">
                                            <strong>{{ $item['linked_diagnose']['code'] }}</strong>
                                            {{ $item['linked_diagnose']['name'] }}
                                        </a>
                                        <br>
                                        <small>{{ $item['linked_diagnose']['name_latin'] }}</small>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Код</th>
                            </tr>
                            @foreach($item['codes'] as $code)
                                <tr>
                                    <td class="text-center"><a>{{$code['value']}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    

                </div>
            @endforeach
        </div>

        <hr>

    @endif

    @if($amblist['lkk'])
        <h4>Лекарска консултативна комисия</h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Номер</th>
                            <td>{{$amblist['lkk']['number']}}</td>
                            <th class="text-center">Тип</th>
                            <td>{{$amblist['lkk']['type_name']}} ({{$amblist['lkk']['type']}})</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Код</th>
                        </tr>
                        @foreach($amblist['lkk']['codes'] as $code)
                            <tr>
                                <td class="text-center"><a>{{$code['name']}} ({{$code['value']}})</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>

    @endif

    @if($amblist['lkk'])
        <h4>Лекарска консултативна комисия</h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Номер</th>
                            <td>{{$amblist['lkk']['number']}}</td>
                            <th class="text-center">Тип</th>
                            <td>{{$amblist['lkk']['type_name']}} ({{$amblist['lkk']['type']}})</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Код</th>
                        </tr>
                        @foreach($amblist['lkk']['codes'] as $code)
                            <tr>
                                <td class="text-center"><a>{{$code['name']}} ({{$code['value']}})</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>
    @endif

    @if($amblist['hlists'])
        <h4>Болнични листи</h4>
        <div class="row">
            @foreach($amblist['hlists'] as $hlist)
                <div class="col-md-6">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <th class="text-center">Номер</th>
                                <td>{{$hlist['number']}}</td>
                                <th class="text-center">Тип</th>
                                <td>{{$hlist['type_name']}} ({{$hlist['type']}})</td>
                                <th class="text-center">Дни</th>
                                <td>{{$hlist['days']}}</td>
                            </tr>
                            <tr>
                                <th class="text-center">От</th>
                                <td>{{$hlist['from']}}</td>
                                <th class="text-center">До</th>
                                <td>{{$hlist['to']}}</td>
                                <th class="text-center">&nbsp;</th>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th class="text-center">Диагноза</th>
                                <td colspan="5">
                                    <a href="/diagnose/{{$hlist['diagnose']['id']}}">
                                        <strong>{{ $hlist['diagnose']['code'] }}</strong>
                                        {{ $hlist['diagnose']['name'] }}
                                    </a>
                                    <br>
                                    <small>{{ $hlist['diagnose']['name_latin'] }}</small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>

        <hr>
    @endif

    @if($amblist['rp'])
        <h4>Рецептурна книжка - {{$amblist['rp']['book']}}</h4>
         <div class="row">
            <div class="col-md-12">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Номер рецепта</th>
                            <th class="text-center">Код</th>
                            <th class="text-center">Количество</th>
                            <th class="text-center">Дни</th>
                            <th class="text-center">Диагноза</th>
                        </tr>
                        @foreach($amblist['rp']['drugs'] as $drug)
                            <tr>
                                <td class="text-center">{{$drug['number']}}</td>
                                <td class="text-center">
                                    <a href="/drug/{{$drug['code']}}">
                                        <strong>{{ $drug['code'] }}</strong>
                                        @if($drug['druginfo'])
                                            {{$drug['druginfo']['name']}}
                                        @endif
                                    </a>
                                </td>
                                <td class="text-center">{{$drug['quantity']}}</td>
                                <td class="text-center">{{$drug['day']}}</td>
                                <td>
                                    <a href="/diagnose/{{$drug['diagnose']['id']}}">
                                        <strong>{{ $drug['diagnose']['code'] }}</strong>
                                        {{ $drug['diagnose']['name'] }}
                                    </a>
                                    <br>
                                    <small>{{ $drug['diagnose']['name_latin'] }}</small>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>
    @endif


    @if($amblist['hosp_napr'])
        <h4>Направление за хоспитализация</h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <th class="text-center">Път</th>
                            <th class="text-center">Номер</th>
                            <th class="text-center">Дата</th>
                        </tr>
                        <tr>
                            <td class="text-center">{{ $amblist['hosp_napr']['path'] }}</td>
                            <td class="text-center">{{ $amblist['hosp_napr']['number'] }}</td>
                            <td class="text-center">{{ $amblist['hosp_napr']['date'] }}</td>
                        </tr>
                    </tbody>
                </table>
                
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th class="text-center">Основно заболяване</th>
                            <th class="text-center">Свързано заболяване</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($amblist['hosp_napr']['diagnoses'] as $diag)
                            <tr>
                                <td>
                                    <a href="/diagnose/{{$diag['main']['id']}}">
                                        <strong>{{ $diag['main']['code'] }}</strong>
                                        {{ $diag['main']['name'] }}
                                    </a>
                                    <br>
                                    <small>{{ $diag['main']['name_latin'] }}</small>
                                </td>
                                <td>
                                    @if ($diag['linked'])
                                        <a href="/diagnose/{{$diag['linked']['id']}}">
                                            <strong>{{ $diag['linked']['code'] }}</strong>
                                            {{ $diag['linked']['name'] }}
                                        </a>
                                        <br>
                                        <small>{{ $diag['linked']['name_latin'] }}</small>
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>
    @endif
</div>

@endsection