@extends('master')

@section('title', $title)

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <h3>Процедури</h3>

          <div class="row">
            <div class="col-md-4">
              <ul class="list-group">
                @foreach($procedures as $item)
                  @if($procedure && $procedure['id'] == $item['id'])
                    <li class="list-group-item active">
                  @else
                    <li class="list-group-item">
                  @endif
                    <a href="/procedure/{{$item['id']}}">
                      <strong>{{$item['code']}}</strong>
                      {{$item['name']}}
                    </a>
                  </li>
                @endforeach
              </ul>              
            </div>
            <div class="col-md-8">
              @if(empty($procedure))

                <div class="well">Моля, изберете процедура от списъка.</div>

              @else
                <h4>
                  {{$procedure['code']}}
                  {{$procedure['name']}}
                </h4>
                <table class="table table-condensed">
                  <thead>
                    <th>Номер</th>
                    <th>Основна диагноза</th>
                    <th>Брой</th>
                    <th>Доктор</th>
                    <th>Вид преглед</th>
                    <th>Дата и час</th>
                  </thead>
                  <tbody>
                    @foreach ($procedure['amblists'] as $amblist)
                      <tr>
                        <td>
                          <a href="/amblist/{{ $amblist['id'] }}">
                            {{ $amblist['number'] }}
                          </a>
                        </td>
                        <td>
                          <a href="/diagnose/{{$amblist['main_diagnose']['id']}}">
                            <strong>{{ $amblist['main_diagnose']['code'] }}</strong>
                            {{ $amblist['main_diagnose']['name'] }}
                          </a>
                          <br>
                          <small>{{ $amblist['main_diagnose']['name_latin'] }}</small>
                        </td>
                        <td>{{ $amblist['count'] }}</td>
                        <td>
                          <a href="/doctor/{{$amblist['doctor']['id']}}">
                            {{$amblist['doctor']['code_name']}} ({{$amblist['doctor']['code']}})
                          </a>
                        </td>
                        <td>{{ $amblist['exam_type_name'] }}</td>
                        <td>{{ $amblist['date'] }} <small>{{ $amblist['time'] }}</small></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @endif
            </div>
          </div>
        </div>
    </div>
</div>

@endsection