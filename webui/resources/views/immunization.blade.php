@extends('master')

@section('title', $title)

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <h3>Имунизации</h3>

          <div class="row">
            <div class="col-md-2">
              <ul class="list-group">
                @foreach($immunizations as $item)
                  @if($immunization && $immunization['id'] == $item['id'])
                    <li class="list-group-item active">
                  @else
                    <li class="list-group-item">
                  @endif
                    <a href="/immunization/{{$item['id']}}">
                      <strong>{{$item['code']}}</strong>
                    </a>
                  </li>
                @endforeach
              </ul>
            </div>
            <div class="col-md-10">
              @if($immunization)
                <h4>{{$immunization['code']}}</h4>
                <table class="table table-condensed">
                  <thead>
                    <th>Номер</th>
                    <th>Основна диагноза</th>
                    <th>Доктор</th>
                    <th>Вид преглед</th>
                    <th>Дата и час</th>
                  </thead>
                  <tbody>
                    @foreach ($immunization['amblists'] as $amblist)
                      <tr>
                        <td>
                          <a href="/amblist/{{ $amblist['id'] }}">
                            {{ $amblist['number'] }}
                          </a>
                        </td>
                        <td>
                          <a href="/diagnose/{{$amblist['main_diagnose']['id']}}">
                            <strong>{{ $amblist['main_diagnose']['code'] }}</strong>
                            {{ $amblist['main_diagnose']['name'] }}
                          </a>
                          <br>
                          <small>{{ $amblist['main_diagnose']['name_latin'] }}</small>
                        </td>
                        <td>
                          <a href="/doctor/{{$amblist['doctor']['id']}}">
                            {{$amblist['doctor']['code_name']}} ({{$amblist['doctor']['code']}})
                          </a>
                        </td>
                        <td>{{ $amblist['exam_type_name'] }}</td>
                        <td>{{ $amblist['date'] }} <small>{{ $amblist['time'] }}</small></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @else
                <div class="well">Моля, изберете имунизация от списъка.</div>
              @endif
            </div>
          </div>
        </div>
    </div>
</div>

@endsection