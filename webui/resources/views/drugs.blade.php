@extends('master')

@section('title', $title)

@section('content')

<script>
$(function(){
  Handlebars.registerHelper('list', function(context, options) {
    var ret = "<ul>";

    for(var i=0, j=context.length; i<j; i++) {
      ret = ret + "<li>" + options.fn(context[i]) + "</li>";
    }

    return ret + "</ul>";
  });

  function filter_codes() {
    var value = $(this).val().toUpperCase();
    $("#drugs .list-group-item").filter(function() {
      $(this).toggle(
        $(this).attr('data-code').indexOf(value) === 0 || 
	    $(this).attr('data-name').toUpperCase().indexOf(value) > -1
      );
    });
  }

  function show_drug(code){
  	$.getJSON('/api/drug/' + code, {}, function(result){
  		$('#no-drug').hide();
  		var source   = $('#drug-info-template').html();
		  var template = Handlebars.compile(source);
  		$('#drug').html(template(result));
  	});
  }

  $("#drug-code").on("keyup", filter_codes);
  $("#drug-code").on("change", filter_codes);
  $("#drugs .list-group-item").on('click', function() { show_drug($(this).attr('data-code')); });

  @if($drug_code)
  	show_drug('{{$drug_code}}');
  @endif
});
</script>

<h3>Лекарства</h3>

<div class="row">	
	<div class="col-md-3">
      <input type="text" class="form-control" placeholder="Код на лекарство" aria-label="Код на лекарство" id="drug-code">
      <ul class="list-group" id="drugs">
        @foreach($drugs as $item)
          <li class="list-group-item" data-code="{{$item['code']}}" data-name="{{$item['name']}} {{$item['market_name']}}">
            <a href="#">
              <strong>{{$item['code']}} - {{$item['name']}}</strong>
              <br>
              {{$item['market_name']}}
              <br>
              <small>
              	{{$item['form']}} - {{$item['quantity']}} {{$item['unit']}}
              </small>
            </a>
          </li>
        @endforeach
      </ul>
	</div>
	<div class="col-md-9">
		<div id="no-drug" class="well">Моля, изберете лекарство от списъка.</div>
		<div id="drug"></div>
	</div>
</div>

<script id="drug-info-template" type="text/x-handlebars-template">
<h3>@{{code}} @{{info.name}}</h3>
<hr>
<table class="table table-condensed">
  <tr>
    <th>Пазарно име</th>
    <td>@{{info.market_name}}</td>
    <th>Форма на прием</th>
    <td>@{{info.form}}</td>
    <th>Количество</th>
    <td>@{{info.quantity}}</td>
    <th>Мерни единици</th>
    <td>@{{info.unit}}</td>
  </tr>
</table>

<table class="table table-condensed">
	<tr>
		<th>Диагнози</th>
	</tr>
@{{#each diagnoses}}
	<tr>
	  <td>
		  <a href="/diagnose/@{{this.id}}">
			<strong>@{{this.code}}</strong> @{{name}}
			<br>
			<small>@{{this.name_latin}}</small>
		  </a>
	  </td>
	</tr>
@{{/each}}
@{{#if diagnoses}}

@{{else}}
	<tr>
	  <td>
		  Няма диагнози свързани с това лекарство.
	  </td>
	</tr>
@{{/if}}
</table>

<table class="table table-condensed">
	<tr>
		<th colspan="5">Амбулаторни листи</th>
	</tr>
	<tr>
		<th>Количество</th>
      	<th>Дни</th>

		<th>Номер</th>
      	<th>Основна диагноза</th>
      	<th>Вид преглед</th>
      	<th>Дата и час</th>
      	<th>&nbsp;</th>
    </tr>
@{{#each amblists}}
	<tr>
	  <td>@{{this.quantity}}</td>
	  <td>@{{this.day}}</td>
	  <td>
        <a href="/amblist/@{{this.id}}">
          @{{this.number}}
        </a>
      </td>
      <td>
        <a href="/diagnose/@{{this.main_diagnose.id}}">
          <strong>@{{this.main_diagnose.code}}</strong>
          @{{this.main_diagnose.name}}
        </a>
        <br>
        <small>@{{this.main_diagnose.name_latin}}</small>
      </td>
      <td>@{{this.exam_type_name}}</td>
      <td>@{{this.date}}<small>@{{this.time}}</small></td>
      <td>
      	@{{#if this.has_primary_visit}}
			С първичен преглед<br>
      	@{{/if}}

      	@{{#if this.has_secondary_visit}}
			С вторичен преглед<br>
      	@{{/if}}
      </td>
	</tr>
@{{/each}}
@{{#if amblists}}

@{{else}}
	<tr>
	  <td colspan="5">
		  Няма амбулаторни листи свързани с това лекарство.
	  </td>
	</tr>
@{{/if}}
</table>

<table class="table table-condensed">
	<tr>
		<th>Пациенти</th>
	</tr>
	<tr>
		<th>Количество</th>
		<th>Дни</th>
		<th>ЕГН</th>
		<th>Име</th>
		<th>Адрес</th>
	</tr>
@{{#each patients}}
	<tr>
	  <td>@{{this.quantity}}</td>
	  <td>@{{this.day}}</td>
	  <td><a href="/pateint/@{{this.id}}">@{{this.EGN}}</a></td>
	  <td>@{{this.name.given}} @{{this.name.sur}} @{{this.name.family}}</td>
	  <td>
	  	Район: @{{this.zdr_rajon}}<br>
	  	РЗОК: @{{this.RZOK}}<br> 
		Адрес:  @{{this.address}}
	</td>
	</tr>
@{{/each}}
@{{#if patients}}

@{{else}}
	<tr>
	  <td>
		  Няма пациенти свързани с това лекарство.
	  </td>
	</tr>
@{{/if}}
</table>

</script>

@endsection