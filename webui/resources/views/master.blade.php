<!doctype html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <title>@yield('title') </title>
      <meta name="author" content="Hristo Georgiev">
      <meta name="description" content="@yield('title') ">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

      <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
      <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

      <style type="text/css">
          .panel-body table{ margin-bottom: 0; }
          .panel { margin-bottom: 0; }
          .table tr:first-child>th, 
          .table tr:first-child>td { border-top: none !important; }
          /* Sticky footer styles
          -------------------------------------------------- */
          html {
            position: relative;
            min-height: 100%;
          }
          body {
            /* Margin bottom by footer height */
            margin-bottom: 60px;
          }
          #footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            background-color: #f5f5f5;
          }


          /* Custom page CSS
          -------------------------------------------------- */
          /* Not required for template or sticky footer method. */

          body > .container {
            padding: 60px 15px 0;
          }
          .container .text-muted {
            margin: 20px 0;
          }

          #footer > .container {
            padding-right: 15px;
            padding-left: 15px;
          }

          code {
            font-size: 80%;
          }

          .list-group-item.active {
            z-index: 2;
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
          }
          
          .list-group-item.active a{
            color: #fff;
          }
      </style>
    </head>
    <body>


    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Амбулаторни листи</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">

            @if($tab == 'practice')
              <li class="active"><a href="/">Практики</a></li>
            @else
              <li><a href="/">Практики</a></li>
            @endif

            @if($tab == 'diagnoses')
              <li class="active"><a href="/diagnoses">Диагнози</a></li>
            @else
              <li><a href="/diagnoses">Диагнози</a></li>
            @endif

            @if($tab == 'morbidity')
              <li class="active"><a href="/morbidity">Морбидност</a></li>
            @else
              <li><a href="/morbidity">Морбидност</a></li>
            @endif

            @if($tab == 'drugs')
              <li class="active"><a href="/drugs">Лекарства</a></li>
            @else
              <li><a href="/drugs">Лекарства</a></li>
            @endif

            @if($tab == 'procedures')
              <li class="active"><a href="/procedures">Процедури</a></li>
            @else
              <li><a href="/procedures">Процедури</a></li>
            @endif

            @if($tab == 'immunizations')
              <li class="active"><a href="/immunizations">Имунизации</a></li>
            @else
              <li><a href="/immunizations">Имунизации</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>

    <!-- Begin page content -->
    <main role="main" class="container">
      @yield("content")
    </main>
    
    <div id="footer">
      <div class="container">
        <p class="text-muted">Hristo Georgiev @ FMI</p>
      </div>
    </div>
  </body>
</html>
