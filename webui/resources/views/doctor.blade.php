@extends('master')

@section('title', 'Doctor')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <h3>Доктор</h3>

          <div class="row">
            <div class="col-xs-6">
                <address>
                    Име: {{ $doctor['name'] }}
                    <br>
                    EGN: {{ $doctor['EGN'] }}
                    <br>
                    UIN: {{ $doctor['UIN'] }}
                    <br>
                    SIMP: {{ $doctor['SIMP'] }} ({{ $doctor['SIMPCode'] }})
                </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                Име на практиката: 
                <br>
                {{ $doctor['practice']['name'] }}
                <br>
                Код на практиката: 
                <br>
                {{ $doctor['practice']['code'] }}
                <br>
              </address>
            </div>
        </div>

        <h3>Амбулаторни листи</h3>

          <table class="table table-condensed">
            <thead>
              <th>Номер</th>
              
              <th>Основна диагноза</th>
              <th>Вид преглед</th>
              <th>Дата и час</th>
              <th>&nbsp;</th>
            </thead>
            <tbody>
              @foreach ($amblists as $amblist)
                <tr>
                  <td>
                    <a href="/amblist/{{ $amblist['id'] }}">
                      {{ $amblist['number'] }}
                    </a>
                  </td>
                  <td>
                    <a href="/diagnose/{{$amblist['main_diagnose']['id']}}">
                      <strong>{{ $amblist['main_diagnose']['code'] }}</strong>
                      {{ $amblist['main_diagnose']['name'] }}
                    </a>
                    <br>
                    <small>{{ $amblist['main_diagnose']['name_latin'] }}</small>
                  </td>
                  <td>{{ $amblist['exam_type_name'] }}</td>
                  <td>{{ $amblist['date'] }} <small>{{ $amblist['time'] }}</small></td>
                  <td>
                    @if($amblist['has_primary_visit'])
                      С първичен преглед<br>
                    @endif
                    @if($amblist['has_secondary_visit'])
                      С вторичен преглед<br>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>

@endsection