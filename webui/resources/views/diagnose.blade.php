@extends('master')

@section('title', $title)

@section('content')

<style type="text/css">
.just-padding {
  padding: 15px;
}

.list-group.list-group-root {
  padding: 0;
  overflow: hidden;
}

.list-group.list-group-root .list-group {
  margin-bottom: 0;
}

.list-group.list-group-root .list-group-item {
  border-radius: 0;
  border-width: 1px 0 0 0;
}

.list-group.list-group-root > .list-group-item:first-child {
  border-top-width: 0;
}

.list-group.list-group-root > .list-group > .list-group-item {
  padding-left: 30px;
}

.list-group.list-group-root > .list-group > .list-group > .list-group-item {
  padding-left: 45px;
}

.list-group-item .glyphicon {
  margin-right: 5px;
}
</style>
<script type="text/javascript">
  let with_usage_only = 0;

  let chapters = {
    current: 0,
    init: function(callback)
    {

      $.getJSON('/api/diagnose/chapters', {with_usage_only: with_usage_only}, function(data){
        let view = '';

        for(let chapter of data)
        {
          view += '<a href="#chapter-' + chapter.id + '" class="list-group-item chapter"><strong>' + chapter.code + '</strong> ' + chapter.name + '</a><div class="list-group collapse set-holder" id="chapter-' + chapter.id + '"></div>';
        }

        $('#chapters').html(view);

        $('.chapter').on('click', function(){ 
          let id = $(this).attr('href').split('-')[1];
          chapters.get(id, function(data){ 
            chapters.toogle(data);
            chapters.build(data); 
          });
          return false;
        });
      });
    },
    get: function(id, callback)
    {
      $.getJSON('/api/diagnose/chapter/' + id, {with_usage_only: with_usage_only}, function(chapter){
        chapters.current   = id;
        sets.current       = 0;
        diagnoses.current  = 0;

        callback(chapter);
      });
    },
    build: function(chapter)
    {
      var source   = $('#diagnose-info-template').html();
      var template = Handlebars.compile(source);
      $('#diagnose').html(template(chapter));
    },
    toogle: function(chapter)
    {
      let element = $(".chapter[href='#chapter-" + chapter.id + "']");
      $('.chapter').removeClass('active');
      $(element).addClass('active');
      $('.set-holder').removeClass('in');
      $('#chapter-' + chapter.id).addClass('in');

      let view = '';
      for(let set of chapter.sets){
        view += '<a href="#set-' + set.id + '" class="list-group-item set"><strong>' + set.code + '</strong> ' + set.name + '</a><div class="list-group collapse diagnose-holder" id="set-' + set.id + '"></div>';
      }

      $('#chapter-' + chapter.id).html(view);

      $('#chapter-' + chapter.id + ' a').on('click', function(){
        let set_id = $(this).attr('href').split('-')[1];
        sets.get(set_id, function(set){
          sets.toogle(set);
          sets.build(set);
        });
        return false;
      });
    }
  };

  let sets = {
    current: 0,
    get: function(id, callback)
    {
      $.getJSON('/api/diagnose/set/' + id, {with_usage_only: with_usage_only}, function(set){
        chapters.current  = set.chapter_id;
        sets.current      = id;
        diagnoses.current = 0;

        callback(set);
      });
    },
    toogle: function(set)
    {
      let element = $(".set[href='#set-" + set.id + "']");
      $('.set').removeClass('active');
      $(element).addClass('active');
      $('.diagnose-holder').removeClass('in');
      $('#set-' + set.id).addClass('in');

      let view = '';
      for(let diagnose of set.diagnoses){
        view += '<a href="#diagnose-' + diagnose.id + '" class="list-group-item diagnose"><strong>' + diagnose.code + '</strong> ' + diagnose.name + '</a>';
      }

      $('#set-' + set.id).html(view);

      $('#set-' + set.id + ' a').on('click', function(){
        let diagnose_id = $(this).attr('href').split('-')[1];

        diagnoses.get(diagnose_id, function(diagnose){
          diagnoses.toogle(diagnose);
          diagnoses.build(diagnose);
        });

        return false;
      });
    },
    build: function(set)
    {
      var source   = $('#diagnose-info-template').html();
      var template = Handlebars.compile(source);
      $('#diagnose').html(template(set));
    }
  };

  let diagnoses = {
    current: 0,
    get: function(id, callback)
    {
      $.getJSON('/api/diagnose/' + id, {}, function(diagnose){
        chapters.current  = diagnose.chapter_id;
        sets.current      = diagnose.set_id;
        diagnoses.current = id;

        callback(diagnose);
      });
    },
    toogle: function(diagnose)
    {
      let element = $(".diagnose[href='#diagnose-" + diagnose.id + "']");
      $('.diagnose').removeClass('active');;
      $(element).addClass('active');
    },
    build: function(diagnose)
    {
      var source   = $('#diagnose-info-template').html();
      var template = Handlebars.compile(source);
      $('#diagnose').html(template(diagnose));
    }
  };

  let navigation = {
    dispatch: function(chapter_id, set_id, diagnose_id)
    {
      if(chapter_id === 0){
        $('#diagnose').html('');
        return;
      }

      chapters.get(chapter_id, function(chapter){
        chapters.toogle(chapter);
        
        if(set_id === 0){
          chapters.build(chapter);
          return;
        }

        sets.get(set_id, function(set){
          sets.toogle(set);
          
          if(diagnose_id === 0){ 
            chapters.build(set);
            return; 
          }

          diagnoses.get(diagnose_id, function(diagnose){
            diagnoses.toogle(diagnose);
            diagnoses.build(diagnose);
          })
        });
      });
    }
  };

  $(function(){
    $('#with-usage-only').on('change', function(){
      with_usage_only = $(this).is(':checked') ? 1 : 0;
      navigation.dispatch(chapters.current, sets.current, diagnoses.current);
    });

    chapters.init(function(){});
    
    @if ($diagnose)
      $.getJSON('/api/diagnose/{{$diagnose}}', {}, function(diagnose){
        navigation.dispatch(diagnose.chapter_id, diagnose.set_id, diagnose.id);
      });
    @endif
  });
</script>

<div class="container">
  <div class="row">
    <h3>Диагнози</h3>
    <div class="col-md-4">
      <label for="with-usage-only">Само срещнати</label>
      <input type="checkbox" id="with-usage-only">
      <div class="just-padding">
        <div class="list-group list-group-root well" id="chapters"></div>
      </div>
    </div>
    <div class="col-md-8" id="diagnose">
    </div>
  </div>
</div>

<script id="diagnose-info-template" type="text/x-handlebars-template">
<h3>@{{code}} @{{name}}</h3>

@{{#if morbidity.id}}
  <a href="/morbidity/@{{morbidity.id}}">@{{morbidity.code}} @{{morbidity.name}}</a><br>
@{{/if}}

@{{#if name_latin}}
  <strong>@{{name_latin}}</strong><br>
@{{/if}}

<hr/>

<h4>Употреба</h4>
<table class="table table-condensed">
  <tbody>
    <tr>
      <th class="text-center">Основна диагноза</th>
      <td>@{{usage.amblist_primary}}</td>
      <th class="text-center">Свързана диагноза</th>
      <td>@{{usage.amblist_primary_linked}}</td>
      <th class="text-center">Придружаваща диагноза</th>
      <td>@{{usage.amblist_secondary}}</td>
      <th class="text-center">Придружаваща свързана диагноза</th>
      <td>@{{usage.amblist_secondary_linked}}</td>
      <th class="text-center">Лекарства</th>
      <td>@{{usage.drugs}}</td>
    </tr>
    <tr>
      <th class="text-center">Болничeн лист</th>
      <td>@{{usage.hlist}}</td>
      <th class="text-center">Направление за МДД</th>
      <td>@{{usage.mdd}}</td>
      <th class="text-center">Направление за МДД - свързана</th>
      <td>@{{usage.mdd_linked}}</td>
      <th class="text-center">Направление за СИМП</th>
      <td>@{{usage.simpconsult}}</td>
      <th class="text-center">Направление за СИМП - свързана</th>
      <td>@{{usage.simpconsult_linked}}</td>
    </tr>
    <tr>
      <th class="text-center">Направление за ВСД</th>
      <td>@{{usage.vsdsimpconsult}}</td>
      <th class="text-center">Направление за ВСД - свързана</th>
      <td>@{{usage.vsdsimpconsult_linked}}</td>
      <th class="text-center">Болнично направление</th>
      <td>@{{usage.hospnapr}}</td>
      <th class="text-center">Болнично направление - свързана</th>
      <td>@{{usage.hospnapr_linked}}</td>
      <th class="text-center">&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>


@{{#if cooccurrence.procedures}}
<hr/>

<h4>Процедури</h4>

<table class="table table-condensed">
  <tbody>
    <tr>
      <th>Процедура</th>
      <th>Брой</th>
      <th>Брой диагноза</th>
      <th>Брой процедури</th>
      <th>PMI</th>
    </tr>
    @{{#each cooccurrence.procedures}}
      <tr>
        <td>
          <a href="/procedure/@{{this.procedure.id}}">
            @{{this.procedure.name}}
          </a>
        </td>
        <td>@{{this.count}}</td>
        <td>@{{this.diagnoseCount}}</td>
        <td>@{{this.procedureCount}}</td>
        <td>@{{this.PMI}}</td>
      </tr>
    @{{/each}}
  </tbody>
</table>
@{{/if}}

</script>
@endsection
